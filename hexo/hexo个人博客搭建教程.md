---
# 博文相关信息描述
---





# centos下hexo个人博客搭建



## 前言

> 以前我一直用csdn博客，没有搭建自己的网站，感觉如果自己的服务器重装系统后，东西都没有了，而且csdn用着感觉还行，就不想麻烦了。前段时间我的写了一篇博客，怎么也提交不上去，而且经常出现无法提交现象，所以就觉得是时候搭建自己的博客网站了。





## 基础环境

```bash
1. node.js
2. git
3. hexo
```



## 1. Node环境安装

### 1.1 系统环境安装配置



**1. 到node.jd官网下载node安装包，用wget命令下载到本地 **

```bash
# 注意：安装最新版本，网址 https://nodejs.org/dist
wget https://nodejs.org/dist/v9.3.0/node-v9.3.0-linux-x64.tar.xz
```



**2. 解压安装包**

```bash
# 解压为tar文件
xz -d node-v9.3.0-linux-x64.tar.xz  
tar -xvf node-v9.3.0-linux-x64.tar
tar -xvJf node-v9.3.0-linux-x64.tar.xz
```



**3 .拷贝到安装目录**

```bash
 mv  node-v9.3.0-linux-x64 node-v9.3.0
 mv  node-v9.3.0 /usr/local/node
```



**4. 配置环境变量**

```bash
# 编辑 /etc/profile (使用vim)
vim /etc/profile
# 在底部添加 PATH 变量
export PATH=$PATH:/usr/local/node/bin
# 保存退出，先按exit键，再按shift+：
wq
# 最后保存并使其生效即可
source /etc/profile
```





### 1.2 用户环境安装配置

**1. 新建用户**

```bash
# 创建用户及用户目录
adduser blog -d /home/blog

# 设置密码
passwd blog

# 安装提示输入密码
blog123;blog123

```



**2. 拷贝node解压包到blog目录下**

```bash
# 拷贝node解码包
mv  node-v9.3.0-linux-x64  node-v9.3.0
mv /root/demo/node-v9.3.0 /home/blog/

# 设置node用户权限
chown -R blog:blog node-v9.3.0

# 设置执行权限
chomd a+x start.sh
```





**3. 配置用户级别的环境变量**

```bash
# 切换用户
su - blog

# 设置用户环境变量覆盖系统环境变量
export PATH=/home/blog/node-v9.3.0/bin:$PATH

# 或者设置
NODEJS_HOME=/home/blog/node-v9.3.0
export PATH=$NODEJS_HOME/bin:$PATH

# 设置环境变量立即生效
source .bashrc

# 查看node安装是否成功
node -v
```





## 2. 安装Hexo

**选择安hexo目录**

````bash
#1.切换用户
su - blog

#2.切换博客目录
cd blog

#3.安装Git(已安装可跳过)
yum install git-core

#4.安装 Hexo
npm install -g hexo-cli

#5.初始化 Hexo
hexo init

````



**安装hexo插件**

```bash
# 我现在看别人博客才发现我没有安装这些插件，建议安装吧。
npm install hexo-generator-index --save
npm install hexo-generator-archive --save
npm install hexo-generator-category --save
npm install hexo-generator-tag --save
npm install hexo-server --save
npm install hexo-deployer-git --save
npm install hexo-deployer-heroku --save
npm install hexo-deployer-rsync --save
npm install hexo-deployer-openshift --save
npm install hexo-renderer-marked --save
npm install hexo-renderer-stylus --save
npm install hexo-generator-feed --save
npm install hexo-generator-sitemap --save
```



**修改hexo配置文件**

```bash
注意：key对应没有值的时候，冒号后面一定要有空格！否则会报错 
例如: timezone:会报错，timezone: 则不会。
```





## 参考资料

```bash
# CentOS下搭建Hexo+github博客
https://blog.csdn.net/qq_32337109/article/details/78730900
```

