---
title: "SQL SERVER 2008 中分页查询"
date: 2019-01-11 09:25:00
author: empathy
img: /resources/images/20190111.jpg
top: true
cover: true
coverImg: /resources/images/20190111.jpg
password: 
toc: true
mathjax: false
summary: "SQL SERVER 2008 中分页查询"
categories: SQL
tags: SqlServer
---



# SQL SERVER 2008 中分页查询



## 前言

​	`Sqlserver`数据库分页查询一直是`Sqlserver`的短板，之前的项目是使用`Sqlserver`数据库，研究过分页查询，最近有时间整理下，后期需要可以查看，我都数据库版本：`Sqlserver 2008 R2`。



## 分页查询



###  建立表

```sql
/** TestTable 测试表 **/
CREATE TABLE [TestTable] (
 [ID] [int] IDENTITY (1, 1) NOT NULL ,
 [FirstName] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NULL ,
 [LastName] [nvarchar] (100) COLLATE Chinese_PRC_CI_AS NULL ,
 [Country] [nvarchar] (50) COLLATE Chinese_PRC_CI_AS NULL ,
 [Note] [nvarchar] (2000) COLLATE Chinese_PRC_CI_AS NULL 
) ON [PRIMARY]
GO
```





### 测试数据

```sql
/** 插入数据：(2万条，用更多的数据测试会明显一些) **/
SET IDENTITY_INSERT TestTable ON
declare @i int
set @i=1
while @i<=20000
begin
    insert into TestTable([id], FirstName, LastName, Country,Note) values(@i, 'FirstName_XXX','LastName_XXX','Country_XXX','Note_XXX')
    set @i=@i+1
end
SET IDENTITY_INSERT TestTable OFF
```





### 分页方案一

利用`Not In`和`SELECT TOP`分页，效率次之。

```sql
/*** 分页查询 ***/
SELECT TOP 10 *
FROM TestTable
WHERE (ID NOT IN
          (SELECT TOP 20 id
         FROM TestTable
         ORDER BY id))
ORDER BY ID

/*** 分页定义说明 ***/
SELECT TOP 页大小 *
FROM TestTable
WHERE (ID NOT IN
          (SELECT TOP 页大小*页数 id
         FROM 表
         ORDER BY id))
ORDER BY ID
```





### 分页方案二

利用`ID`大于多少和`SELECT TOP`分页,效率最高。

```sql
/** 分页查询 **/
SELECT TOP 10 *
FROM TestTable
WHERE (ID >
          (SELECT MAX(id)
         FROM (SELECT TOP 20 id
                 FROM TestTable
                 ORDER BY id) AS T))
ORDER BY ID

/** 分页定义说明 **/
SELECT TOP 页大小 *
FROM TestTable
WHERE (ID >
          (SELECT MAX(id)
         FROM (SELECT TOP 页大小*页数 id
                 FROM 表
                 ORDER BY id) AS T))
ORDER BY ID
```



### 分页方案三

利用SQL的游标存储过程分页。

```sql
/**  游标分页查询，执行效率低，可以忽略。 **/
create  procedure XiaoZhengGe
@sqlstr nvarchar(4000), --查询字符串
@currentpage int, --第N页
@pagesize int --每页行数
as
set nocount on
declare @P1 int, --P1是游标的id
 @rowcount int
exec sp_cursoropen @P1 output,@sqlstr,@scrollopt=1,@ccopt=1,@rowcount=@rowcount output
select ceiling(1.0*@rowcount/@pagesize) as 总页数
--@rowcount as 总行数,@currentpage as 当前页 
set @currentpage=(@currentpage-1)*@pagesize+1
exec sp_cursorfetch @P1,16,@currentpage,@pagesize 
exec sp_cursorclose @P1
set nocount off
```



### 分页应用

```sql
/** 老平台数据迁移的数据分页查询 **/
/*** 分页查询(每页取200条) ***/
DECLARE @page int;
SET @page=1;
DECLARE @size int;
SET @size=30;
SELECT TOP (@size)
--@size 分页大小
a.Id as order_no,
null as order_name,
1 as order_num,
a.[Money] as amount,
a.OrderPaymentStatus AS order_status,
CASE a.OrderPaymentStatus WHEN 4 THEN 'S07' WHEN 8 THEN 'S10' WHEN 9 THEN 'S09' ELSE 'S05' END as order_status,
CASE a.DeductStatus WHEN 1 THEN 'T' ELSE 'F' END as smt_flag,
a.TransmissionData as callback_param,
a.ChargeCode as charge_code,
'C03' as charge_type,
a.Mobile as mobile_phone,
a.Imsi as imsi,
a.Imei as imei,
a.Iccid as iccid,
c.Code as distributor_id,
null as user_ip,
SmsPayChannelInfoId as product_id,
'96' as product_id,
null as channel_id,
a.MobileProvince as region_id,
CASE a.MobileProvince  
WHEN '中国' THEN '100'
WHEN '广东' THEN '1001'
WHEN '广西壮族' THEN '1002'
WHEN '宁夏回族' THEN '1003'
WHEN '新疆维吾尔' THEN '1004'
WHEN '安徽' THEN '1203'
WHEN '澳门' THEN '1322'
WHEN '北京' THEN '1325'
WHEN '福建' THEN '1345'
WHEN '甘肃' THEN '1441'
WHEN '广西' THEN '1543'
WHEN '贵州' THEN '1667'
WHEN '海南' THEN '1768'
WHEN '河北' THEN '1817'
WHEN '河南' THEN '2015'
WHEN '黑龙江' THEN '2185'
WHEN '湖北' THEN '2328'
WHEN '湖南' THEN '2430'
WHEN '吉林' THEN '2587'
WHEN '江苏' THEN '2661'
WHEN '江西' THEN '2799'
WHEN '辽宁' THEN '2913'
WHEN '内蒙古' THEN '3029'
WHEN '宁夏' THEN '3143'
WHEN '青海' THEN '3175'
WHEN '山东' THEN '3214'
WHEN '山西' THEN '3373'
WHEN '陕西' THEN '3508'
WHEN '上海' THEN '3626'
WHEN '台湾' THEN '3647'
WHEN '天津' THEN '3666'
WHEN '西藏' THEN '3687'
WHEN '香港' THEN '3768'
WHEN '新疆' THEN '3788'
WHEN '云南' THEN '3900'
WHEN '浙江' THEN '4046'
WHEN '重庆' THEN '4154'
WHEN '四川' THEN '4196' ELSE '100' END AS region_id,
UserName as sale_id,
CASE a.UserName WHEN 'A' THEN '1' WHEN 'B' THEN '2' WHEN 'C' THEN '3' ELSE '0' END as sale_id,
a.PlatformOrderId as thd_order_no,
a.FirstOrderId as link_order_no,
a.PaymentMessage as order_msg,
a.OrderCrateTime as create_date,
a.OrderCompleteTime as update_date,
a.Ext1 as ext1,
a.CallbackUrl as callback_url
FROM SmsPaymentOrderOfMonth a
LEFT JOIN dbo.CustomerInfo_bak c ON a.CustomerInfoId=c.Id
WHERE a.Id  NOT IN(SELECT TOP ((@page-1)*@size) Id FROM SmsPaymentOrderOfMonth  WHERE SmsPayChannelInfoId=96 AND OrderPaymentStatus=9 ORDER BY Id ASC) 
and a.SmsPayChannelInfoId=96 AND a.OrderPaymentStatus=9
ORDER BY a.Id ASC;


/***  计算分页大小：131620数据；1316页 ***/
SELECT 131620/100
SELECT count(1) FROM SmsPaymentOrderOfMonth  
WHERE SmsPayChannelInfoId=96 AND OrderPaymentStatus=9;

```



### 总结

```sh
# 其它的方案：
如果没有主键，可以用临时表，也可以用方案三做，但是效率会低。
建议优化的时候，加上主键和索引，查询效率会提高。

# 通过SQL 查询分析器，显示比较：我的结论是:
分页方案二：(利用ID大于多少和SELECT TOP分页）效率最高，需要拼接SQL语句
分页方案一：(利用Not In和SELECT TOP分页)   效率次之，需要拼接SQL语句
分页方案三：(利用SQL的游标存储过程分页)    效率最差，但是最为通用
在实际情况中，要具体分析。
```





## 参考资料

```sh
# 高效的SQLSERVER分页查询(推荐)
https://blog.csdn.net/qiaqia609/article/details/41445233
# SQL SERVER 2008 中三种分页方法与比较
https://www.jb51.net/article/35269.htm
# 真正高效的SQLSERVER分页查询
https://www.cnblogs.com/ddlink/archive/2013/03/30/2991007.html
```

