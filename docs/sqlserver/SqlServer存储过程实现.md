---
title: "SqlServer存储过程实现"
date: 2019-01-12 09:25:00
author: empathy
img: /resources/images/20190112.jpg
top: true
cover: true
coverImg: /resources/images/20190112.jpg
password: 
toc: true
mathjax: false
summary: "SqlServer存储过程实现"
categories: SQL
tags: SqlServer
---



# SqlServer存储过程实现



## 应用场景

在实际的业务处理中，我们需要每天定时从流水表中统计当天的数据到一张汇总表，这个可以通道代码或者`sql`执行计划的存储过程来实现，下面我们主要讲解`TSQL`的存储过程来实现订单统计当天的数据到汇总表。





## 存储过程实现

### 业务逻辑说明

```bash
# 统计点播和包月表的昨天和今天的数据到2张表
# 定时插入当天不同时间段数据到JieBaDayPaper,插入前清空当天的数据
1. JieBaDayPaper  
# 定时插入昨日数据到JieBaPaper表，历史数据不会清空
2. JieBaPaper
```



### 汇总表结构

```sql
/**1.JieBaDayPaper 表结构 **/
USE [DailyPaper]
GO

/****** Object:  Table [dbo].[JieBaDayPaper]    
Script Date: 01/11/2019 14:32:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JieBaDayPaper](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[dateTime] [datetime] NULL,
	[people] [nvarchar](20) NULL,
	[chanId] [int] NULL,
	[cusId] [nvarchar](100) NULL,
	[chanName] [nvarchar](200) NULL,
	[cusName] [nvarchar](200) NULL,
	[kpi] [int] NULL,
	[isMonthy] [int] NULL,
	[allMoney] [decimal](18, 2) NULL,
	[smtMoney] [decimal](18, 2) NULL,
	[newMoney] [decimal](18, 2) NULL,
	[lcMoney] [decimal](18, 2) NULL,
	[newUser] [int] NULL,
	[lcUser] [int] NULL,
	[CreateOn] [datetime] NULL,
	[CreateBy] [nvarchar](10) NULL,
 CONSTRAINT [PK_dbo.JieBaDayPaper] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



/** 2.JieBaPaper 表结构 **/
USE [DailyPaper]
GO

/****** Object:  Table [dbo].[JieBaPaper]    
Script Date: 01/11/2019 14:33:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[JieBaPaper](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[dateTime] [datetime] NULL,
	[people] [nvarchar](20) NULL,
	[chanId] [int] NULL,
	[cusId] [nvarchar](100) NULL,
	[chanName] [nvarchar](200) NULL,
	[cusName] [nvarchar](200) NULL,
	[kpi] [int] NULL,
	[isMonthy] [int] NULL,
	[allMoney] [decimal](18, 2) NULL,
	[smtMoney] [decimal](18, 2) NULL,
	[newMoney] [decimal](18, 2) NULL,
	[lcMoney] [decimal](18, 2) NULL,
	[newUser] [int] NULL,
	[lcUser] [int] NULL,
	[CreateOn] [datetime] NULL,
	[CreateBy] [nvarchar](10) NULL,
	[chPeople] [varchar](20) NULL,
 CONSTRAINT [PK_dbo.JieBaPaper] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO





/** 3. 业务预警表结构 **/
USE [DailyPaper]
GO

/****** Object:  Table [dbo].[DailySmtStatistics]    
Script Date: 01/11/2019 14:32:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DailySmtStatistics](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[people] [nvarchar](20) NULL,
	[chanId] [int] NULL,
	[cusId] [uniqueidentifier] NULL,
	[chanName] [nvarchar](200) NULL,
	[cusName] [nvarchar](200) NULL,
	[smtNum] [int] NULL,
	[smtProbability] [int] NULL,
	[smtWhite] [int] NULL,
	[createOn] [datetime] NULL,
	[createBy] [nvarchar](10) NULL,
 CONSTRAINT [PK_dbo.DailySmtStatistics] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/** 4. SmsPaymentOrderInfoSummaryByDay表结构 **/
USE [Jieba.SDK.Base]
GO

/****** Object:  Table [dbo].[SmsPaymentOrderInfoSummaryByDay]    
Script Date: 01/11/2019 14:34:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SmsPaymentOrderInfoSummaryByDay](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[CustomerInfoId] [uniqueidentifier] NULL,
	[CustomerInfoName] [nvarchar](50) NULL,
	[SmsPayChannelInfoId] [int] NULL,
	[SmsPayChannelInfoName] [nvarchar](50) NULL,
	[SmsPayChannelInfoBusinessType] [nvarchar](50) NULL,
	[BusinessPerson] [nvarchar](50) NULL,
	[MobileProvince] [nvarchar](50) NULL,
	[MobileOperator] [int] NULL,
	[TotalOrderCount] [int] NOT NULL,
	[SuccessOrderCount] [int] NOT NULL,
	[UnfinishedOrderCount] [int] NOT NULL,
	[FailureOrderCount] [int] NOT NULL,
	[SuccessTotalMoney] [decimal](18, 2) NOT NULL,
	[SuccessTotalUserCount] [int] NOT NULL,
	[DeductMoney] [decimal](18, 2) NOT NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Remark] [nvarchar](max) NULL,
	[DeductOrderCount] [int] NOT NULL,
	[DeductUserCount] [int] NOT NULL,
	[AddUsersOfDay] [int] NOT NULL,
 CONSTRAINT [PK_dbo.SmsPaymentOrderInfoSummaryByDay] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SmsPaymentOrderInfoSummaryByDay] ADD  DEFAULT ((0)) FOR [DeductOrderCount]
GO

ALTER TABLE [dbo].[SmsPaymentOrderInfoSummaryByDay] ADD  DEFAULT ((0)) FOR [DeductUserCount]
GO

ALTER TABLE [dbo].[SmsPaymentOrderInfoSummaryByDay] ADD  DEFAULT ((0)) FOR [AddUsersOfDay]
GO

/** 5.SmsPaymentOrderInfoSummaryByHour表结构 **/
USE [Jieba.SDK.Base]
GO

/****** Object:  Table [dbo].[SmsPaymentOrderInfoSummaryByHour]    
Script Date: 01/11/2019 14:36:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SmsPaymentOrderInfoSummaryByHour](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[CustomerInfoId] [uniqueidentifier] NULL,
	[CustomerInfoName] [nvarchar](50) NULL,
	[SmsPayChannelInfoId] [int] NULL,
	[SmsPayChannelInfoName] [nvarchar](50) NULL,
	[SmsPayChannelInfoBusinessType] [nvarchar](50) NULL,
	[BusinessPerson] [nvarchar](50) NULL,
	[TotalOrderCount] [int] NOT NULL,
	[SuccessOrderCount] [int] NOT NULL,
	[UnfinishedOrderCount] [int] NOT NULL,
	[FailureOrderCount] [int] NOT NULL,
	[SuccessTotalMoney] [decimal](18, 2) NOT NULL,
	[SuccessTotalUserCount] [int] NOT NULL,
	[DeductMoney] [decimal](18, 2) NOT NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Remark] [nvarchar](max) NULL,
	[MobileProvince] [nvarchar](50) NULL,
	[MobileOperator] [int] NULL,
	[DeductOrderCount] [int] NOT NULL,
	[DeductUserCount] [int] NOT NULL,
 CONSTRAINT [PK_dbo.SmsPaymentOrderInfoSummaryByHour] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SmsPaymentOrderInfoSummaryByHour] ADD  DEFAULT ((0)) FOR [DeductOrderCount]
GO

ALTER TABLE [dbo].[SmsPaymentOrderInfoSummaryByHour] ADD  DEFAULT ((0)) FOR [DeductUserCount]
GO

```



### 存储过程TSQL语句 

```sql
/** 存储过程sql语句 **/

/** 1.当天数据插入操作 **/
USE [Jieba.SDK]
GO

/****** Object:  StoredProcedure [dbo].[Pro_Insert_Db_By_Day_Data]    
Script Date: 01/11/2019 14:21:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[Pro_Insert_Db_By_Day_Data]
as
begin
--清空表数据--
TRUNCATE table DailyPaper.dbo.JieBaDayPaper;
--插入每日数据--
INSERT INTO DailyPaper.dbo.JieBaDayPaper
	--点播数据插入-- 
	SELECT convert(varchar(8),a.CreateOn,112) as [dateTime],
	a.UserName as [people],
	a.SmsPayChannelInfoId as [chanId],
	a.CustomerInfoId as [cusId],
	b.Name as [chanName],
	c.Name as [cusName],
	CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END [kpi],
	'0' as [isMonthy],
	sum([Money]) AS [allMoney],
	sum(CASE a.DeductStatus WHEN 1 THEN a.[Money] ELSE 0 END) as [smtMoney],
	'0' AS [newMoney],
	'0' AS [lcMoney],
	count(0) AS [newUser],
	'0' AS [lcUser],
	getdate() as [CreateOn],
	'empathy' as [CreateBy]
	FROM dbo.SmsPaymentOrderInfo a INNER JOIN dbo.SmsPayChannelInfo b ON a.SmsPayChannelInfoId=b.Id
	INNER JOIN dbo.CustomerInfo c ON a.CustomerInfoId=c.Id
	WHERE a.PaymentStatus=1 
	AND DATEDIFF(day,a.CreateOn,GETDATE())=0
	GROUP BY convert(varchar(8),a.CreateOn,112),a.UserName,a.SmsPayChannelInfoId,a.CustomerInfoId,CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END,b.Name,c.Name;
	--包月数据插入--
INSERT INTO DailyPaper.dbo.JieBaDayPaper 
	SELECT convert(varchar(8),a.OrderCrateTime,112) as [dateTime],
	a.UserName as [people],
	a.SmsPayChannelInfoId as [chanId],
	a.CustomerInfoId as [cusId],
	b.Name as [chanName],
	c.Name as [cusName],
	CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END [kpi],
	'1' as [isMonthy],
	sum([Money]) AS [allMoney],
	sum(CASE a.DeductStatus WHEN 1 THEN a.[Money] ELSE 0 END) as [smtMoney],
	SUM(CASE WHEN (a.OrderPaymentStatus=4 OR a.OrderPaymentStatus=6) THEN a.[Money] ELSE 0 END) AS [newMoney],
	SUM(CASE a.OrderPaymentStatus WHEN 9 THEN a.[Money] ELSE 0 END) AS [lcMoney],
	SUM(CASE WHEN (a.OrderPaymentStatus=4 OR a.OrderPaymentStatus=6) THEN 1 ELSE 0 END) AS [newUser],
	SUM(CASE a.OrderPaymentStatus WHEN 9 THEN 1 ELSE 0 END) AS [lcUser],
	getdate() as [CreateOn],
	'empathy' as [CreateBy]
	FROM dbo.SmsPaymentOrderOfMonth a INNER JOIN dbo.SmsPayChannelInfo b ON a.SmsPayChannelInfoId=b.Id
	INNER JOIN dbo.CustomerInfo c ON a.CustomerInfoId=c.Id
	WHERE (a.OrderPaymentStatus=4 OR a.OrderPaymentStatus=6 OR a.OrderPaymentStatus=9) 
	AND DATEDIFF(day,a.OrderCrateTime,GETDATE())=0
	GROUP BY convert(varchar(8),a.OrderCrateTime,112),a.UserName,a.SmsPayChannelInfoId,a.CustomerInfoId,CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END,b.Name,c.Name;
end

GO


/** 2.昨天数据插入操作 **/
USE [Jieba.SDK]
GO

/****** Object:  StoredProcedure [dbo].[Pro_Insert_Db_By_Yes_Data]    
Script Date: 01/11/2019 14:23:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[Pro_Insert_Db_By_Yes_Data]
as
begin
--清空表数据--
--TRUNCATE table DailyPaper.dbo.JieBaPaper;
--点播数据插入-- 
INSERT INTO DailyPaper.dbo.JieBaPaper
	SELECT convert(varchar(8),a.CreateOn,112) as [dateTime],
	a.UserName as [people],
	a.SmsPayChannelInfoId as [chanId],
	a.CustomerInfoId as [cusId],
	b.Name as [chanName],
	c.Name as [cusName],
	CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END [kpi],
	'0' as [isMonthy],
	sum([Money]) AS [allMoney],
	sum(CASE a.DeductStatus WHEN 1 THEN a.[Money] ELSE 0 END) as [smtMoney],
	'0' AS [newMoney],
	'0' AS [lcMoney],
	count(0) AS [newUser],
	'0' AS [lcUser],
	getdate() as [CreateOn],
	'王超' as [CreateBy],
	b.Salesman as [chPeople]
	FROM dbo.SmsPaymentOrderInfo a INNER JOIN dbo.SmsPayChannelInfo b ON a.SmsPayChannelInfoId=b.Id
	INNER JOIN dbo.CustomerInfo c ON a.CustomerInfoId=c.Id
	WHERE a.PaymentStatus=1 
	AND DATEDIFF(day,a.CreateOn,GETDATE())=1
	GROUP BY convert(varchar(8),a.CreateOn,112),a.UserName,a.SmsPayChannelInfoId,a.CustomerInfoId,CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END,b.Name,c.Name,b.Salesman;
--包月数据插入--
INSERT INTO DailyPaper.dbo.JieBaPaper 
	SELECT convert(varchar(8),a.OrderCrateTime,112) as [dateTime],
	a.UserName as [people],
	a.SmsPayChannelInfoId as [chanId],
	a.CustomerInfoId as [cusId],
	b.Name as [chanName],
	c.Name as [cusName],
	CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END [kpi],
	'1' as [isMonthy],
	sum([Money]) AS [allMoney],
	sum(CASE a.DeductStatus WHEN 1 THEN a.[Money] ELSE 0 END) as [smtMoney],
	SUM(CASE WHEN (a.OrderPaymentStatus=4 OR a.OrderPaymentStatus=6) THEN a.[Money] ELSE 0 END) AS [newMoney],
	SUM(CASE a.OrderPaymentStatus WHEN 9 THEN a.[Money] ELSE 0 END) AS [lcMoney],
	SUM(CASE WHEN (a.OrderPaymentStatus=4 OR a.OrderPaymentStatus=6) THEN 1 ELSE 0 END) AS [newUser],
	SUM(CASE a.OrderPaymentStatus WHEN 9 THEN 1 ELSE 0 END) AS [lcUser],
	getdate() as [CreateOn],
	'王超' as [CreateBy],
	b.Salesman as [chPeople]
	FROM dbo.SmsPaymentOrderOfMonth a INNER JOIN dbo.SmsPayChannelInfo b ON a.SmsPayChannelInfoId=b.Id
	INNER JOIN dbo.CustomerInfo c ON a.CustomerInfoId=c.Id
	WHERE (a.OrderPaymentStatus=4 OR a.OrderPaymentStatus=6 OR a.OrderPaymentStatus=9) 
	AND DATEDIFF(day,a.OrderCrateTime,GETDATE())=1
	GROUP BY convert(varchar(8),a.OrderCrateTime,112),a.UserName,a.SmsPayChannelInfoId,a.CustomerInfoId,CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END,b.Name,c.Name,b.Salesman;
end

GO


/** 3. 带参数的存储过程实现 **/
USE [Jieba.SDK]
GO

/****** Object:  StoredProcedure [dbo].[Pro_Insert_Db_By_Data_Param]    
Script Date: 01/11/2019 14:24:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[Pro_Insert_Db_By_Data_Param]
--带参数输入存储过程
@dt datetime=null
as
begin
--清空表数据--
--TRUNCATE table DailyPaper.dbo.JieBaPaper;
--点播数据插入-- 
INSERT INTO DailyPaper.dbo.JieBaPaper
	SELECT convert(varchar(8),a.CreateOn,112) as [dateTime],
	a.UserName as [people],
	a.SmsPayChannelInfoId as [chanId],
	a.CustomerInfoId as [cusId],
	b.Name as [chanName],
	c.Name as [cusName],
	CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END [kpi],
	'0' as [isMonthy],
	sum([Money]) AS [allMoney],
	sum(CASE a.DeductStatus WHEN 1 THEN a.[Money] ELSE 0 END) as [smtMoney],
	'0' AS [newMoney],
	'0' AS [lcMoney],
	count(0) AS [newUser],
	'0' AS [lcUser],
	getdate() as [CreateOn],
	'王超' as [CreateBy]
	FROM dbo.SmsPaymentOrderInfo a INNER JOIN dbo.SmsPayChannelInfo b ON a.SmsPayChannelInfoId=b.Id
	INNER JOIN dbo.CustomerInfo c ON a.CustomerInfoId=c.Id
	WHERE a.PaymentStatus=1 
	AND DATEDIFF(day,a.CreateOn,@dt)=0
	GROUP BY convert(varchar(8),a.CreateOn,112),a.UserName,a.SmsPayChannelInfoId,a.CustomerInfoId,CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END,b.Name,c.Name;
--包月数据插入--
INSERT INTO DailyPaper.dbo.JieBaPaper 
	SELECT convert(varchar(8),a.OrderCrateTime,112) as [dateTime],
	a.UserName as [people],
	a.SmsPayChannelInfoId as [chanId],
	a.CustomerInfoId as [cusId],
	b.Name as [chanName],
	c.Name as [cusName],
	CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END [kpi],
	'1' as [isMonthy],
	sum([Money]) AS [allMoney],
	sum(CASE a.DeductStatus WHEN 1 THEN a.[Money] ELSE 0 END) as [smtMoney],
	SUM(CASE WHEN (a.OrderPaymentStatus=4 OR a.OrderPaymentStatus=6) THEN a.[Money] ELSE 0 END) AS [newMoney],
	SUM(CASE a.OrderPaymentStatus WHEN 9 THEN a.[Money] ELSE 0 END) AS [lcMoney],
	SUM(CASE WHEN (a.OrderPaymentStatus=4 OR a.OrderPaymentStatus=6) THEN 1 ELSE 0 END) AS [newUser],
	SUM(CASE a.OrderPaymentStatus WHEN 9 THEN 1 ELSE 0 END) AS [lcUser],
	getdate() as [CreateOn],
	'王超' as [CreateBy]
	FROM dbo.SmsPaymentOrderOfMonth a INNER JOIN dbo.SmsPayChannelInfo b ON a.SmsPayChannelInfoId=b.Id
	INNER JOIN dbo.CustomerInfo c ON a.CustomerInfoId=c.Id
	WHERE (a.OrderPaymentStatus=4 OR a.OrderPaymentStatus=6 OR a.OrderPaymentStatus=9) 
	AND DATEDIFF(day,a.OrderCrateTime,@dt)=0
	GROUP BY convert(varchar(8),a.OrderCrateTime,112),a.UserName,a.SmsPayChannelInfoId,a.CustomerInfoId,CASE b.PriorityLevel WHEN 1 THEN 1 ELSE 0 END,b.Name,c.Name;
end

GO


/** 4. 业务预警统计表的插入 **/
USE [Jieba.SDK]
GO

/****** Object:  StoredProcedure [dbo].[Pro_smtStatistics_Daily_Data]    
Script Date: 01/11/2019 14:25:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[Pro_smtStatistics_Daily_Data]
as
begin
--清空smt表数据--
TRUNCATE table DailyPaper.dbo.DailySmtStatistics;
--插入每日smt数据--
--点播smt数据插入-- 
INSERT INTO [DailyPaper].[dbo].[DailySmtStatistics] ([people], [chanId], [cusId], [chanName], [cusName], [smtNum], [smtProbability], [smtWhite], [createOn], [createBy])  
SELECT c.Salesman AS [people],a.SmsPayChannelInfoId AS [chanId],a.CustomerInfoId AS [cusId],b.Name AS [chanName],c.Name AS [cusName],
SUM(CASE a.DeductStatus WHEN 1 THEN 1 ELSE 0 END ) smtNum,e.Probability AS [smtProbability],f.status AS [smtWhite],CONVERT(varchar(8),a.CreateOn,112) AS [createOn],'王超' AS [createBy]
FROM SmsPaymentOrderInfo  a WITH(NOLOCK)
INNER JOIN SmsPayChannelInfo b WITH(NOLOCK) ON  a.SmsPayChannelInfoId=b.Id
INNER JOIN CustomerInfo c WITH(NOLOCK) ON  a.CustomerInfoId=c.Id
LEFT JOIN SmsPaymentDeduct e ON a.SmsPayChannelInfoId=e.SmsPayChannelInfoId and a.CustomerInfoId=e.CustomerInfoId
LEFT JOIN CustomerSmtWhite f ON a.SmsPayChannelInfoId=f.SmsPayChannelInfoId and f.CustomerInfoId=e.CustomerInfoId
WHERE a.PaymentStatus=1
AND b.Status=0 AND c.Status=0
AND datediff(day,a.CreateOn,getdate())=0
GROUP BY c.Name,b.Name,a.SmsPayChannelInfoId,a.CustomerInfoId,e.Probability,c.Salesman,f.status,CONVERT(varchar(8),a.CreateOn,112);
--包月smt数据插入--
INSERT INTO [DailyPaper].[dbo].[DailySmtStatistics] ([people], [chanId], [cusId], [chanName], [cusName], [smtNum], [smtProbability], [smtWhite], [createOn], [createBy])  
SELECT c.Salesman AS [people],a.SmsPayChannelInfoId AS [chanId],a.CustomerInfoId AS [cusId],b.Name AS [chanName],c.Name AS [cusName],
SUM(CASE a.DeductStatus WHEN 1 THEN 1 ELSE 0 END ) smtNum,e.Probability AS [smtProbability],f.status AS [smtWhite],CONVERT(varchar(8),a.OrderCrateTime,112) AS [createOn],'王超' AS [createBy]
FROM SmsPaymentOrderOfMonth  a WITH(NOLOCK)
INNER JOIN SmsPayChannelInfo b WITH(NOLOCK) ON  a.SmsPayChannelInfoId=b.Id
INNER JOIN CustomerInfo c WITH(NOLOCK) ON  a.CustomerInfoId=c.Id
LEFT JOIN SmsPaymentDeduct e ON a.SmsPayChannelInfoId=e.SmsPayChannelInfoId and a.CustomerInfoId=e.CustomerInfoId
LEFT JOIN CustomerSmtWhite f ON a.SmsPayChannelInfoId=f.SmsPayChannelInfoId and f.CustomerInfoId=e.CustomerInfoId
WHERE a.OrderPaymentStatus IN (4,6)
AND b.Status=0 AND c.Status=0
AND datediff(day,a.OrderCrateTime,getdate())=0
GROUP BY c.Name,b.Name,a.SmsPayChannelInfoId,a.CustomerInfoId,e.Probability,c.Salesman,f.status,CONVERT(varchar(8),a.OrderCrateTime,112); 
end

GO


/** 5. 接吧平台订单表前一天的数据汇总**/
USE [Jieba.SDK]
GO

/****** Object:  StoredProcedure [dbo].[OrderStatisticsByDay]    
Script Date: 01/11/2019 14:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: Lawrence		
-- Create date: 2016-7-11
-- Description:	 接吧平台订单表前一天的数据汇总
-- =============================================
CREATE PROCEDURE [dbo].[OrderStatisticsByDay]

@CreateOn datetime
AS
BEGIN
Begin TransAction

DECLARE @errorNo int --错误编号

SET @errorNo=0

INSERT INTO [Jieba.SDK.Base].[dbo].SmsPaymentOrderInfoSummaryByDay(
           [OrderDate]
           ,[CustomerInfoId]
           ,[CustomerInfoName]
           ,[SmsPayChannelInfoId]
           ,[SmsPayChannelInfoName]
           ,[SmsPayChannelInfoBusinessType]
           ,[BusinessPerson]
           ,[TotalOrderCount]
           ,[SuccessOrderCount]
           ,[UnfinishedOrderCount]
           ,[FailureOrderCount]
           ,[SuccessTotalMoney]
           ,[SuccessTotalUserCount]
           ,[DeductMoney]
           ,[ConversionRate]
           ,[MobileProvince]
           ,[MobileOperator]
           ,[DeductOrderCount]
           ,[DeductUserCount]
)

SELECT 
	CONVERT(DATETIME,CONVERT(VARCHAR(10),CreateOn,121)) OrderDate, 
	CustomerInfoId,
	CustomerInfoName,
	SmsPayChannelInfoId,
	SmsPayChannelInfoName,
	SmsPayChannelInfoBusinessType,
	UserName AS BusinessPerson,
	COUNT(1) TotalOrderCount,
	SUM(CASE WHEN PaymentStatus = 1 THEN 1 ELSE 0 END) AS SuccessOrderCount,
	SUM(CASE WHEN PaymentStatus = 0 THEN 1 ELSE 0 END) AS UnfinishedOrderCount, 
	SUM(CASE WHEN PaymentStatus > 1 THEN 1 ELSE 0 END) AS FailureOrderCount,
	SUM(CASE WHEN PaymentStatus = 1 THEN [Money] ELSE 0 END ) AS SuccessTotalMoney,
	COUNT(DISTINCT CASE WHEN PaymentStatus = 1  THEN Mobile ELSE '' END) AS SuccessTotalUserCount,
	SUM(CASE WHEN PaymentStatus = 1 AND DeductStatus = 1 THEN [Money] ELSE 0 END ) AS DeductMoney,
	CONVERT(DECIMAL(18,2), (CONVERT(DECIMAL,SUM(CASE WHEN PaymentStatus = 1 THEN 1 ELSE 0 END)) / CONVERT(DECIMAL,COUNT(1))) *100) AS ConversionRate,
	ISNULL(MobileProvince,'') MobileProvince,
	MobileOperator,
	SUM(CASE WHEN PaymentStatus = 1 AND DeductStatus = 1 THEN 1 ELSE 0 END ) AS DeductOrderCount,
	COUNT(DISTINCT CASE WHEN PaymentStatus = 1 AND DeductStatus = 1 THEN Mobile ELSE '' END) AS DeductUserCount
FROM 
	VIEW_SmsPaymentOrderInfo
WHERE 
	[Status] = 0 AND DATEDIFF(DAY,CreateOn,@CreateOn) = 1
GROUP BY CONVERT(DATETIME,CONVERT(VARCHAR(10),CreateOn,121)), 
     CustomerInfoId, 
     CustomerInfoName, 
     SmsPayChannelInfoId, 
     SmsPayChannelInfoName, 
     SmsPayChannelInfoBusinessType, 
     UserName, 
     ISNULL(MobileProvince,''), 
     MobileOperator
     
SET @errorNo=@errorNo+@@error

-- 根据是否产生错误决定事务是提交还是撤销
If (@errorNo>0)
begin 
   rollback 
end 
Else
Begin 
   Commit 
End 

END

GO


/** 6. 接吧平台订单表上一个小时的数据汇总 **/
USE [Jieba.SDK]
GO

/****** Object:  StoredProcedure [dbo].[OrderStatisticsByHour]    
Script Date: 01/11/2019 14:38:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: Lawrence		
-- Create date: 2016-7-11
-- Description:	 接吧平台订单表上一个小时的数据汇总
-- =============================================
CREATE PROCEDURE [dbo].[OrderStatisticsByHour]
@CreateOn datetime
AS
BEGIN
Begin TransAction

DECLARE @errorNo int --错误编号

SET @errorNo=0

INSERT INTO [Jieba.SDK.Base].[dbo].SmsPaymentOrderInfoSummaryByHour(
           [OrderDate]
           ,[CustomerInfoId]
           ,[CustomerInfoName]
           ,[SmsPayChannelInfoId]
           ,[SmsPayChannelInfoName]
           ,[SmsPayChannelInfoBusinessType]
           ,[BusinessPerson]
           ,[TotalOrderCount]
           ,[SuccessOrderCount]
           ,[UnfinishedOrderCount]
           ,[FailureOrderCount]
           ,[SuccessTotalMoney]
           ,[SuccessTotalUserCount]
           ,[DeductMoney]
           ,[ConversionRate]
           ,[MobileProvince]
           ,[MobileOperator]
           ,[DeductOrderCount]
           ,[DeductUserCount]
)

SELECT 
	CONVERT(DATETIME,CONVERT(VARCHAR(13),CreateOn,121)+':00:00') OrderDate, 
	CustomerInfoId,
	CustomerInfoName,
	SmsPayChannelInfoId,
	SmsPayChannelInfoName,
	SmsPayChannelInfoBusinessType,
	UserName AS BusinessPerson,
	COUNT(1) TotalOrderCount,
	SUM(CASE WHEN PaymentStatus = 1 THEN 1 ELSE 0 END) AS SuccessOrderCount,
	SUM(CASE WHEN PaymentStatus = 0 THEN 1 ELSE 0 END) AS UnfinishedOrderCount, 
	SUM(CASE WHEN PaymentStatus > 1 THEN 1 ELSE 0 END) AS FailureOrderCount,
	SUM(CASE WHEN PaymentStatus = 1 THEN [Money] ELSE 0 END ) AS SuccessTotalMoney,
	COUNT(DISTINCT CASE WHEN PaymentStatus = 1  THEN Mobile ELSE '' END) AS SuccessTotalUserCount,
	SUM(CASE WHEN PaymentStatus = 1 AND DeductStatus = 1 THEN [Money] ELSE 0 END ) AS DeductMoney,
	CONVERT(DECIMAL(18,2), (CONVERT(DECIMAL,SUM(CASE WHEN PaymentStatus = 1 THEN 1 ELSE 0 END)) / CONVERT(DECIMAL,COUNT(1))) *100) AS ConversionRate,
	ISNULL(MobileProvince,'') MobileProvince,
	MobileOperator,
	SUM(CASE WHEN PaymentStatus = 1 AND DeductStatus = 1 THEN 1 ELSE 0 END ) AS DeductOrderCount,
	COUNT(DISTINCT CASE WHEN PaymentStatus = 1 AND DeductStatus = 1 THEN Mobile ELSE '' END) AS DeductUserCount
FROM 
	VIEW_SmsPaymentOrderInfo
WHERE 
	[Status] = 0 AND DATEDIFF(HOUR,CreateOn,@CreateOn) = 1
GROUP BY CONVERT(DATETIME,CONVERT(VARCHAR(13),CreateOn,121)+':00:00'), 
     CustomerInfoId, 
     CustomerInfoName, 
     SmsPayChannelInfoId, 
     SmsPayChannelInfoName, 
     SmsPayChannelInfoBusinessType, 
     UserName, 
     ISNULL(MobileProvince,''), 
     MobileOperator
     
SET @errorNo=@errorNo+@@error

-- 根据是否产生错误决定事务是提交还是撤销
If (@errorNo>0)
begin 
   rollback 
end 
Else
Begin 
   Commit 
End 

END

GO

```



## Python业务实现

### 收入日报脚本

```python

# 收入日报发送需求脚本
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

reload(sys)
sys.setdefaultencoding("utf-8")

import smtplib
import traceback
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# <editor-fold desc="T-SQL-语句">
# 包月、点播的信息费，留存信息费
MONTHY_MONEY_SQL = """
SELECT isMonthy,sum(allMoney) as [allMoney],sum(smtMoney) as [smtMoney],sum(newMoney)as [newMoney],sum(lcMoney)as [lcMoney],sum(newUser) as [newUser],sum(lcUser) as [lcUser]
FROM JieBaPaper WHERE DATEDIFF(MONTH,dateTime,GETDATE())=0 AND DATEDIFF(DAY,dateTime,GETDATE())>=1
GROUP BY isMonthy;"""

# KPI通道信息费
MONTHY_KPI_SQL = """
SELECT chanName as [chanName],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaPaper WHERE DATEDIFF(MONTH,dateTime,GETDATE())=0 AND DATEDIFF(DAY,dateTime,GETDATE())>=1 AND kpi=1
GROUP BY chanName
ORDER BY allMoney DESC;"""

# 商务人员信息费
MONTHY_PEOPLE_SQL = """
SELECT people as [saleman],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaPaper WHERE DATEDIFF(MONTH,dateTime,GETDATE())=0 AND DATEDIFF(DAY,dateTime,GETDATE())>=1
GROUP BY people
ORDER BY allMoney DESC;"""

# 昨日信息费、留存信息费
YERTERDAY_MONEY_SQL = """
SELECT isMonthy,sum(allMoney) as [allMoney],sum(smtMoney) as [smtMoney],sum(newMoney)as [newMoney],sum(lcMoney)as [lcMoney],sum(newUser) as [newUser],sum(lcUser) as [lcUser]
FROM JieBaPaper WHERE DATEDIFF(DAY,dateTime,GETDATE())=1
GROUP BY isMonthy;"""

# KPI通道信息费
YERTERDAY_KPI_SQL = """
SELECT chanName as [chanName],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaPaper WHERE DATEDIFF(DAY,dateTime,GETDATE())=1 AND kpi=1
GROUP BY chanName
ORDER BY allMoney DESC;"""

# 商务人员信息费
YESTERDAY_PEOPLE_SQL = """
SELECT people as [saleman],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaPaper WHERE DATEDIFF(DAY,dateTime,GETDATE())=1
GROUP BY people
ORDER BY allMoney DESC;"""

# 今日信息费、留存信息费
TODAY_MONEY_SQL = """
SELECT isMonthy,sum(allMoney) as [allMoney],sum(smtMoney) as [smtMoney],sum(newMoney)as [newMoney],sum(lcMoney)as [lcMoney],sum(newUser) as [newUser],sum(lcUser) as [lcUser]
FROM JieBaDayPaper WHERE DATEDIFF(DAY,dateTime,GETDATE())=0
GROUP BY isMonthy;"""

# 今日KPI通道信息费
TODAY_KPI_SQL = """
SELECT chanName as [chanName],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaDayPaper WHERE DATEDIFF(DAY,dateTime,GETDATE())=0 AND kpi=1
GROUP BY chanName
ORDER BY allMoney DESC;"""

# 今日商务人员信息费
TODAY_PEOPLE_SQL = """
SELECT people as [saleman],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaDayPaper WHERE DATEDIFF(DAY,dateTime,GETDATE())=0
GROUP BY people
ORDER BY allMoney DESC;"""


# 每月1号汇总上月数据
# 包月、点播的信息费，留存信息费
ONE_MONTHY_MONEY_SQL = """
SELECT isMonthy,sum(allMoney) as [allMoney],sum(smtMoney) as [smtMoney],sum(newMoney)as [newMoney],sum(lcMoney)as [lcMoney],sum(newUser) as [newUser],sum(lcUser) as [lcUser]
FROM JieBaPaper WHERE DATEDIFF(MONTH,dateTime,GETDATE())=1 AND DATEDIFF(DAY,dateTime,GETDATE())>=1
GROUP BY isMonthy;"""

# KPI通道信息费
ONE_MONTHY_KPI_SQL = """
SELECT chanName as [chanName],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaPaper WHERE DATEDIFF(MONTH,dateTime,GETDATE())=1 AND DATEDIFF(DAY,dateTime,GETDATE())>=1 AND kpi=1
GROUP BY chanName
ORDER BY allMoney DESC;"""

# 商务人员信息费
ONE_MONTHY_PEOPLE_SQL = """
SELECT people as [saleman],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaPaper WHERE DATEDIFF(MONTH,dateTime,GETDATE())=1 AND DATEDIFF(DAY,dateTime,GETDATE())>=1
GROUP BY people
ORDER BY allMoney DESC;"""


# 通过传参数控制时间，date_time
# 包月、点播的信息费，留存信息费
SET_MONTHY_MONEY_SQL = """
SELECT isMonthy,sum(allMoney) as [allMoney],sum(smtMoney) as [smtMoney],sum(newMoney)as [newMoney],sum(lcMoney)as [lcMoney],sum(newUser) as [newUser],sum(lcUser) as [lcUser]
FROM JieBaPaper WHERE DATEDIFF(MONTH,dateTime,'{0}')=1 AND DATEDIFF(DAY,dateTime,'{0}')>=1
GROUP BY isMonthy;"""

# KPI通道信息费
SET_MONTHY_KPI_SQL = """
SELECT chanName as [chanName],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaPaper WHERE DATEDIFF(MONTH,dateTime,'{0}')=1 AND DATEDIFF(DAY,dateTime,'{0}')>=1 AND kpi=1
GROUP BY chanName
ORDER BY allMoney DESC;"""

# 商务人员信息费
SET_MONTHY_PEOPLE_SQL = """
SELECT people as [saleman],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaPaper WHERE DATEDIFF(MONTH,dateTime,'{0}')=1 AND DATEDIFF(DAY,dateTime,'{0}')>=1
GROUP BY people
ORDER BY allMoney DESC;"""

# 昨日信息费、留存信息费
SET_YERTERDAY_MONEY_SQL = """
SELECT isMonthy,sum(allMoney) as [allMoney],sum(smtMoney) as [smtMoney],sum(newMoney)as [newMoney],sum(lcMoney)as [lcMoney],sum(newUser) as [newUser],sum(lcUser) as [lcUser]
FROM JieBaPaper WHERE DATEDIFF(DAY,dateTime,'{0}')=1
GROUP BY isMonthy;"""

# KPI通道信息费
SET_YERTERDAY_KPI_SQL = """
SELECT chanName as [chanName],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaPaper WHERE DATEDIFF(DAY,dateTime,'{0}')=1 AND kpi=1
GROUP BY chanName
ORDER BY allMoney DESC;"""

# 商务人员信息费
SET_YESTERDAY_PEOPLE_SQL = """
SELECT people as [saleman],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaPaper WHERE DATEDIFF(DAY,dateTime,'{0}')=1
GROUP BY people
ORDER BY allMoney DESC;"""


# 今日信息费、留存信息费-dt=GETDATE()
SET_TODAY_MONEY_SQL = """
SELECT isMonthy,sum(allMoney) as [allMoney],sum(smtMoney) as [smtMoney],sum(newMoney)as [newMoney],sum(lcMoney)as [lcMoney],sum(newUser) as [newUser],sum(lcUser) as [lcUser]
FROM JieBaDayPaper WHERE DATEDIFF(DAY,dateTime,'{0}')=0
GROUP BY isMonthy;"""

# 今日KPI通道信息费
SET_TODAY_KPI_SQL = """
SELECT chanName as [chanName],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaDayPaper WHERE DATEDIFF(DAY,dateTime,'{0}')=0 AND kpi=1
GROUP BY chanName
ORDER BY allMoney DESC;"""

# 今日商务人员信息费
SET_TODAY_PEOPLE_SQL = """
SELECT people as [saleman],SUM(allMoney) as [allMoney],SUM(smtMoney) as [smtMoney]
FROM JieBaDayPaper WHERE DATEDIFF(DAY,dateTime,'{0}')=0
GROUP BY people
ORDER BY allMoney DESC;"""

# </editor-fold>

# <editor-fold desc="邮件发送处理类">
'''发送邮件方法'''


def sendmail(subject, msg, toaddrs, fromaddr, smtpaddr, password):
    '''''
    @subject:邮件主题
    @msg:邮件内容
    @toaddrs:收信人的邮箱地址
    @fromaddr:发信人的邮箱地址
    @smtpaddr:smtp服务地址，可以在邮箱看，比如163邮箱为smtp.163.com
    @password:发信人的邮箱密码
    '''
    mail_msg = MIMEMultipart()
    if not isinstance(subject, str):
        subject = subject
    mail_msg['Subject'] = subject
    mail_msg['From'] = fromaddr
    mail_msg['To'] = ','.join(toaddrs)
    mail_msg.attach(MIMEText(msg, 'html', 'utf-8'))
    try:
        s = smtplib.SMTP_SSL()
        s.connect(smtpaddr,465)  # 连接smtp服务器
        s.login(fromaddr, password)  # 登录邮箱
        s.sendmail(fromaddr, toaddrs, mail_msg.as_string())  # 发送邮件
        s.quit()
    except Exception as e:
        print("Error: unable to send email")
        print(traceback.format_exc())


""" 发送邮件"""


def send_email(subject, msg, toaddrs):
    '''
    @param subject: 邮件发送主题
    @param msg:  邮件发发送内容
    @param toaddrs:  邮件发送地址
    @return:  无返回值
    '''
	# smtpaddr = "ssl//:smtp.exmail.qq.com"
    fromaddr = "xxx"
    smtpaddr = "smtp.exmail.qq.com"
    password = "xxx"
    sendmail(subject, msg, toaddrs, fromaddr, smtpaddr, password)


# </editor-fold>

# <editor-fold desc="PYMSSQL-数据库操作">
import os
import pymssql


class MSSQL:
    def __init__(self, host, user, pwd, db):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.db = db

    def __GetConnect(self):
        '''desc:获取数据库链接字符串'''
        if not self.db:
            raise (NameError, "没有设置数据库信息,db none")
        # pymssql链接数据库配置
        self.conn = pymssql.connect(host=self.host, user=self.user, password=self.pwd, database=self.db, charset="utf8")
        cur = self.conn.cursor()
        # 获取数据库游标
        if not cur:
            raise (NameError, "连接数据库失败,cur none")
        else:
            return cur

    def ExecQuery(self, sql):
        '''desc:执行sql查询'''
        cur = self.__GetConnect()
        cur.execute(sql)
        resList = cur.fetchall()
        # 查询完毕后必须关闭连接
        # self.conn.commit()
        self.conn.close()
        return resList

    def ExecNonQuery(self, sql):
        cur = self.__GetConnect()
        cur.execute(sql)
        self.conn.commit()
        self.conn.close()

    # 批量插入数据
    def ExecNonInsert(self, sql, list):
        cur = self.__GetConnect()
        cur.executemany(sql, list)
        self.conn.commit()
        self.conn.close()


# </editor-fold>

# <editor-fold desc="收入日报执行业务处理">
"""获取数据库链接"""


def get_db_connect():
    mb_143 = MSSQL(host="127.0.0.1", user="sa", pwd="123456", db="DailyPaper")
    return mb_143


# 今天的n天后的日期。
def get_time(day):
    """
     时间加减day天的操作
    :param day: 加减day天
    :return: 
    """
    import datetime
    # date_str = "2018-04-01 09:10:15"
    # date_time = datetime.datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
    # delta = datetime.timedelta(day)
    # n_days = date_time + delta
    now = datetime.datetime.now()
    delta = datetime.timedelta(day)
    n_days = now + delta
    return n_days.strftime('%Y-%m-%d')


# 日报基础数据

ALL_MONTH_PARAM = {}
ALL_YESTODAY_PARAM = {}
ALL_TODAY_PARAM = {}
all_month_content = "【截至{7} 24时】:\n本月总信息费：{0}元，包月信息费：{1}元，点播信息费：{2}元;\n包月新增信息费：{3}元,留存信息费：{4}元,包月新增用户数：{5}个,留存用用户数：{6}个;\n\n"
kpi_month_content = "本月KPI通道信息费：\n"
people_month_content = "本月商务人员信息费：\n"

all_yesterday_content = "【截止{7} 24时】:\n昨日总信息费：{0}元，包月信息费：{1}元，点播信息费：{2}元;\n包月新增信息费：{3}元,留存信息费：{4}元,包月新增用户数：{5}个,留存用用户数：{6}个;\n\n"
kpi_yesterday_content = "昨日KPI通道信息费：\n"
people_yesterday_content = "昨日商务人员信息费：\n"

all_today_content = "【截至{7}】:\n今日总信息费：{0}元，包月信息费：{1}元，点播信息费：{2}元;\n包月新增信息费：{3}元,留存信息费：{4}元,包月新增用户数：{5}个,留存用用户数：{6}个;\n\n"
kpi_today_content = "今日KPI通道信息费：\n"
people_today_content = "今日商务人员信息费：\n"


# 本月截至昨天-及昨天的信息费统计
def get_month_yes_day():
    import datetime
    global ALL_MONTH_PARAM
    month_list = get_db_connect().ExecQuery(MONTHY_MONEY_SQL)
    for item in month_list:
        if item[0] == 1:
            ALL_MONTH_PARAM['by'] = item[1]
            ALL_MONTH_PARAM['by_new'] = item[3]
            ALL_MONTH_PARAM['by_lc'] = item[4]
            ALL_MONTH_PARAM['new_user'] = item[5]
            ALL_MONTH_PARAM['lc_user'] = item[6]
        if item[0] == 0:
            ALL_MONTH_PARAM['db'] = item[1]

    # 全局变量声明及其使用
    global all_month_content
    all_month_content = all_month_content.format(ALL_MONTH_PARAM.get('by', 0) + ALL_MONTH_PARAM.get('db', 0),
                                                 ALL_MONTH_PARAM.get('by', 0), ALL_MONTH_PARAM.get('db', 0),
                                                 ALL_MONTH_PARAM.get('by_new', 0), ALL_MONTH_PARAM.get('by_lc', 0),
                                                 ALL_MONTH_PARAM.get('new_user', 0), ALL_MONTH_PARAM.get('lc_user', 0),
                                                 get_time(-1))

    # 本月-KPI-信息费
    month_kpi_list = get_db_connect().ExecQuery(MONTHY_KPI_SQL)
    for item in month_kpi_list:
        global kpi_month_content
        kpi_month_content = kpi_month_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    kpi_month_content = kpi_month_content + "\n"

    # 本月-商务-信息费
    month_people_list = get_db_connect().ExecQuery(MONTHY_PEOPLE_SQL)
    for item in month_people_list:
        global people_month_content
        people_month_content = people_month_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    people_month_content = people_month_content + "\n"

    # 截至昨天-信息费
    month_list = get_db_connect().ExecQuery(YERTERDAY_MONEY_SQL)
    for item in month_list:
        if item[0] == 1:
            ALL_YESTODAY_PARAM['by'] = item[1]
            ALL_YESTODAY_PARAM['by_new'] = item[3]
            ALL_YESTODAY_PARAM['by_lc'] = item[4]
            ALL_YESTODAY_PARAM['new_user'] = item[5]
            ALL_YESTODAY_PARAM['lc_user'] = item[6]
        if item[0] == 0:
            ALL_YESTODAY_PARAM['db'] = item[1]

    global all_yesterday_content

    all_yesterday_content = all_yesterday_content.format(
        ALL_YESTODAY_PARAM.get('by', 0) + ALL_YESTODAY_PARAM.get('db', 0),
        ALL_YESTODAY_PARAM.get('by', 0), ALL_YESTODAY_PARAM.get('db', 0),
        ALL_YESTODAY_PARAM.get('by_new', 0), ALL_YESTODAY_PARAM.get('by_lc', 0),
        ALL_YESTODAY_PARAM.get('new_user', 0), ALL_YESTODAY_PARAM.get('lc_user', 0), get_time(-1))

    # 昨日-KPI-信息费
    month_list = get_db_connect().ExecQuery(YERTERDAY_KPI_SQL)
    for item in month_list:
        global kpi_yesterday_content
        kpi_yesterday_content = kpi_yesterday_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    kpi_yesterday_content = kpi_yesterday_content + "\n"

    # 昨日-商务-信息费
    month_list = get_db_connect().ExecQuery(YESTERDAY_PEOPLE_SQL)
    for item in month_list:
        global people_yesterday_content
        people_yesterday_content = people_yesterday_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    people_yesterday_content = people_yesterday_content + "\n"

    daily_paper_content = all_month_content + kpi_month_content + people_month_content + all_yesterday_content + kpi_yesterday_content + people_yesterday_content
    return str(daily_paper_content).replace('\n', '<br>')


# 截至今日当前时间的信息费统计
def get_day():
    import datetime
    today_all_list = get_db_connect().ExecQuery(TODAY_MONEY_SQL)
    for item in today_all_list:
        if item[0] == 1:
            ALL_TODAY_PARAM['by'] = item[1]
            ALL_TODAY_PARAM['by_new'] = item[3]
            ALL_TODAY_PARAM['by_lc'] = item[4]
            ALL_TODAY_PARAM['new_user'] = item[5]
            ALL_TODAY_PARAM['lc_user'] = item[6]
        if item[0] == 0:
            ALL_TODAY_PARAM['db'] = item[1]

    global all_today_content

    all_today_content = all_today_content.format(ALL_TODAY_PARAM.get('by', 0) + ALL_TODAY_PARAM.get('db', 0),
                                                 ALL_TODAY_PARAM.get('by', 0), ALL_TODAY_PARAM.get('db', 0),
                                                 ALL_TODAY_PARAM.get('by_new', 0), ALL_TODAY_PARAM.get('by_lc', 0),
                                                 ALL_TODAY_PARAM.get('new_user', 0), ALL_TODAY_PARAM.get('lc_user', 0),
                                                 datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    # 今日-KPI-信息费
    today_kpi_list = get_db_connect().ExecQuery(TODAY_KPI_SQL)
    for item in today_kpi_list:
        global kpi_today_content
        kpi_today_content = kpi_today_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    kpi_today_content = kpi_today_content + "\n"

    # 今日-商务-信息费
    today_people_list = get_db_connect().ExecQuery(TODAY_PEOPLE_SQL)
    for item in today_people_list:
        global people_today_content
        people_today_content = people_today_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    people_today_content = people_today_content + "\n"

    day_paper_content = all_today_content + kpi_today_content + people_today_content
    return str(day_paper_content).replace('\n', '<br>')


# 本月截至昨天信息费统计
def get_daily_paper():
    return get_month_yes_day() + get_day()



# 每月1号-本月截至月底-及昨天的信息费统计
def get_one_month_yes_day(date_time):
    """
    获取本月截止昨日；昨日的数据
    :param date_time: 当天时间
    :return:
    """
    import datetime
    global ALL_MONTH_PARAM
    month_list = get_db_connect().ExecQuery(SET_MONTHY_MONEY_SQL.format(date_time))
    for item in month_list:
        if item[0] == 1:
            ALL_MONTH_PARAM['by'] = item[1]
            ALL_MONTH_PARAM['by_new'] = item[3]
            ALL_MONTH_PARAM['by_lc'] = item[4]
            ALL_MONTH_PARAM['new_user'] = item[5]
            ALL_MONTH_PARAM['lc_user'] = item[6]
        if item[0] == 0:
            ALL_MONTH_PARAM['db'] = item[1]

    # 全局变量声明及其使用
    global all_month_content
    all_month_content = all_month_content.format(ALL_MONTH_PARAM.get('by', 0) + ALL_MONTH_PARAM.get('db', 0),
                                                 ALL_MONTH_PARAM.get('by', 0), ALL_MONTH_PARAM.get('db', 0),
                                                 ALL_MONTH_PARAM.get('by_new', 0), ALL_MONTH_PARAM.get('by_lc', 0),
                                                 ALL_MONTH_PARAM.get('new_user', 0), ALL_MONTH_PARAM.get('lc_user', 0),
                                                 get_time(-1))

    # 本月-KPI-信息费
    month_kpi_list = get_db_connect().ExecQuery(SET_MONTHY_KPI_SQL.format(date_time))
    for item in month_kpi_list:
        global kpi_month_content
        kpi_month_content = kpi_month_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    kpi_month_content = kpi_month_content + "\n"

    # 本月-商务-信息费
    month_people_list = get_db_connect().ExecQuery(SET_MONTHY_PEOPLE_SQL.format(date_time))
    for item in month_people_list:
        global people_month_content
        people_month_content = people_month_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    people_month_content = people_month_content + "\n"

    # 截至昨天-信息费
    month_list = get_db_connect().ExecQuery(SET_YERTERDAY_MONEY_SQL.format(date_time))
    for item in month_list:
        if item[0] == 1:
            ALL_YESTODAY_PARAM['by'] = item[1]
            ALL_YESTODAY_PARAM['by_new'] = item[3]
            ALL_YESTODAY_PARAM['by_lc'] = item[4]
            ALL_YESTODAY_PARAM['new_user'] = item[5]
            ALL_YESTODAY_PARAM['lc_user'] = item[6]
        if item[0] == 0:
            ALL_YESTODAY_PARAM['db'] = item[1]

    global all_yesterday_content

    all_yesterday_content = all_yesterday_content.format(
        ALL_YESTODAY_PARAM.get('by', 0) + ALL_YESTODAY_PARAM.get('db', 0),
        ALL_YESTODAY_PARAM.get('by', 0), ALL_YESTODAY_PARAM.get('db', 0),
        ALL_YESTODAY_PARAM.get('by_new', 0), ALL_YESTODAY_PARAM.get('by_lc', 0),
        ALL_YESTODAY_PARAM.get('new_user', 0), ALL_YESTODAY_PARAM.get('lc_user', 0), get_time(-1))

    # 昨日-KPI-信息费
    month_list = get_db_connect().ExecQuery(SET_YERTERDAY_KPI_SQL.format(date_time))
    for item in month_list:
        global kpi_yesterday_content
        kpi_yesterday_content = kpi_yesterday_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    kpi_yesterday_content = kpi_yesterday_content + "\n"

    # 昨日-商务-信息费
    month_list = get_db_connect().ExecQuery(SET_YESTERDAY_PEOPLE_SQL.format(date_time))
    for item in month_list:
        global people_yesterday_content
        people_yesterday_content = people_yesterday_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    people_yesterday_content = people_yesterday_content + "\n"

    daily_paper_content = all_month_content + kpi_month_content + people_month_content + all_yesterday_content + kpi_yesterday_content + people_yesterday_content
    return str(daily_paper_content).replace('\n', '<br>')



def get_one_day(date_time):
    """
    获取当天的数据
    :param date_time: 当天的时间
    :return:
    """
    import datetime
    today_all_list = get_db_connect().ExecQuery(SET_TODAY_MONEY_SQL.format(date_time))
    for item in today_all_list:
        if item[0] == 1:
            ALL_TODAY_PARAM['by'] = item[1]
            ALL_TODAY_PARAM['by_new'] = item[3]
            ALL_TODAY_PARAM['by_lc'] = item[4]
            ALL_TODAY_PARAM['new_user'] = item[5]
            ALL_TODAY_PARAM['lc_user'] = item[6]
        if item[0] == 0:
            ALL_TODAY_PARAM['db'] = item[1]

    global all_today_content

    all_today_content = all_today_content.format(ALL_TODAY_PARAM.get('by', 0) + ALL_TODAY_PARAM.get('db', 0),
                                                 ALL_TODAY_PARAM.get('by', 0), ALL_TODAY_PARAM.get('db', 0),
                                                 ALL_TODAY_PARAM.get('by_new', 0), ALL_TODAY_PARAM.get('by_lc', 0),
                                                 ALL_TODAY_PARAM.get('new_user', 0), ALL_TODAY_PARAM.get('lc_user', 0),
                                                 datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    # 今日-KPI-信息费
    today_kpi_list = get_db_connect().ExecQuery(SET_TODAY_KPI_SQL.format(date_time))
    for item in today_kpi_list:
        global kpi_today_content
        kpi_today_content = kpi_today_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    kpi_today_content = kpi_today_content + "\n"

    # 今日-商务-信息费
    today_people_list = get_db_connect().ExecQuery(SET_TODAY_PEOPLE_SQL.format(date_time))
    for item in today_people_list:
        global people_today_content
        people_today_content = people_today_content + str(item[0]) + "：" + str(item[1]) + "元；\n"
    people_today_content = people_today_content + "\n"

    day_paper_content = all_today_content + kpi_today_content + people_today_content
    return str(day_paper_content).replace('\n', '<br>')



#  获取1号的上月截止月底的数据-汇总
def get_one_daily_paper(date_time):
    """
    获取本月截止昨日；昨日；截止今天的数据
    :param date_time: 当天时间
    :return:
    """
    return get_one_month_yes_day(date_time) + get_one_day(date_time)

# </editor-fold>



# 邮件接收人设置(随时变动，优点：不需要重新编译发布。)
OWN_PEOPELE = ['xxx@qq.com']
TEST_PEOPELE = ['xxx@qq.com']
RECIVE_PEOPLE = ['xxx@do-info.cn', 'xxx@do-info.cn','xxx@do-info.cn']
SALE_MAN = ['xxx@do-info.cn','xxx@do-info.cn']
LEADER_MAN = ['xxx@do-info.cn','xxx@do-info.cn']
PROSEE_PEOPLE=['xxx@do-info.cn','xxx@qq.com']


"""执行入口"""
if __name__ == "__main__":
    import time, datetime
	# 邮件发送时间统一在小时的10分
    # date_str = "2018-04-01 09:10:25"
    # send_time = datetime.datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
    # print send_time
    send_time = datetime.datetime.now()
    hour_time = send_time.hour
    start_time = time.time()
    # 统计每月1号的数据，1号需要统计上月截止月底的数据
    if send_time.day==1:
        if hour_time == 9:
            send_email('收入日报发送', get_one_daily_paper(send_time), LEADER_MAN)
            print u'发邮件时间！'
        elif hour_time == 15 or hour_time == 19 or hour_time == 23:
            send_email('收入日报发送', get_one_day(send_time), LEADER_MAN)
            print u'发邮件时间！'
        else:
            print u'非发邮件时间！'
            # send_email('收入日报发送', get_one_daily_paper(), OWN_PEOPELE)
    else:
        if hour_time == 9:
            send_email('收入日报发送', get_daily_paper(),LEADER_MAN)
            print u'发邮件时间！'
        elif hour_time == 15 or hour_time == 19 or hour_time == 23:
            send_email('收入日报发送', get_day(), LEADER_MAN)
            print u'发邮件时间！'
        else:
            print u'非发邮件时间！'
            # send_email('收入日报发送', get_daily_paper(), OWN_PEOPELE)
    end_time = time.time()
    # 邮件发送
    spend_time = (int)(end_time - start_time)
    print u'花费时间为：%d 秒' % spend_time
```



### 定时生成表脚本

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

__time__ = '2018/3/8 11:03'
__author__ = 'wangchao'
__email__ = 'mr.chao2016@outlook.com'
__filename__ = 'send_daily__excel_data.py'
__software__ = 'PyCharm'

import sys
#  python 2.7 设置编码，避免报错
reload(sys)
sys.setdefaultencoding('utf-8')

# region 查询T-SQL
MONTHY_MONEY_SQL = """
SELECT CONVERT(VARCHAR(10),dateTime,23) [日期],chanName [通道],cusName [渠道],people [商务],allMoney [smt前信息费],smtMoney [smt信息费]
FROM JieBaPaper 
WHERE datediff(DAY,dateTime,getdate())=1
ORDER BY chanName,cusName,people,allMoney;"""
# endregion

# region PYMSSQL-数据库操作
import os
import pymssql

class MSSQL:
    def __init__(self, host, user, pwd, db):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.db = db

    def __GetConnect(self):
        '''desc:获取数据库链接字符串'''
        if not self.db:
            raise (NameError, "没有设置数据库信息,db none")
        # pymssql链接数据库配置
        self.conn = pymssql.connect(host=self.host, user=self.user, password=self.pwd, database=self.db, charset="utf8")
        cur = self.conn.cursor()
        # 获取数据库游标
        if not cur:
            raise (NameError, "连接数据库失败,cur none")
        else:
            return cur

    def ExecQuery(self, sql):
        '''desc:执行sql查询'''
        cur = self.__GetConnect()
        cur.execute(sql)
        resList = cur.fetchall()
        # 查询完毕后必须关闭连接
        # self.conn.commit()
        self.conn.close()
        return resList

    def ExecNonQuery(self, sql):
        cur = self.__GetConnect()
        cur.execute(sql)
        self.conn.commit()
        self.conn.close()

    # 批量插入数据
    def ExecNonInsert(self, sql, list):
        cur = self.__GetConnect()
        cur.executemany(sql, list)
        self.conn.commit()
        self.conn.close()
# endregion

# region 数据业务处理
"""获取数据库链接"""


def get_db_connect():
    mb_143 = MSSQL(host="127.0.0.1", user="sa", pwd="123456", db="DailyPaper")
    return mb_143


# 今天的n天后的日期。
def get_time(day):
    """时间加减day天的操作"""
    import datetime
    now = datetime.datetime.now()
    delta = datetime.timedelta(day)
    n_days = now + delta
    return n_days.strftime('%Y-%m-%d')


""" Excel 写入指定文件路径"""


def excel_write(save_file_path, table_header, table_body_data, file_name, sheet_name):
    """
    :param save_file_path:   Excel保存路径
    :param table_header:   Excel文件头部标题-列表
    :param table_body_data: Excel文件内容-列表
    :param file_name: Excel文件名称
    :param sheet_name: Excel工作表名称
    :return:
    """
    flag = True
    import xlwt
    # 创建一个Workbook对象，这就相当于创建了一个Excel文件
    book = xlwt.Workbook(encoding='utf-8', style_compression=0)
    # 创建一个sheet对象，一个sheet对象对应Excel文件中的一张表格。
    # 在电脑桌面右键新建一个Excel文件，其中就包含sheet1，sheet2，sheet3三张表
    sheet = book.add_sheet(sheet_name, cell_overwrite_ok=True)
    # 其中的test是这张表的名字,cell_overwrite_ok，表示是否可以覆盖单元格，
    # 其实是Worksheet实例化的一个参数，默认值是False
    # 向表test中添加数据,其中的'0-行, 0-列'指定表中的单元，
    """遍历数据列表，循环写入数据"""
    tab_x = len(table_header)  # 数据头部标题列数
    tab_y = len(table_body_data)  # 写入数据行数
    # 写入表格头部标题
    try:
        for i in range(tab_x):
            sheet.write(0, i, table_header[i].decode('utf-8'))
    except Exception, e:
        flag = False
        print e.message
    # 写入表格正文内容
    m = 1
    try:
        for data in table_body_data:
            for i in range(tab_x):
                sheet.write(m, i, str(data[i]).decode('utf-8') if isinstance(data[i], str) else data[i])
            m += 1
    except Exception, e:
        flag = False
        print e.message
    # 此处需要将中文字符串解码成unicode码，否则会报错
    # sheet.write(0, 1, '中文名字'.decode('utf-8')) 最后，将以上操作保存到指定的Excel文件中
    # 在字符串前加r，声明为raw字符串，这样就不会处理其中的转义了。否则，可能会报错
    try:
        book.save(r'{0}\{1}.xlsx'.format(save_file_path, file_name).decode('utf-8'))
    except Exception, e:
        flag = False
        print e.message
    return flag


"""
 邮件发送功能，带自定义附件
"""


def send_email_attach(subject_title, subject_body, receiver_people, cc_people, attach_file_path, file_name):
    """
    function_desc: 邮件发送功能，带自定义附件
    :param subject_title: 邮件主题
    :param subject_body:  邮件正文
    :param receiver_people: 邮件接收者
    :param cc_people: 邮件抄送人
    :param attach_file_path: 附件地址
    :param file_name: 附件名称
    :return: None
    """
    import smtplib
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    flag = True
    # 邮件服务器邮箱配置
    sender = 'xxx'
    asmtpserver = "smtp.exmail.qq.com"
    ausername = "xxx@do-info.cn"
    apassword = "123456"

    # 邮件相关信息配置
    msgroot = MIMEMultipart('related')
    msgroot['Subject'] = subject_title
    msgroot['to'] = ','.join(str(i) for i in receiver_people)
    msgroot['Cc'] = ','.join(str(i) for i in cc_people)
    msgroot['from'] = sender

    # MIMEText有三个参数，第一个对应文本内容，第二个对应文本的格式，第三个对应文本编码
    thebody = MIMEText(subject_body, 'plain', 'utf-8')
    msgroot.attach(thebody)

    # 读取xlsx文件作为附件，open()要带参数'rb'，使文件变成二进制格式,从而使'base64'编码产生作用，否则附件打开乱码
    # *****中文的文件路径需要utf-8解码处理，不然文件读取不到****
    # attach_file_path=attach_file_path.decode('utf-8')
    att = MIMEText(open(attach_file_path, 'rb').read(), 'base64', 'utf-8')
    # 设置邮件请求配置
    att['Content-Type'] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    # 下面的filename 等号(=)后面好像不能有空格
    attname = u"attachment; filename =\"{0}\"".format(file_name)
    att['Content-Disposition'] = attname.encode('gb2312')
    msgroot.attach(att)
    # print msgroot.as_string().decode('utf-8')

    # 服务器邮箱登录
    asmtp = smtplib.SMTP()
    asmtp.connect(asmtpserver)
    asmtp.login(ausername, apassword)
    # 发送给多人、同时抄送给多人，发送人和抄送人放在同一个列表中
    # asmtp.sendmail(sender, receiver_people + cc_people, msgroot.as_string())
    try:
        asmtp.sendmail(sender, receiver_people + cc_people, msgroot.as_string())
        asmtp.quit()
    except Exception, e:
        flag = False
        print e.message
    return flag


"""生成excel数据文件存放在服务器"""


def generate_excel(file_path):
    """
    :param file_path:  服务器存放资料附件路径
    :return:
    """
    from datetime import datetime
    flag = True
    # data_time = datetime.now().strftime('%Y-%m-%d')
    data_time=get_time(-1)
    # 基础数据
    table_header = ['日期', '通道', '渠道', '商务', 'smt前信息费', 'smt信息费']
    table_data_list = get_db_connect().ExecQuery(MONTHY_MONEY_SQL)
    # tab_data_list 列表里面放元组数据
    tab_name = "{0}-JieBaDayData".format(data_time)
    tab_sheet_name = data_time
    return excel_write(file_path, table_header, table_data_list, tab_name, tab_sheet_name)


"""从服务器路径读取附件发送给需求方"""


def send_email(file_path, receiver_people, cc_people):
    """
    :param file_path:  服务器存放资料附件路径
    :param receiver_people:  邮件接收人
    :param cc_people:  邮件抄送人
    :return:
    """
    from datetime import datetime
    # data_time = datetime.now().strftime('%Y-%m-%d')
    data_time = get_time(-1)
    subject_title = u'【渠道-通道-每日收入数据】'
    subject_body = u"姜总：\n\n 附件为{0}-渠道-通道-收入数据，请查收。".format(data_time)
    attach_file_path = r"{0}\{1}-JieBaDayData.xlsx".format(file_path, data_time)
    print attach_file_path
    file_name = "{0}-JieBaDayData.xlsx".format(data_time)
    print file_name
    # print attach_file_path
    # print file_name
    return send_email_attach(subject_title, subject_body, receiver_people, cc_people, attach_file_path,file_name)
# endregion


# 邮件接收人设置（随时变动，优点：不需要重新编译发布。）
TEST_PEOPLE = ['xxx@qq.com']
RECEIVER_PEOPLE = ['xxx@rui-info.cn']
# CC_PEOPLE_ABC = ['xxx@do-info.cn','xxxn@do-info.cn']
CC_PEOPLE = ['xxx@do-info.cn']

"""执行入口"""
if __name__ == "__main__":
    import time, datetime

    send_time = datetime.datetime.now().hour
    start_time = time.time()
    # 服务器存放资料的路径
    file_path = r'D:\Users\datas'
    if send_time in [14,15] :
        generate_flag = generate_excel(file_path)
        if generate_flag:
            print u'Excel数据文件生成成功'
            send_flag = send_email(file_path, RECEIVER_PEOPLE, CC_PEOPLE)
            if send_flag:
                print u'邮件发送成功'
            else:
                print u'邮件发送失败'
        else:
            print u'Excel数据文件生成失败'
    else:
        print u'非发邮件时间！'
    # 邮件发送时间
    end_time = time.time()
    spend_time = (int)(end_time - start_time)
    print u'花费时间为：%d 秒' % spend_time

```



### 扣量预警脚本

```python
# smt扣量预警脚本
#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'mrs.wc'
__filename__ = 'smt_daily_statistics'
__datetime__ = '2018/2/25 20:58'

import sys

reload(sys)
sys.setdefaultencoding("utf-8")

import smtplib
import traceback
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# <editor-fold desc="T-SQL-语句">

# 当日通道smt扣量预警信息统计
SMT_DAILY_STATISTICS_SQL = "SELECT [Id],[people],[chanId],[cusId],[chanName],[cusName],[smtNum],[smtProbability],[smtWhite],[createOn],[createBy] FROM [DailyPaper].[dbo].[DailySmtStatistics];"

# </editor-fold>

# <editor-fold desc="邮件发送处理类">
'''发送邮件方法'''


def sendmail(subject, msg, toaddrs, fromaddr, smtpaddr, password):
    '''''
    @subject:邮件主题
    @msg:邮件内容
    @toaddrs:收信人的邮箱地址
    @fromaddr:发信人的邮箱地址
    @smtpaddr:smtp服务地址，可以在邮箱看，比如163邮箱为smtp.163.com
    @password:发信人的邮箱密码
    '''
    mail_msg = MIMEMultipart()
    if not isinstance(subject, str):
        subject = subject
    mail_msg['Subject'] = subject
    mail_msg['From'] = fromaddr
    mail_msg['To'] = ','.join(toaddrs)
    mail_msg.attach(MIMEText(msg, 'html', 'utf-8'))
    try:
        s = smtplib.SMTP()
        s.connect(smtpaddr)  # 连接smtp服务器
        s.login(fromaddr, password)  # 登录邮箱
        s.sendmail(fromaddr, toaddrs, mail_msg.as_string())  # 发送邮件
        s.quit()
    except Exception as e:
        print("Error: unable to send email")
        print(traceback.format_exc())


""" 发送邮件"""


def send_email(subject, msg, toaddrs):
    '''
    @param subject: 邮件发送主题
    @param msg:  邮件发发送内容
    @param toaddrs:  邮件发送地址
    @return:  无返回值
    '''
    fromaddr = "xxx@do-info.cn"
    smtpaddr = "smtp.exmail.qq.com"
    password = "123456"
    sendmail(subject, msg, toaddrs, fromaddr, smtpaddr, password)


# </editor-fold>

# <editor-fold desc="PYMSSQL-数据库操作">
import pymssql


class MSSQL:
    def __init__(self, host, user, pwd, db):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.db = db

    def __GetConnect(self):
        '''desc:获取数据库链接字符串'''
        if not self.db:
            raise (NameError, "没有设置数据库信息,db none")
        # pymssql链接数据库配置
        self.conn = pymssql.connect(host=self.host, user=self.user, password=self.pwd, database=self.db, charset="utf8")
        cur = self.conn.cursor()
        # 获取数据库游标
        if not cur:
            raise (NameError, "连接数据库失败,cur none")
        else:
            return cur

    def ExecQuery(self, sql):
        '''desc:执行sql查询'''
        cur = self.__GetConnect()
        cur.execute(sql)
        resList = cur.fetchall()
        # 查询完毕后必须关闭连接
        # self.conn.commit()
        self.conn.close()
        return resList

    def ExecNonQuery(self, sql):
        cur = self.__GetConnect()
        cur.execute(sql)
        self.conn.commit()
        self.conn.close()

    # 批量插入数据
    def ExecNonInsert(self, sql, list):
        cur = self.__GetConnect()
        cur.executemany(sql, list)
        self.conn.commit()
        self.conn.close()


# </editor-fold>

# <editor-fold desc="收入日报执行业务处理">
"""获取数据库链接"""


def get_db_connect():
    mb_143 = MSSQL(host="127.0.0.1", user="dssql", pwd="123456", db="DailyPaper")
    return mb_143


# 今天的n天后的日期。
def get_time(day):
    """时间加减day天的操作"""
    import datetime
    now = datetime.datetime.now()
    delta = datetime.timedelta(day)
    n_days = now + delta
    return n_days.strftime('%Y-%m-%d')


def get_cur_time():
    """时间加减day天的操作"""
    import datetime
    now = datetime.datetime.now()
    return now.strftime('%Y-%m-%d %H:%M:%S')


# smt预警信息
low_smt_today_content = "【截至{0},当天成功计费，扣量比例小于等于10%的渠道】\n".format(get_cur_time())
no_smt_today_content = "【截至{0},当天成功计费，没有设置扣量比率的渠道】\n".format(get_cur_time())
high_smt_today_content = "【截至{0},当天成功计费，扣量比例大于等于10%的渠道】\n".format(get_cur_time())
# (1)、未设置扣量的信息
smt_no_create_configuration = "({0})、{1}--->{2}--->计费成功，未设置扣量;\n"
# (1)、扣量比例低于10%信息
smt_low_configuration = "({0})、{1}--->{2}--->扣量比例：{3}%;\n"

# 列表追加字典对象时注意，不要追加同一个对象，否则出现引用列表的所有元素导致追加内容一样。
NO_SMT_CONFIG_INFO = {"people": None, "chanName": None, "cusName": None, "smtNum": None, "smtProbability": None,
                      "smtWhite": None}


# 未设置扣量的信息
NO_SMT_CONFIG_INFO_LIST=[]
# 扣量比例低于10%信息
LOW_SMT_CONFIG_INFO_LIST=[]
# 扣量比例高于10%信息
HIGH_SMT_CONFIG_INFO_LIST=[]


# 标识符(全局变量和局部变量引用赋值问题)
is_no_smt=False
is_low_smt=False
IS_LOW_SMT=False

# 全局变量
SMT_CONTENT=''
NO_SMT_CONTENT=''
LOW_SMT_CONTENT=''
HIGH_SMT_CONTENT=''

# 截至今日当前时间的smt扣量设置预警统计
def get_smt_statistic_day():
    import datetime
    global is_no_smt
    global is_low_smt
    global SMT_CONTENT,low_smt_today_content,no_smt_today_content,high_smt_today_content
    today_smt_list = get_db_connect().ExecQuery(SMT_DAILY_STATISTICS_SQL)
    for item in today_smt_list:
        # [smtNum],[smtProbability],[smtWhite] #(0 None None)
        # print item[1], item[4], item[5],item[6], item[7], item[8]
        # 未设置smt的情况
        if item[6] >=0 and item[7] == None and item[8] == None:
            NO_SMT_CONFIG_INFO_LIST.append(item)
        # smt比例较低的情况
        if item[6] >=0 and 0<=item[7] <=10 and item[8] == None:
            LOW_SMT_CONFIG_INFO_LIST.append(item)
        # 有smt设置,且smt不低于10的情况
        if item[6] >=0 and item[7] >10 and item[8] == None:
            HIGH_SMT_CONFIG_INFO_LIST.append(item)

    # 没有设置扣量
    if len(NO_SMT_CONFIG_INFO_LIST)>0:
        is_no_smt=True
        global NO_SMT_CONTENT
        global smt_no_create_configuration
        i=1
        for item in NO_SMT_CONFIG_INFO_LIST:
            NO_SMT_CONTENT+=str(smt_no_create_configuration).format(i,item[5],item[4])
            i += 1
        SMT_CONTENT+=no_smt_today_content+NO_SMT_CONTENT+'\n'

    # 扣量低于10%的
    if len(LOW_SMT_CONFIG_INFO_LIST)>0:
        is_low_smt=True
        global LOW_SMT_CONTENT
        global smt_low_configuration
        i=1
        for item in LOW_SMT_CONFIG_INFO_LIST:
            LOW_SMT_CONTENT+=str(smt_low_configuration).format(i,item[5],item[4],item[7])
            i += 1
        SMT_CONTENT += low_smt_today_content+LOW_SMT_CONTENT+'\n'

    # 扣量超过10%的
    if len(HIGH_SMT_CONFIG_INFO_LIST)>0:
        is_low_smt=True
        global HIGH_SMT_CONTENT
        i=1
        for item in HIGH_SMT_CONFIG_INFO_LIST:
            HIGH_SMT_CONTENT+=str(smt_low_configuration).format(i,item[5],item[4],item[7])
            i += 1
        SMT_CONTENT += high_smt_today_content + HIGH_SMT_CONTENT+'\n'
    # print SMT_CONTENT
    return SMT_CONTENT.replace('\n', '<br>')

# </editor-fold>

# <editor-fold desc="邮件接收人设置">
OWN_PEOPELE = ['xxx@qq.com','xxx@do-info.cn']
RECIVE_PEOPLE = ['xxx@do-info.cn', 'xxx@do-info.cn','xxx@do-info.cn']
SALE_MAN = ['xxx@do-info.cn','xxx@do-info.cn']
# </editor-fold>

"""执行入口"""
if __name__ == "__main__":
    import time, datetime
    send_time = datetime.datetime.now().hour
    start_time = time.time()
    if send_time in [12,18,21] :
        send_email('smt扣量预警提醒', get_smt_statistic_day(), OWN_PEOPELE)
        print u'smt统计预警提醒-时间！'
    else:
        print u'非smt统计预警提醒-时间！'
        send_email('smt扣量预警提醒', get_smt_statistic_day(), OWN_PEOPELE)
    end_time = time.time()
    # 邮件发送
    spend_time = (int)(end_time - start_time)
    print u'邮件发送时间花费：%d 秒' % spend_time

```



## Python执行脚本

```bash
# 收入日报执行脚本，格式bat
@echo off  
D:  
cd D:\Users\workspace\python.project
start python daily_paper.py

# 预警任务执行脚本，格式bat
@echo off  
D:  
cd D:\Users\workspace\python.project
start python smt_daily_statistics.py

# 数据生成脚本，格式bat
@echo off  
D:
cd D:\Users\workspace\python.project
start python send_daily__excel_data.py
```



## 计划任务

```bash
# 使用windows任务计划执行bat脚本
# 配置参考资料：
# windows服务器上定时执行脚
http://39.96.68.224:81/index.php/archives/37/
```



## 参考资料

```bash
# SQL Server基础之存储过程
https://www.cnblogs.com/selene/p/4483612.html
# SQLSERVER存储过程基本语法
https://www.cnblogs.com/chaoa/articles/3894311.html
# SqlServer存储过程详解
https://www.cnblogs.com/sunniest/p/4386296.html
```

