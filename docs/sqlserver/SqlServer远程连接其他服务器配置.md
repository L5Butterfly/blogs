---
title: "SQL Server配置T-SQL访问远程数据库"
date: 2019-01-13 09:25:00
author: empathy
img: /resources/images/20190113.jpg
top: true
cover: true
coverImg: /resources/images/20190113.jpg
password: 
toc: true
mathjax: false
summary: "SQL Server配置T-SQL访问远程数据库"
categories: SQL
tags: SqlServer
---



# SQL Server配置T-SQL访问远程数据库



## 配置TSQL语句

```sql
/** T_SQL配置远程连接SQL语句总结 **/
--1.创建链接服务器 
exec sp_addlinkedserver   'ITSV ', ' ', 'SQLOLEDB ', '远程服务器名或ip地址 ' 
exec sp_addlinkedsrvlogin 'ITSV ', 'false ',null, '用户名 ', '密码 ' 

--2.查询示例 
select * from ITSV.数据库名.dbo.表名 

--3.导入示例 
select * into 表 from ITSV.数据库名.dbo.表名 

--4.以后不再使用时删除链接服务器 
exec sp_dropserver  'ITSV ', 'droplogins '


/**########################################**/
--1. 使用sp_addlinkedserver来增加链接
EXEC sp_addlinkedserver
@server='192.168.2.66',--被访问的服务器别名（习惯上直接使用目标服务器IP，或取个别名如：JOY）
@srvproduct='',
@provider='SQLOLEDB',
@datasrc='192.168.2.66'; --要访问的服务器

--2. 使用sp_addlinkedsrvlogin 来增加用户登录链接

EXEC sp_addlinkedsrvlogin
'192.168.2.66', --被访问的服务器别名（如果上面sp_addlinkedserver中使用别名JOY，则这里也是JOY）
'false',
NULL,
'sa', --帐号
'sa123'; --密码

--3.使用举例(访问目标服务器上的数据库Music，查看其中表test的内容)：
--如果建立链接时的别名是目标服务器IP，即192.168.2.66
--则：
select * from [192.168.2.66].[Music].dbo.test;

--如果建立链接时的别名是JOY,则：
select * from [JOY].[Music].dbo.test;

```



## SQL执行插入操作

```sql
/** SQL执行插入操作总结 **/
--1.关闭表主键自增
SET IDENTITY_INSERT [Jieba.SDK].[dbo].[SmsPaymentOrderOfMonth]   ON;
insert into dbo.SmsPayChannelInfo(Id,PaymentChannel_Id,Name,Code,Description,Operator,BusinessType,SupportCompany,SupportContacts,SuccessRate,PriorityLevel,Status,CreateOn,ChargeModel,TotalAmountLimitByMonth,TotalAmountLimitByDay,SingleUserAmountLimitByMonth,SingleUserAmountLimitByDay,ComplaintRateByAmount,SafeComplaintRateByAmount,ComplaintCountByDay,SafeComplaintCountByDay,ComplaintCountByMonth,SafeComplaintCountByMonth,StartTime,EndTime,AbnormalIncreaseIndex,AbnormalReduceIndex,Type,ComplaintRateByUser,SafeComplaintRateByUser,PCode,OutType,Ext1,Ext2,Ext3,ChargeType,BusinessGroup) select * from [TempSqllp].[Jieba.SDK].dbo.SmsPayChannelInfo;
SET IDENTITY_INSERT [Jieba.SDK].[dbo].[SmsPaymentOrderOfMonth]   OFF;

--2.清空数据表
TRUNCATE Table dbo.[SmsPaymentOrderOfMonth];

--3.查看插入数据--
SELECT count(1)
FROM dbo.[SmsPaymentOrderOfMonth]
WHERE SmsPayChannelInfoId=96;
```



## 参考资料

```bash
# T-Sql访问远程数据库
https://blog.csdn.net/u011127019/article/details/51121514
# 在T-SQL语句中访问远程数据库
https://www.cnblogs.com/lgx5/p/7821887.html
```

