---
title: Hexo-个人博客部署命令
layout: post
date: 2019-06-01 19:07:43
comments: true
categories: Blog
tags: [Hexo,Blog]
keywords: Hexo, Blog
description: 生命在于折腾，又把博客折腾到Hexo了。给Hexo点赞。
---



### Hexo安装

```bash
npm install hexo -g # 安装  
npm update hexo -g # 升级  
hexo init # 初始化
```



### Hexo发布

```bash
# 清除缓存
$ hexo clean

# 生成静态文件
$ hexo generate
$ hexo g # 简写

# 启动本地服务器
$ hexo server
$ hexo s # 简写
用于预览主题，默认地址： http://localhost:4000/

# 部署到远程站点
$ hexo deploy
$ hexo d  # 简写

```



### 常用命令

```bash
hexo n "我的博客" == hexo new "我的博客" #新建文章
hexo p == hexo publish
hexo g == hexo generate#生成
hexo s == hexo server #启动服务预览
hexo d == hexo deploy#部署
```



###  扩展

```bash
# 博客md文件描述;文章头部信息
---
title: "centos下ngrok内网穿透服务搭建"
date: 2019-03-09 09:25:00
author: empathy
img: /resources/images/20190315.jpg
top: true
cover: true
coverImg: /resources/images/20190315.jpg
password: 
toc: true
mathjax: false
summary: "centos下ngrok内网穿透服务搭建"
categories: Ngrok
tags: 
  - Ngrok
  - Centos
---


# hexo账号
su - blog
/home/blog/hexo

# 技术文档存放路径
/home/blog/hexo/source/_posts

# 资源文件存放路径
/home/blog/hexo/source/resources

```



### 参考资料

```bash
# hexo常用命令笔记
https://blog.csdn.net/qq_26975307/article/details/62447489
# hexo发表博客常用命令
https://blog.csdn.net/Ivan_zcy/article/details/82916911
# Git个人博客hexo设置关于、标签、分类、归档
https://blog.csdn.net/u010820857/article/details/82027535
```

