---
title: "centos部署flask应用"
date: 2019-03-11 09:25:00
author: empathy
img: /resources/images/20190313.jpg
top: true
cover: true
coverImg: /resources/images/20190313.jpg
password: 
toc: true
mathjax: false
summary: "centos7环境下部署flas框架的web应用;"
categories: Python
tags: 
  - Python
  - Flask
---



# centos部署flask应用



## 前言

​	这几天学着用flask写一些api接口，然后部署到云上。这个过程虽然网上有很多的教程，但还是遇到不少的问题！采用flask的原因是因为它比较容易上手吧。用flask有专门restful api的框架，用起来简单粗暴有木有！另外，

查了一下flask的应用框架，发现用WSGI + Nginx有下面的几个优势：

-  高性能
- 部署比较方便，采用Nginx可以部署多个Flask的应用



## Flask应用

```python
# centos + Flask + WSGI + Nginx
#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'empathy'
__filename__ = 'manage.py'
__datetime__ = '2018/4/22 19:38'

from flask import Flask
from flask import request
from flask import make_response,Response
import json

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


def Response_headers(content):
    resp = Response(content)
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


@app.errorhandler(403)
def page_not_found(error):
    content = json.dumps({"error_code": "403"})
    resp = Response_headers(content)
    return resp


@app.errorhandler(404)
def page_not_found(error):
    content = json.dumps({"error_code": "404"})
    resp = Response_headers(content)
    return resp


@app.errorhandler(400)
def page_not_found(error):
    content = json.dumps({"error_code": "400"})
    # resp = Response(content)
    # resp.headers['Access-Control-Allow-Origin'] = '*'
    resp = Response_headers(content)
    return resp
    # return "error_code:400"


@app.errorhandler(410)
def page_not_found(error):
    content = json.dumps({"error_code": "410"})
    resp = Response_headers(content)
    return resp


@app.errorhandler(500)
def page_not_found(error):
    content = json.dumps({"error_code": "500"})
    resp = Response_headers(content)
    return resp


@app.route('/test', methods=['POST','GET'])
def test():
    if request.method == 'POST':
        req_type = request.headers['Content-Type']
        print (req_type)
        if req_type=='application/json':
            content = json.dumps(request.json,ensure_ascii=False)
            resp=Response_headers(content)
            resp=content
        elif req_type=="application/x-www-form-urlencoded":
            # print request.form,request.form.to_dict()
            content=json.dumps(request.form.to_dict(),ensure_ascii=False)
            resp=Response_headers(content)
        elif 'form-data' in req_type:
            content = json.dumps(request.form.to_dict(), ensure_ascii=False)
            resp = Response_headers(content)
        else:
            content = json.dumps({"error_code": "1002","error_msg": "未知请求"}, ensure_ascii=False)
            resp = Response_headers(content)

    elif request.method == 'GET':
        data = request.args.to_dict()
        print (data)
        if data is None or data=={}:
            content = json.dumps({"error_code": "1003","error_msg": "参数错误"}, ensure_ascii=False)
        else:
            content = json.dumps(data,ensure_ascii=False)
        resp = Response_headers(content)
    else:
        content = json.dumps({"error_code": "1001","error_msg": "非法请求"},ensure_ascii=False)
        resp = Response_headers(content)
    return resp

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=8037)
	#app.run(host='127.0.0.1',port=8037)

```



## 部署流程

### uwsig安装

```sh
# pip install uwsgi
# 注意，安装python最后指定路径安装，这样安装uwsgi就会在指定的安装目录下，便于区分。
pip2 install uwsgi
pip3 install uwsgi

# 配置软连接，便于直接使用
ln -s  /usr/local/python27/bin/uwsgi  /usr/bin/uwsgi2
ln -s  /usr/local/python36/bin/uwsgi  /usr/bin/uwsgi3

# python2版本编译的uwsgi
uwsgi2 --ini /var/www/pyapp/config.ini
# python3版本编译的uwsgi
uwsgi3 --ini /var/www/pyapp/config.ini
```



###  uwsgi配置

```sh
# 配置文件名称 config.ini，配置文件内容如下

[uwsgi]

# uwsgi 启动时所使用的地址与端口
socket = 127.0.0.1:8037

# 指向网站目录
chdir = /var/www/pyapp/

# python 启动程序文件
wsgi-file = manage.py 

# python 程序内用以启动的 application 变量名
callable = app

#后台启动，并把日志记录到指定文件
daemonize = /var/www/pyapp/log/uwsgi.log

# 处理器数
processes = 4

# 线程数
threads = 2

#状态检测地址
stats = 127.0.0.1:8039

```



### 项目结构

```sh
-rw-r--r--. 1 root root 4074 12月 14 17:25 config.ini # uwsgi配置文件
drwxr-xr-x. 2 root root    6 12月 14 15:33 env   # 虚拟环境
drwxr-xr-x. 2 root root   23 12月 14 15:54 log #日志文件
-rw-r--r--. 1 root root 2951 12月 14 18:04 manage.py #flask应用
-rwxrwxr-x. 1 root root  583 12月 14 18:40 requirements.txt # pip安装包
```



### 项目启动

```sh
# 接口访问地址，nginx反向代理地址
http://192.168.1.33:8038

# 启动uwsgi
uwsgi --ini /var/www/pyapp/config.ini
# python2版本编译的uwsgi
uwsgi2 --ini /var/www/pyapp/config.ini
# python3版本编译的uwsgi
uwsgi3 --ini /var/www/pyapp/config.ini

```





### nginx反向代理配置

```sh
# nginx配置，配置文件文app.conf
server{
    listen      8038;
    server_name localhost;
    charset     utf-8;

    #charset;
    access_log  /root/test/app_access.log;
    error_log  /root/test/app_error.log;
    location / {
        include       uwsgi_params;
        uwsgi_pass    127.0.0.1:8037;
        #uwsgi_param UWSGI_PYHOME /var/www/pyapp/env;
		uwsgi_param UWSGI_PYHOME /usr/local/bin/python2.7
        uwsgi_param UWSGI_CHDIR  /var/www/pyapp;
        uwsgi_param UWSGI_SCRIPT manage:app;

    }
}
```





### 扩展

```sh
#########################################################
# 批量杀死进程
ps -ef | grep uwsgi3 | grep -v grep | awk '{print $2}' | xargs kill -9
ps aux | grep uwsgi3 | grep -v grep | awk '{print $2}' | xargs kill -9

# 平时我们在杀死 同一类很多进程时，一个一个杀死太麻烦了！
# Linux、centos 批量杀死进程命令；用下面的命令一次性批量杀死；
ps aux | grep xxx | grep -v grep | awk '{print $2}' | xargs kill -9
# xxx 为进程名

#########################################################
#启动wsgi
uwsgi --ini /app/project/uwsgi.ini
uwsgi --ini /var/www/pyapp/config.ini

#查看是否启动成功
ps aux | grep wsgi | grep -v grep

#启动nginx 服务
nginx

#查看启动进程
ps aux | grep nginx | grep -v grep

#停止服务  
nginx -s -stop

#强制停止： 
pkill -9 nginx

```



### 部署问题解决

```python
# 解决问题：ImportError: cannot import name MAXREPEAT
#错误：
#运行Python是显示错误（无法导入一个常量的定义）
from _sre import MAXREPEAT
ImportError: cannot import name MAXREPEAT

#有人尝试修改py文件。
from _sre import MAXREPEAT
#改成
try:
        from _sre import MAXREPEAT
except ImportError:
        import _sre
        _sre.MAXREPEAT = 65535
#暂时解决了问题。
```





## 参考博文

```sh
# Centos + Flask + WSGI + Nginx
# uwsgi wsgi nginx centos7.2部署flask 
1. https://www.cnblogs.com/waken-captain/p/7776883.html
# centos 7 nginx+uwsgi+flask 环境搭建
2. https://blog.csdn.net/yo746862873/article/details/52006667
# Flask + WSGI + Nginx 云部署
3. https://www.cnblogs.com/marvinzns/p/4696293.html
# 阿里云部署 Flask + WSGI + Nginx 详解
4. http://www.cnblogs.com/Ray-liang/p/4173923.html
# centos 7 部署 Flask + WSGI + Nginx 
5. http://blog.sina.com.cn/s/blog_71daa06b0102x9oi.html
```

