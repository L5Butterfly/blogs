---
title: "python环境的快速配置"
date: 2019-03-11 09:25:00
author: empathy
img: /resources/images/20190311.jpg
top: true
cover: true
coverImg: /resources/images/20190311.jpg
password: 
toc: true
mathjax: false
summary: "python环境的快速配置;python环境的安装脚本;"
categories: Python
tags: Python
---



# python环境的快速配置



## 前言

`CentOS 7`默认已经安装`Python 2.7`，但是某些软件可能需要用到`Python 3`，这篇文章分享`CentOS 7`环境下安装`Python3`的方法，同时不影响原有的`Python 2.7`，让两者共存。



# python环境的安装脚本



python3.6一键安装shell脚本如下。

```sh
#coding:utf-8
export LANG=UTF-8
export LANGUAGE=UTF-8
#更新yum
yum -y update
yum groupinstall -y "Development tools"
# 安装基础依赖
yum install -y nano make sqlite-devel screen gcc ncurses-devel ncurses-libs zlib-devel mysql-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel openssl-devel
# 下载python
wget https://www.moerats.com/usr/shell/Python3/Python-3.6.4.tar.gz && tar zxvf Python-3.6.4.tar.gz && cd Python-3.6.4
# 指定编译路径
./configure --prefix=/usr/local/python3
# 编译安装
make&& make install
cd ..
# 删除安装包
rm -rf Python-3.6.4 Python-3.6.4.tar.gz
```





## 手动安装python3步骤

​	目前`Python`最新版为`Python 3.7.0`，如果以后有新版本更新，基本也差不多，以下是编译安装`Python 3.7.0`的方法，根据提示一行一个命令输入即可：



```sh
#安装依赖
yum -y install wget gcc gcc-c++ libffi-devel zlib-devel
#下载源码（官方）
wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tar.xz
#如果速度较慢，可以从xiaoz软件库下载
wget http://soft.xiaoz.org/python/Python-3.7.0.tar.xz
#解压
tar -xvJf Python-3.7.0.tar.xz
#进入目录
cd Python-3.7.0
#编译安装
./configure --prefix=/usr/local/python3 --enable-optimizations
make -j4 && make -j4 install
#设置软连接
ln -s /usr/local/python3/bin/python3.7 /usr/bin/python3
ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3
```

​	

如果一切顺利，输入命令`python3 -V`就可以看到版本号啦，如果要使用原来的Python 2.7，直接输入`python xxx`即可，两者互不影响。





## 安装和卸载脚本

​	虽说编译安装已经足够简单，但如果多台服务器需要安装`Python 3`，一行一行的敲击命令难免会浪费时间，直接使用`shell`写好的一键脚本安装省事、省力，直接复制下面命令即可



```sh
# 一键安装Python 3
wget https://raw.githubusercontent.com/helloxz/shell/master/python3.sh && sh python3.sh
```



​	`python3`环境的`安装`和`卸载`的脚本实现代码：

```sh
#!/bin/bash
###############################
####CentOS7一键安装Python3######
###############################

#导入环境变量
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin
export PATH

#安装依赖
function rely(){
	yum -y install wget gcc gcc-c++ libffi-devel zlib-devel
}

#安装Python 3.7函数
function install_py37(){
	#调用安装依赖函数
	rely
	#下载源码
	wget http://soft.xiaoz.org/python/Python-3.7.0.tar.xz
	#解压
	tar -xvJf Python-3.7.0.tar.xz
	cd Python-3.7.0
	#编译安装
	./configure --prefix=/usr/local/python3 --enable-optimizations
	make -j4 && make -j4 install
	#清理工作
	cd ..
	rm -rf Python-*
	#设置软连接
	ln -s /usr/local/python3/bin/python3.7 /usr/bin/python3
	ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3
	echo "------------------------------------------------"
	echo '|	恭喜您，Python 3安装完成！  		 |'	
	echo "------------------------------------------------"
}

###卸载Python 3
function uninstall(){
	rm -rf /usr/local/python3
	rm -rf /usr/bin/python3
	rm -rf /usr/bin/pip3
	echo "------------------------------------------------"
	echo '|	Python 3已卸载！				 |'	
	echo "------------------------------------------------"
}

echo "------------------------------------------------------------"
echo 'CentOS 7一键安装Python 3脚本 ^_^ 请选择需要执行的操作：'
echo "1) 安装Python 3.7.0"
echo "2) 卸载Python 3"
echo "q) 退出！"
echo "------------------------------------------------------------"
read -p ":" istype

case $istype in
	1)
		install_py37
	;;
	2)
		uninstall
	;;
	'q')
		exit
	;;
	*)
		echo '参数错误！'
		exit
	;;
esac	
```



## 参考资料

```sh
# 1.CentOS7编译安装/一键安装Python3
https://www.xiaoz.me/archives/10619
# 2.Python3.6一键安装脚本 for CentOS/Debian
https://www.moerats.com/archives/507/
```







