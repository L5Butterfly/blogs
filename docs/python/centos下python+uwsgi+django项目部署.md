---
title: "centos下django web 应用部署"
date: 2019-03-10 09:25:00
author: empathy
img: /resources/images/20190310.jpg
top: true
cover: true
coverImg: /resources/images/20190310.jpg
password: 
toc: true
mathjax: false
summary: "cnetos下python+nginx+uwsgi+django项目部署"
categories: Python
tags: 
  - Python
  - Django
---



# cnetos下nginx+uwsgi+django项目部署



## 前言

​	当我们在用django开发的web项目时，开发测试过程中用到的是django自带的测试服务器，由于其安全及稳定等性能方面的局限性，django官方并不建议将测试服务器用在实际生产。

　　nginx+uwsgi+django是我们常用的django部署方式。nginx作为最前端的服务器，他负责接收所有的客户端请求，对于请求的静态文件，由nginx服务器自己完成，因为它具有很好处理静态文件的能力，性能进行过优化，支持高并发量；uWSGI服务器作为支持服务器，是用来服务nginx的，nginx将请求的动态文件交给uWSGI进行处理。uWSGI实现了uwsgi、wsgi和http协议，uwsgi协议是uWSGI自定义的协议，定义的是框架（django）和服务器对接的接口。



**服务器环境信息：**

1. `服务器`：centos7.2
2. `python版本`：python2.7 和 python3.6
3. `代理服务器`：nginx、uwsgi
4. `虚拟环境`: virtualenv和 virtualenvwrapper
5. `包管理工具`：pip



## 部署流程

### 虚拟环境安装

​	虚拟环境的管理工具使用的是virtualenv和virtualenvwrapper，便于python多个项目下要求不同版本的python和不同的python第三方扩展包环境。

```sh
# 创建博客系统专用的生产环境py2env_blog
virtualenv -p python2 py2env_blog

##################################################
# 创建虚拟环境基础使用总结
1. virtualenv创建虚拟环境执行命令
virtualenv --help
virtualenv --python=python2  py2env_app
virtualenv --python=python3  py3env_app
virtualenv -p python2 py2env_app
virtualenv -p python3 py3env_app

# 创建项目虚环境;进入项目文件夹
cd /var/www/blog   
# 创建名为env的python空环境  
virtualenv py2env_blog --python=python2.7 --no-site-packages  

# 虚拟环境配置方式
virtualenv -p python2 py2env_blog
virtualenv py2env_blog -p python2 
virtualenv -p python3 py3env_blog
virtualenv py3env_blog -p python3

# --python 指定python的版本,接着跟着虚拟环境的名称
virtualenv --python=python3  py3env_app
virtualenv --python=python2  py2env_app
```





### 项目结构

​	项目结构主要包括应用日志，应用源代码，虚拟环境，uwsgi配置等信息。在服务器应用目录创建blog文件夹，文件夹下存放以上内容信息。

```sh
# 查看blog下的项目基本结构
# tree blog -L 3
blog
├── log  #nginx日志文件
│   ├── access.log
│   └── error.log
├── myblog #源代码
│   ├── blog
│   │   ├── admin.py
│   │   ├── admin.pyc
│   │   ├── apps.py
│   │   ├── blog_urls.py
│   │   ├── blog_urls.pyc
│   │   ├── __init__.py
│   │   ├── __init__.pyc
│   │   ├── migrations
│   │   ├── models.py
│   │   ├── models.pyc
│   │   ├── __pycache__
│   │   ├── templates
│   │   ├── tests.py
│   │   ├── views.py
│   │   └── views.pyc
│   ├── db.sqlite3
│   ├── manage.py
│   ├── myblog
│   │   ├── __init__.py
│   │   ├── __init__.pyc
│   │   ├── __pycache__
│   │   ├── settings_bak.py
│   │   ├── settings.py
│   │   ├── settings.pyc
│   │   ├── urls.py
│   │   ├── urls.pyc
│   │   ├── wsgi.py
│   │   └── wsgi.pyc
│   └── static
│       └── admin
├── py2env_blog #虚拟环境
│   ├── bin
│   │   ├── activate
│   │   ├── activate.csh
│   │   ├── activate.fish
│   │   ├── activate.ps1
│   │   ├── activate_this.py
│   │   ├── django-admin
│   │   ├── django-admin.py
│   │   ├── django-admin.pyc
│   │   ├── easy_install
│   │   ├── easy_install-2.7
│   │   ├── pip
│   │   ├── pip2
│   │   ├── pip2.7
│   │   ├── python -> python2
│   │   ├── python2
│   │   ├── python2.7 -> python2
│   │   ├── python-config
│   │   ├── uwsgi
│   │   └── wheel
│   ├── include
│   │   └── python2.7 -> /usr/include/python2.7
│   ├── lib
│   │   └── python2.7
│   └── lib64 -> /var/www/blog/py2env_blog/lib
├── uwsgi #uwsgi容器
    ├── uwsgi_blogs.ini #uwsgi配置文件
    ├── uwsgi.pid
    └── wsgi.log

```





###  uwsgi安装

​	首先要激活虚拟环境py2env_blog，然后使用pip工具安装uwsgi包和django基础扩展包。

```sh
# 进入虚拟环境下，激活虚拟环境
cd /var/www/blog/py2env_blog/bin

# 激活虚拟环境
source ./activate

# 安装uwsgi和django
pip install uwsgi
pip install django

# 退出虚拟环境
deactivate

```





### uwsgi配置

​	在项目目录下创建一个uwsgi文件夹，进入文件夹下创建一个uwsgi的配置文件，配置项目的配置信息，配置内容如下。

```sh
# 配置文件名称：uwsgi_blogs.ini 具体配置内容如下 
[uwsgi]
# 配置服务器的监听ip和端口，让uWSGI作为nginx的支持服务器的话，设置socke就行；如果要让uWSGI作为单独的web-server，用http
# http = 127.0.0.1:3309
socket = 127.0.0.1:3309
# 配置项目目录（此处设置为项目的根目录）
chdir = /var/www/blog/myblog
# 配置入口模块 (django的入口函数的模块，即setting同级目录下的wsgi.py)
wsgi-file = myblog/wsgi.py
# 开启master, 将会多开一个管理进程, 管理其他服务进程
master = True
# 服务器开启的进程数量
processes = 3
# 以守护进程方式提供服, 输出信息将会打印到log中
daemonize =/var/www/blog/uwsgi/wsgi.log
# 服务器进程开启的线程数量
threads = 2
# 退出的时候清空环境变量
vacuum = true
# 进程pid
pidfile =/var/www/blog/uwsgi/uwsgi.pid
# 配uWSGI搜索静态文件目录（及django项目下我们存放static文件的目录，用uWSGI作为单独服务器时才需要设置，此时我们是用nginx处理静态文件）
# check-static = /var/www/blog/myblog/static
```





### uwsgi启动

```sh
# 单独启动python项目，测试基础环境是否运行正常
/var/www/blog/py2env_blog/bin/python manage.py runserver 192.168.1.33:8000

# 使用uwsgi启动python django项目
cd /var/www/blog/py2env_blog/bin
uwsgi --ini /var/www/blog/uwsgi/uwsgi_blogs.ini


# 查看是否启动成功
ps -ef | grep uwsgi_blogs
ps aux | grep uwsgi_blogs

# 重启服务，杀掉相关进程，停止服务重启即可
kill -9 pid
uwsgi --ini /var/www/blog/uwsgi/uwsgi_blogs.ini

# uwsgi的更多启动参数，参考博客：ubuntu14 nginx+uwsgi配置
https://blog.csdn.net/u012447842/article/details/79369506
https://www.cnblogs.com/breezey/p/6674129.html
```





### nginx配置

​	nginx作为最前端的服务器，他负责接收所有的客户端请求，对于请求的静态文件，由nginx服务器自己完成，因为它具有很好处理静态文件的能力，性能进行过优化，支持高并发量；其配置如下。



```sh
# 配置文件路径
/home/local/nginx/conf/vhost/blogs.conf
# 配置文件信息 blogs.conf;

server{
    listen      8059;
    server_name localhost;
    charset     utf-8;

    #nginx的日志目录;
    access_log  /var/www/blog/log/access.log;
    error_log  /var/www/blog/log/error.log;
    location / {
        # 设置将所有请求转发给uwsgi服务器处理
        include    uwsgi_params;
        # 项目启动地址;指定uwsgi服务器url
        uwsgi_pass    127.0.0.1:3309;
        # 指定虚拟环境
        uwsgi_param UWSGI_PYHOME /var/www/blog/py2env_blog;
        # 指定项目路径
        uwsgi_param UWSGI_CHDIR  /var/www/blog/myblog;
        # 指定启动脚本
        uwsgi_param UWSGI_SCRIPT myblog/wsgi.py;

    }
    location /static {
       alias /var/www/blog/myblog/static/;
    }
}


# 校验配置文件是否正确
nginx -t

# nginx配置重新加载
nginx -s reload
```





## 参考博文



```sh
# python三大神器之virtualenv
1. https://www.cnblogs.com/freely/p/8022923.html
# 使用uwsgi配置django
2. https://www.cnblogs.com/breezey/p/6674129.html
# centos下nginx+uwsgi+django部署流程
3. https://www.cnblogs.com/leexl/p/7810843.html
# 网站部署上线admin静态文件无法正常显示 | Django
4.https://www.cnblogs.com/pymkl/p/7955261.html
# UWSGI配置中文说明
5. https://www.jianshu.com/p/fb6ca54f355d
# Django Nginx+uwsgi 安装配置
6. http://www.runoob.com/django/django-nginx-uwsgi.html
```

