---
title: "centos6.8下python版本2.6升级2.7"
date: 2019-04-18 09:25:00
author: empathy
img: /resources/images/20190418.jpg
top: true
cover: true
coverImg: /resources/images/20190418.jpg
password: 
toc: true
mathjax: false
summary: "centos6.8下python版本2.6升级2.7"
categories: Python
tags: Python
---





# centos6.8下python版本2.6升级2.7

## 前言

​	安装完CentOS7后，执行#Python与#python -V，看到版本号是2.6，而且之前写的都是跑在python3.X上面的，3.X和2.X有很多不同，在这里我就不弊述两者之间的区别了新python千万不要把老版本的删除！新老版本是可以共存的，很多基本的命令、软件包都要依赖预装的老版本python的，比如yum。

​	而且python的多版本是可以共存的，下面会教大家如何处理共存问题



## 下载安装

​	首先下载python2.7源码包，首先到官网下载python2.7.3版本，编译安装，或者直接在centos系统窗口执行,安装编译之前，需要更新gcc，因为gcc版本太老会导致新版本python包编译不成功。

```bash
# 更新gcc、zlib、zlib-devel环境
yum -y install gcc
yum -y install zlib zlib-devel
# 下载python2.7
wget http://www.python.org/ftp/python/2.7.3/Python-2.7.3.tgz
# 解压已下载的二进制包并编译安装
tar zxvf Python-2.7.3.tgz
#进入解压目录：
cd Python-2.7.3
# 编译安装
./configure
# 配置安装目录，其他选项一般默认即可：(不配置也可以，直接./configure命令)
#./configure  --prefix=/usr/python2.7

# 开始编译安装：
make && make install

# 编译安装完毕以后，可以输入上面一行命令，查看版本 
python –V
```



## 建立软连接

​	Centos6.8 默认使用的python2.6,直接升级版本，需要保留原来的2.6版本，因为yum源是使用python2.6开发，直接升级高级的python版本会导致yun无法使用，因此升级版本后需要更改yum的配置。而Centos 7.0以上的系统默认使用的python2.7，升级到高级版本可以不用考虑这个问题。

​	系统中原来的python在`/usr/bin/python`，通过ls -l可以看到，python是一个软链接，链接到本目录下的python2.6，我们可以不用把这个删除，只需备份即可，不对原来默认的环境做任何修改，只新建一个python2的软链接即可，只是需要执行python2代码时python要改成python2，或者python脚本头部解释器要改为`#!/usr/bin/python2`，python3的版本可以修改为：`#!/usr/bin/python3`即可。

```bash
# 然后备份原来的python，并把python2.7做软连接到新的位置；
# 建立软连接指向到当前系统默认python命令的bin目录
# 备份软连接，以前的2.6版本
mv /usr/bin/python /usr/bin/python2.6
mv /usr/bin/python /usr/bin/python.bak

# 创建新的python软连接，版本2.7
ln -s /usr/local/bin/python2.7 /usr/bin/python
ln -s /usr/local/bin/python2.7 /usr/bin/python2
# 创建新的python软连接，版本3.6
ln -s /usr/python/bin/python3 /usr/bin/python3
# 查看当前python版本
python -V
python2 -V
python3 -V
```



​	这样就建立好了，以后直接执行`python2`或者`python3`命令就可以调用`python2`和`python3`不同版本的解释器环境了，另外如果仔细看python安装目录下的bin目录，实际上`python2`和`python3`也是个软链接，链接到python3.6.1，这样多次链接也是为了多个版本的管理更加方便。



## yum环境配置

centos6.8版本的yum默认使用的版本是python2.6,升级2.7后需要修好yum配置文件,使其能正常运行；把`#/usr/bin/python`改成`#/usr/bin/python2.6 ` ,这样yum可以正常运行了。

```bash
vi /usr/bin/yum
# 修改内容：
#/usr/bin/python改成#/usr/bin/python2.6即可。

# 刷新配置
source /usr/bin/yum
```



## 安装PiP环境

```bash
# python2.7版本安装pip2环境方法；
# 从官网下载pip包
wget --no-check-certificate https://pypi.python.org/packages/1f/7a/6b239a65d452b04ad8068193ae313b386e6fc745b92cd4584fccebecebf0/setuptools-25.1.6.tar.gz
 
# 解压包
tar -xvf setuptools-25.1.6.tar.gz  
cd setuptools-25.1.6

# 使用python2.7的解释器按照包即可
python2.7 setup.py install  

# python3 直接下载包安装后自带pip环境，不需要按照，若发现没有pip环境，可以参照python的方式按照即可。
python3.6 setup.py install 
```





## pip安装失败解决方案

​	要先安装epel-release，这个包包含了 EPEL 源的 gpg 密钥和软件源信息，该软件包会自动配置yum的软件仓库。执行命令如下安装基础环境，再执行安装命令即可：

```bash
#1.要先安装epel-release，
yum -y install epel-release
#2.执行成功之后，再次执行yum install python-pip
yum -y install python-pip

#3. python安装失败，需要重新按照基础环境，然后重新编译，重新编译命令如下
#需要先安装zlib，再重新编译python，方式两种：
yum install zlib
yum install zlib-devel
#重新编译安装python2.7 
./configure --prefix=/usr/local/python2.7 
make && make install
```



## python安装包异常解决方案

```bash
# 参考博客解决方案
# python3中pip3安装出错，找不到SSL
https://blog.csdn.net/jeryjeryjery/article/details/77880227
# pip install package出现TLS/SSL异常处理方法
https://blog.csdn.net/zhengcaihua0/article/details/79681991

# yum安装openssl-devel 
yum install openssl-devel -y 
yum install libssl-dev -y

# 重新对python3.6进行编译安装，用一下过程来实现编译安装
./configure --with-ssl
make && make install

# 编译成功后，重新安装需要的包即可
pip3 install flask

```



## 扩展知识

​	Linux指定编译安装软件的安装目录的便于软件的维护，和编辑，也让软件文件路径清晰。在Linux编译安装软件时一般分为三个步骤：配置软件、编译软件、安装软件。

```bash
# Linux指定编译安装软件的安装目录(方便管理)
#1.配置软件
./confgure
./confgure --prefix=/usr/local/appname
#2.编译软件
make
#3.安装软件
make install

在软件配置的那一步指定安装软件的路径  ./configure –prefix=/usr/local/soft，
一般软件都安装在/usr/local/这个目录下，比如：./configure –prefix=/usr/local/php

#nginx目录
./configure --prefix=/usr/local/nginx
#php目录
./configure --prefix=/usr/local/php
#mysql目录
./configure --prefix=/usr/local/mysql

如果不进行 ./configure –prefix=软件路径；设置的操作程序会自动安装分配文件，会把主文件放在/usr /local/bin文件夹，
配置文件放在/usr/local/etc文件夹，库文件放在/usr/local/lib文件夹，其他的公共资源文件放在/usr /local/share文件夹，
不利于后期维护。当你要卸载的时候就要一个一个目录去找，很容易删错文件。
所以在软件编译安装的时候最好通过配置指定软件的安装文件夹操作，这样软件所有文件都会在指定的文件夹中了。

```



##  Linux 查看路径

​	centos系统怎么查看软件安装路径？

```bash
# 1、通过rpm查看
查看软件是否安装。首先我们需要查看软件是否已经安装，或者说查看安装的软件包名称。如查找是否安装mysql
可以直接使用  rpm -qal |grep mysql 查看mysql所有安装包的文件存储位置
rpm -qal |grep mysql
# 2、yum查找
除了rpm 查询还可以通过yum search 查找对应可以安装的软件包
yum search mysql
# 3、其他查找方法
除了根据软件包来找文件位置之外，最常用的就是通过find查找某个关键字比如mysql所有包含mysql服务的文件路径
find / -name mysal
# 4、Which查找命令
Which命令是通过 PATH环境变量查找可执行文件路径，用于查找指向这个命令所在的文件夹
Which  mysql
# 5、Whereis查找命令
Whereis命令和find类似，不过不同的是whereis是通过本地架构好的数据库索引查找会比较快。如果没有更新到数据库里面的文件或命令则无法查找到信息
Whereis mysql
```



**注意：python多版本要单独安装pip，否则无法导致安装包找不到的错误发生**

```bash
# 单独安装的python对应版本的pip安装，查看是否安装
find / -name "pip*"
/usr/local/python2.7.12/bin/pip
/usr/local/python2.7.12/bin/pip2
/usr/local/python2.7.12/bin/pip2.7

# 上面的pip已经安装过了，没有安装的按照如下步骤执行安装即可。
mkdir /usr/local/python2.7.12
yum install openssl openssl-devel zlib-devel gcc -y
./configure --prefix=/usr/local/python2.7.12 --with-zlib
make
make install
mv /usr/bin/python /usr/bin/python2.6.6
ln -s /usr/local/python2.7.12/bin/python2.7 /usr/bin/python
python -V

# 安装最新版本的pip
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py
# python2版本的pip安装
python2 get-pip.py
# python3版本的pip安装
python3 get-pip.py

# python2和python3安装完成后，安装不了下会有pip的目录
/usr/local/bin/pip
/usr/local/bin/pip2
/usr/local/bin/pip2.7
/usr/local/bin/pip3
/usr/local/bin/pip3.6

# 注意，若python安装pip后无法使用，多版本导致包找不到，需要重新编译python安装，然后按照上面的教程按照pip即可解决python包导入找不到模块的问题。
```





## 参考博文

```bash
# 超详细的 Linux CentOS 编译安装python3
1. https://www.cnblogs.com/sunshine-H/p/8117968.html
# CentOS6.5安装Python2.7和Pip
2. http://bicofino.io/2014/01/16/installing-python-2-dot-7-6-on-centos-6-dot-5
# python3中pip3安装出错，找不到SSL
3. https://blog.csdn.net/jeryjeryjery/article/details/77880227
# Python 2.7.12并安装最新pip
4. https://blog.csdn.net/WHACKW/article/details/78141094
```

