---
title: "centos下flask web应用部署"
date: 2019-03-11 09:25:00
author: empathy
img: /resources/images/20190314.jpg
top: true
cover: true
coverImg: /resources/images/20190314.jpg
password: 
toc: true
mathjax: false
summary: "centos下flask web应用部署"
categories: Python
tags: 
  - Python
  - Flask
---



# centos下flask web应用部署



## 前言

​	使用`runserver`可以使我们的`flask`项目很便捷的在本地运行起来，但这只能在局域网内访问，如果在生产环境部署`flask`，就要多考虑一些问题了。比如静态文件处理，安全，效率等等，本篇文章总结归纳了一下基于uwsgi+Nginx下flask项目生产环境的部署。

**nginx简介:**

nginx是一个HTTP服务器,也是一个反向代理服务器，由基础模块、核心模块、第三方模块构成，与Apache相比更轻量级，占用更少的内存及资源。

**WSGI/uWSGI/uwsgi区别:**

`WSGI（Python Web Server GateWay Interface）`: WSGI是一种Web服务器网关接口。它是一个Web服务器（如nginx）与应用服务器（如uWSGI服务器）通信的一种规范。由于WSGI的通用性，出现了独立的WSGI程序，例如uWSGI和Apacke的mod_wsgi。

`uWSGI`: 是一个Web服务器，它实现了WSGI协议、uwsgi、http等协议。用于接收前端服务器转发的动态请求并处理后发给 web 应用程序。

`uwsgi`: 是`uWSGI`服务器实现的独有的协议。



**Nginx/uWSGI/Django项目工作流程:**

1. 用户通过浏览器发出http请求到服务器。
2. nginx负责接受外部http请求并进行解包，若请求是静态文件则根据设置好的静态文件路径返回对应内容。请求是动态内容则将请求交给uWSGI服务器（nginx和uWSGI使用端口或socket通信）。
3. uWSGI服务器收到请求后，根据wsgi协议解析并回调Django应用。
4. Django应用则根据请求进行数据库增删查改和模版渲染等工作，然后再逆方向返回nginx。
5. nginx将响应交付用户浏览器



## 环境配置

### 虚拟环境安装

​	`Centos7`下使用`virtualeanv`、`virtualwrapper`创建`Python`独立虚拟环境。优点：

- 使不同的应用开发环境独立
- 环境升级不影响其他应用，也不会影响全局的python环境
- 它可以防止系统出现包管理混乱和版本的冲突

```sh
# 安装步骤
# 1. 安装
pip install virtualenv
# 2.创建虚拟环境，虚拟环境目录
virtualenv py27env 
# 3. 启动虚拟环境
cd py27env
source ./bin/activate
# 4. 退出虚拟环境
deactivate
# 5. 安装包
pip install uwsgi
pip install flask
```



```sh
# 配置环境变量，编辑.bash_profile文件
# 安装virtualenvwrapper,virtualenv扩展
pip install virtualenvwrapper
mkdir -p ~/Envs

# 配置环境变量，设置virtualenvwrapper
用vi打开~/.bashrc
# 虚拟环境
# export WORKON_HOME=~/Envs
export WORKON_HOME=~/.virtualenvs
# 指定python版本
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
# 配置安装virtualenvwrapper相关信息
source /usr/bin/virtualenvwrapper.sh
# 使配置生效
source ~/.bash_profile
```



**virtualenvwrapper其他操作**

```sh
# 退出虚拟环境
deactivate

# 查看当前有哪些虚拟环境
workon

# 进入指定的虚拟环境 workon [虚拟环境名]
workon test2

# 在指定虚拟环境安装包
# 1. 进入指定虚拟环境
# 2. 查看当前虚拟环境中有哪些安装包
# 3. 安装包
workon test2
pip list
pip install requests

# 卸载包
pip uninstall requests
```



```sh
# python2和python3同时存在一台电脑，指定python版本解决方法
mkvirtualenv --python=python版本的路径 虚拟环境名称
mkvirtualenv --python=python2 demo2
mkvirtualenv --python=python3 demo3

# 所有的命令可使用：virtualenvwrapper --help 进行查看，这里列出几个常用的：
创建基本环境：mkvirtualenv [环境名]
删除环境：rmvirtualenv [环境名]
激活环境：workon [环境名]
退出环境：deactivate
列出所有环境：workon 或者 lsvirtualenv -b

# virtualenv 配置，编辑.bash_profile文件
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/workspace
source /usr/local/python27/bin/virtualenvwrapper.sh
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python
# 使配置生效
source ~/.bash_profile
```



### 项目结构

```sh
# tree zhiliao -L 2
# tree zhiliao -N -L 2

[root@xiaorui www]# tree zhiliao -L 2
zhiliao
├── config.ini
├── env_zhiliao
│   ├── bin
│   ├── include
│   ├── lib
│   └── lib64 -> /var/www/zhiliao/env_zhiliao/lib
├── log
│   ├── access.log
│   ├── error.log
│   └── uwsgi.log
└── zhiliao
    ├── config.py
    ├── data
    ├── decorators.py
    ├── exits.py
    ├── manage.py
    ├── migrations
    ├── models.py
    ├── mylog.py
    ├── static
    ├── templates
    ├── utils
    ├── zhiliao.py
```



### uwsgi配置

```sh
[uwsgi]

# uwsgi 启动时所使用的地址与端口
socket = 127.0.0.1:8047

# 指向网站目录
chdir = /var/www/zhiliao/zhiliao

# python 启动程序文件
wsgi-file = zhiliao.py 

# python 程序内用以启动的 application 变量名
callable = app

# python env path
virtualenv = /var/www/zhiliao/env_zhiliao

#后台启动，并把日志记录到指定文件
daemonize = /var/www/zhiliao/log/uwsgi.log

# 处理器数
processes = 2

# 线程数
threads = 2

#状态检测地址
# stats = 127.0.0.1:8049
```



### nginx配置

```sh
server{
    listen      8048;
    server_name localhost;
    charset     utf-8;

    #charset;
    access_log  /var/www/zhiliao/log/access.log;
    error_log  /var/www/zhiliao/log/error.log;
    location / {
        include       uwsgi_params;
        # 项目启动地址
        uwsgi_pass    127.0.0.1:8047;
        # 指定虚拟环境
        uwsgi_param UWSGI_PYHOME /var/www/zhiliao/env_zhiliao;
		#uwsgi_param UWSGI_PYHOME /usr/local/bin/python2.7;
		# 指定项目路径
        uwsgi_param UWSGI_CHDIR  /var/www/zhiliao/zhiliao;
        # 指定启动脚本，zhiliao.py
        uwsgi_param UWSGI_SCRIPT zhiliao:app;

    }
}
```



### 启动uwsgi容器

```sh
# 虚拟环境下配置启动应用
# 进入虚拟环境，env_zhiliao
uwsgi --ini=/var/www/zhiliao/config.ini

# 查看进程是否启动成功
ps aux | grep uwsgi
ps aux | grep zhiliao
ps -ef | grep uwsgi

# 停止uwsgi
# 批量杀死进程
ps -ef | grep uwsgi | grep -v grep | awk '{print $2}' | xargs kill -9
ps aux | grep uwsgi | grep -v grep | awk '{print $2}' | xargs kill -9
```





### 问题解决

```sh
# uwsgi安装失败解决
# 按照uwsgi失败，致命错误：Python.h：没有那个文件或目录
# 报类似下面的错误代码，多半因为python-devel没有正确安装，重新安装就好了
在安装uwsgi 以前，请确保libxml2，gcc，python-devel正确安装
yum -y intsall libxml gcc python-devel

#Python uwsgi 无法安装以及编译报错的处理方式
之前安装uwsgi的时候编译一步有出错，因为比较早，部分错误代码已经找不到了，网上找了部分错误信息， 现把解决方式共享出来。

# 重新安装uwsgi即可
pip intsall uwsgi
```



### 总结

```sh
# centos 环境下多个python版本共存，注意不要影响yum的python版本，否则坑很大。
CentOS 7.2 默认安装了python2.7.5 因为一些命令要用它比如yum 它使用的是python2.7.5。

# CentOS7.2 多个python版本共存
https://www.cnblogs.com/sanduzxcvbnm/p/5984352.html

# Centos7下安装多python版本并存开发环境
https://blog.csdn.net/anderslu/article/details/71336210
```



## 参考博文

```sh
# Centos7下使用virtualeanv、virtualwrapper创建Python独立虚拟环境
1. https://segmentfault.com/a/1190000009168338
# virtualenv和virtualenvwrapper介绍、安装和使用
2. https://segmentfault.com/a/1190000012030061
# python环境神器virtualenvwrapper安装与使用
3. https://segmentfault.com/a/1190000014935970
# 快速部署Python应用：Nginx+uWSGI配置详解
4. https://www.cnblogs.com/catkins/p/5270437.html
# Django + Uwsgi + Nginx 的生产环境部署
5. https://www.cnblogs.com/chenice/p/6921727.html
# 从零开始搭建个人网站（6）nginx+uWSGI部署Django项目（上）
6. https://blog.csdn.net/zuimrs/article/details/79091518
# uWSGI+Nginx安装、配置
7. https://www.cnblogs.com/configure/p/6401695.html
```

