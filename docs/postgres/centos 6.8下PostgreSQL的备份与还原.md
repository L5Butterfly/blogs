---
title: "Centos 6.8下PostgreSQL数据库的备份与还原"
date: 2019-06-05 09:25:00
author: empathy
img: /resources/images/2019060501.jpg
top: true
cover: true
coverImg: /resources/images/2019060501.jpg
password: 
toc: true
mathjax: false
layout: post
summary: "Centos 6.8下PostgreSQL数据库的备份与还原"
categories: PostgreSQL
tags: [Centos,PostgreSQL]
keywords: Centos,PostgreSQL
comments: true
description: Centos 6.8下PostgreSQL数据库的备份与还原
---



# Centos 6.8下PostgreSQL数据库的备份与还原

`标签：` postgresql



## 1.  pg_dump和psql命令 

```bash
# 完整数据库备份
pg_dump -h 127.0.0.1 -p 5432 -U postgres -d demo > /home/demo.bak
# 完整数据库还原
psql -h localhost -p 5432 -U postgres -d demo < /home/demo.bak

# 备份还原命令的说明：
pg_dump -h localhost -p 5432 -U postgres databasename >  /home/databasename.bak 
-h：数据库服务器地址；
-p：数据库端口号；
-U：U 大写,表示用户名；
-d：数据库名称；
-f：把备份文件放在哪里；

# windows系统:
psql -h localhost -p 5432 -U postgres -d new_db  -f "C:/emoneysit.bak"
# linux系统:
psql -h localhost -p 5432 -U emmweb -d emmweb < /home/jianghai/Desktop/emmweb.bak 
-h：数据库服务器地址；
-p：数据库端口号；
-U：U 大写,表示用户名；
-d：数据库名称；
-f：备份文件路径以及备份文件名称；

```







## 2. pg_dump和pg_restore命令

```bash
# 操作命令：
pg_dump -U postgres -d demo -C -F c -f demo.sql
-C 表示创建该数据库本身。
-F c 表示给pg_restore还原的格式格式，默认表示纯文本格式。
-f 指定输出文件名称。
-a 表示只备份表的内容。
-t 指定表名。
-s 表示只备份表结构。


# 1. 备份整个数据库：（备份表结构和表数据）
pg_dump -U postgres -d demo -C -F c -f /home/demo.sql
# 1. 恢复整个数据库：
pg_restore -U postgres -d demo -C -F c /home/demo.sql

# 2. 备份一张表数据：(只备份表数据，不备份表结构)
pg_dump -U postgres -d demo -a -F c -t tmp_1 -f /home/tmp_1.sql
# 2. 恢复一张表数据：
pg_restore -U postgres -d demo -F c -a /home/tmp_1.sql

# 3. 备份整个表结构：(只备份表结构，不备份表数据)
pg_dump -U postgres -d demo -F c -s -f /home/demo_struct.sql
# 3. 恢复整个表结构：
pg_restore -U postgres -d demo -F c -s /home/demo_struct.sql


# 单张表数据备份，好处，当单张表数据存在，还原表数据不会覆盖之前的数据，会在之前的数据上最近备份的数据。
# 应用场景：接吧数据库部分表某几天的数据丢失，从日志还原丢失数据，从备库导出单张表数据恢复历史数据。


```





## 3. 实际应用

```bash
# 【实战操作应用】
# 1. 备份单张表数据：
注意：导出的表数据为二进制文件数据，不能直接查看文件数据。
cd /usr/local/postgresql/bin/   切换到postgres的程序命令下：
pg_dump -h localhost -p 8432 -U postgres -d jieba_bak -a -F c -t distributor_info -f /mnt/data_backup/data_backup/pg_data_backup/distributor_info.sql

pg_dump -h localhost -p 8432 -U postgres -d jieba_bak -a -F c -t product_info -f /mnt/data_backup/data_backup/pg_data_backup/product_info.sql

pg_dump -h localhost -p 8432 -U postgres -d jieba_bak -a -F c -t order_summary_log -f /mnt/data_backup/data_backup/pg_data_backup/order_summary_log.sql

pg_dump -h localhost -p 8432 -U postgres -d jieba_bak -a -F c -t month_order_summary_log -f /mnt/data_backup/data_backup/pg_data_backup/month_order_summary_log.sql

pg_dump -h localhost -p 8432 -U postgres -d jieba_bak -a -F c -t total_summary_log -f /mnt/data_backup/data_backup/pg_data_backup/total_summary_log.sql


# 还原表数据(清空表数据，追加表数据)
注意：存在主键序列重复，还原单张表数据会失败。
pg_restore -h localhost -p 8432 -U postgres -d jieba_bak -F c -a /mnt/data_backup/data_backup/pg_data_backup/distributor_info.sql

pg_restore -h localhost -p 8432 -U postgres -d jieba_bak -F c -a /mnt/data_backup/data_backup/pg_data_backup/product_info.sql

pg_restore -h localhost -p 8432 -U postgres -d jieba_bak -F c -a /mnt/data_backup/data_backup/pg_data_backup/order_summary_log.sql

pg_restore -h localhost -p 8432 -U postgres -d jieba_bak -F c -a /mnt/data_backup/data_backup/pg_data_backup/month_order_summary_log.sql

pg_restore -h localhost -p 8432 -U postgres -d jieba_bak -F c -a /mnt/data_backup/data_backup/pg_data_backup/total_summary_log.sql


# pg_dump可以对针对单表或者多表进行备份
# 注意：备份的文件包括表数据和表结构，表结构存在也可以还原，不会报错。

# 备份单张表或者多张表数据：
pg_dump -h localhost -p 8432 -U postgres -d jieba_bak -t distributor_info -t product_info > /mnt/data_backup/data_backup/pg_data_backup/base.sql

# 还原单张表或者多张表数据的2种方式：
psql-h localhost -p 8432 -U postgres -f /mnt/data_backup/data_backup/pg_data_backup/base.sql jieba_bak
psql-h localhost -p 8432 -U postgres -d jieba_bak < /mnt/data_backup/data_backup/pg_data_backup/base.sql 

# pg_dump只能备份单个数据库，而且恢复的时候需要创建空数据库。
# pg_dumpall可以备份所有数据库，并且备份角色、表空间。

```





## 4.  pg纯文件格式的备份还原

```bash

# 1. 只导出postgres数据库的数据，不包括模式 -s
pg_dump -U postgres -f /postgres.sql -s postgres(数据库名)

# 2. 导出postgres数据库（包括数据）
pg_dump -U postgres -f /postgres.sql  postgres(数据库名)

# 3. 导出postgres数据库中表test01的数据
create database "test01" with owner="postgres" encoding='utf-8';(单引号，双引号不能错)
pg_dump -U postgres -f /postgres.sql -t test01 postgres(数据库名)

# 4. 导出postgres数据库中表test01的数据,以insert语句的形式
pg_dump -U postgres -f /postgres.sql -t test01 --column-inserts postgres(数据库名)

# 5. 恢复数据到bk01数据库
psql -U postgres -f /postgres.sql bk01
```





## 5. 参考博文

```bash
# PostgreSQL pg_dump&psql 数据的备份与恢复
1. https://www.cnblogs.com/chjbbs/p/6480687.html
# PostgreSQL备份与恢复示例
2. https://blog.csdn.net/licheng6302/article/details/7567523
# postgresql数据库备份和恢复
3. https://www.cnblogs.com/xiaofoyuan/p/5253332.html
# Postgresql备份与还原命令pg_dump
4. https://blog.csdn.net/timo1160139211/article/details/78171272
# postgresql数据库备份与恢复(单表备份)
5. https://www.2cto.com/database/201703/610140.html

```







