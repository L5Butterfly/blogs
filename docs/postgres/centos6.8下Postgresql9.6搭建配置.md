---
title: "Centos6.8下Postgresql9.6安装与配置"
date: 2019-03-11 09:25:00
author: empathy
img: /resources/images/20190311.jpg
top: true
cover: true
coverImg: /resources/images/20190311.jpg
password: 
toc: true
mathjax: false
summary: "Centos6.8下Postgresql9.6安装与配置"
categories: PostgreSQL
tags:
  - Centos
  - PostgreSQL
---



# Centos6.8下Postgresql9.6安装与配置



## 1. 环境介绍

```sh
系统：Centos7
数据库：Postgresql9.6
```



## 2. 安装步骤

### 2.1 安装依赖包

```bash
# 安装基础依赖包，用于编译安装的基础环境
yum -y install gcc*
yum -y install readline-devel
yum -y install zlib-devel
```



###  2.2 源码包获取 

```bash
# 使用wget工具拉取软件源码包
wget http://ftp.postgresql.org/pub/source/v9.6.6/postgresql-9.6.6.tar.gz
```



### 2.3 解压源码包 

```bash
# tar zxf 包名称 进行解压
tar zxf postgresql-9.6.6.tar.gz
```



### 2.4  添加用户管理pg

```bash
# 创建用户postgres、设置密码 postgres
adduser postgres
passwd postgres
# 根据提示输入用户密码
```



### 2.5 编译安装 

```bash
# cd 到解压的安装包目录进行编译安装包
cd postgresql-9.6.6
# 指定包的安装路径
./configure --prefix=/home/postgres/pgsql
# 编译安装包即可
gmake
gmake install
```



### 2.6  设置环境变量

```bash
#  设置系统环境变量
vim /etc/profile
vi /etc/profile

# 添加一行软件的安装路径信息，信息如下
PATH=$PATH:$HOME/bin:/home/postgres/pgsql/bin

# 保存退出，使用source命令是文件立即生效
source /etc/profile
```



### 2.7  创建数据库目录

```bash
 
# 切换到postgre用户，初始化数据库
su - postgres
# 指定数据库数据文件目录进行初始化设置
/home/postgres/pgsql/bin/initdb -D /home/postgres/pgsql/data
# 退出postgres用户，切换到root用户
exit
```



### 2.8 初始化数据库 

```bash
# 切换到postgre用户，初始化数据库
su - postgres
# 指定数据库数据文件目录进行初始化设置
/home/postgres/pgsql/bin/initdb -D /home/postgres/pgsql/data
# 退出postgres用户，切换到root用户
exit
```


### 2.9 编译启动命令 

```bash
# 从postgres解压后的文件夹里拷贝linux到/etc/init.d/下
# 注意：/etc/init.d/目录是相关服务的启动目录存放文件信息，启动配置文件

# 拷贝linux到/etc/init.d/目录下并重命名postgresql
cp /root/postgresql-9.6.6/contrib/start-scripts/linux /etc/init.d/postgresql

# 编辑脚本postgresql信息
vim /etc/init.d/postgresql
vi /etc/init.d/postgresql

# 编辑内容主要是修改下面两行即可。
# pg的安装目录
prefix=/home/postgres/pgsql
# pg的数据存放目录
PGDATA="/home/postgres/pgsql/data"

# 保存退出,为postgresql添加可执行权限
chmod +x /etc/init.d/postgresql
```



### 2.10 启动数据库

```bash
# 使用postgresql脚本呢启动postgres数据库
/etc/init.d/postgresql start
# 重启服务
/etc/init.d/postgresql restart
```



### 2.11 配置服务器自动启动

```bash
# 注意：postgresql启动后就可以利用servicepostgresql start、restart或者stop来控制它了。
# 初始化数据库：/etc/init.d/postgresql initdb
# 启动数据库：/etc/init.d/postgresql start
# 停止数据库：/etc/init.d/postgresql stop

1.把postgresql加入自启动列表
cd /etc/init.d
chkconfig --add postgresql

1.查看一下自启动列表：chkconfig --list。
```



### 2.11 测试数据库

```bash
# [root@MidApp ~]#su - postgres
[postgres@MidApp ~]$ psql
psql (9.6.6)
Type "help" for help.
postgres=# \l
                                  List of databases
   Name    |  Owner   | Encoding |   Collate   |    Ctype    |   Access privileges   
-----------+----------+----------+-------------+-------------+-----------------------
 postgres  | postgres | UTF8     | en_US.utf-8 | en_US.utf-8 | 
 template0 | postgres | UTF8     | en_US.utf-8 | en_US.utf-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.utf-8 | en_US.utf-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
(3 rows)
postgres=# \q
```



## 2. PostgreSQL 允许远程访问设置方法

### 2.1  配置对数据库的访问权限

```bash
# 安装PostgreSQL数据库之后，默认是只接受本地访问连接。如果想在其他主机上访问PostgreSQL数据库服务器，就需要进行相应的配置。
# 配置远 程连接PostgreSQL数据库的步骤很简单，只需要修改data目录下的pg_hba.conf和postgresql.conf。
1. pg_hba.conf：配置对数据库的访问权限，
2. postgresql.conf：配置PostgreSQL数据库服务器的相应的参数。

# 配置的步骤：
# 1.找到配置文件所在路径
find / -name  pg_hba.conf
cd /home/postgres/pgsql/data/pg_hba.conf
# 2.编辑pg_hba.conf文件
vi /home/postgres/pgsql/data/pg_hba.conf
# 3.修改pg_hba.conf文件，配置用户的访问权限，在末尾追加如下内容即可，然后保存退出。
host    all             all             0.0.0.0/0               md5 

# 注意：这个文件最后有一个列表，它决定了分派了每一个用户的权限，以及认证方式。格式是“Type Database User Address Method”，要注意的是method最好写md5
```



### 2.2 配置PostgreSQL数据库服务器的相应的参数

```bash
# 修改postgresql.conf文件，将数据库服务器的监听模式修改为监听所有主机发出的连接请求。

# 操作步骤：
# 1.找到配置文件所在路径
find / -name  postgresql.conf
cd /home/postgres/pgsql/data/postgresql.conf
# 2.编辑postgresql.conf文件
vi /home/postgres/pgsql/data/postgresql.conf
# 3. 修改内容如下：
将listen_addresses前的#去掉，并将listen_addresses ='localhost'改成listen_addresses = '*'
将listen_port前的#去掉,可以修改数据库的端口。

# 保存退出即可

# 查看post相关进程信息
ps -ef|grep post

# 重启服务以使设置生效
/etc/init.d/postgresql restart
```



### 2.3  远程链接不上问题汇总

```bash
#  防火墙开启，需求关闭（iptables 的基本操作）：
# 查看当前防火墙状态
$ service iptables status
# 查看规则
$ service iptables 
# 关闭停止服务
$ service iptables stop
# 开启服务
$ service iptables start
# 重启服务
$ service iptables restart
```



## 3. 登录数据库相关配置

### 3.1 修改数据库密码

```bash
# PostgreSQL 数据库默认会创建一个postgres的数据库用户作为数据库的管理员，默认密码为空，我们需要修改为指定的密码，这里设定为’postgres’.
# 在控制台输入以下命令：
$ su - postgres
$ psql
# 修改postgres用户密码用于远程登录
$ ALTER USER postgres WITH PASSWORD 'postgres';
# 查询数据库
$ select * from pg_shadow ;
# 创建数据库
$ create database demo;
# 链接demo数据库
$ \c demo;
$ david=# 写SQL语句
```



### 3.2 Postgresql控制台命令

```bash
# Postgresql的数据库操作基本命令总结：
$ \q：命令退出控制台
$ \password：为XXX用户设置一个密码。
$ \h：查看SQL命令的解释，比如\h select。
$ \?：查看psql命令列表。
$ \l：列出所有数据库。
$ \c [database_name]：连接其他数据库。
$ \d：列出当前数据库的所有表格。
$ \d [table_name]：列出某一张表格的结构。
$ \du：列出所有用户。
$ \e：打开文本编辑器。
$ \conninfo：列出当前数据库和连接的信息。
```



## 4. 参考博文

```bash
# Centos6.6下Postgresql9.6.6安装与配置
1. http://blog.51cto.com/qingmiao/2046357
# 远程链接配置
2. https://blog.csdn.net/yuxuan_08/article/details/51428688
```

