---
title: "centos6.8 下postgresql数据路径迁移教程"
date: 2019-06-05 09:25:00
author: empathy
img: /resources/images/2019060503.jpg
top: true
cover: true
coverImg: /resources/images/2019060503.jpg
password: 
toc: true
mathjax: false
layout: post
summary: "centos6.8 下postgresql数据路径迁移教程"
categories: PostgreSQL
tags: [Centos,PostgreSQL]
keywords: Centos,PostgreSQL
comments: true
description: centos6.8 下postgresql数据路径迁移教程
---



# centos6.8 下postgresql数据路径迁移教程

`标签：postgresql迁移`

`时间：2018-03-21`





### 前言

```bash
迁移方法有两种：
（1）重新初始化postgresql数据库，初始化时指定新的数据路径---PGDATA，然后在新的环境下将原有的数据库备份恢复一下。不过这个方法有点麻烦
（2）直接将现有的数据库文件全部拷贝到新的数据库路径下，然后重起数据库服务
第二种方法比较简单，因此，就详细描述一下第二种方法：

```



### 迁移方案

```bash
# postgresql数据迁移：
1. 数据文件迁移方式：直接将现有的数据库文件全部拷贝到新的数据库路径下，然后重起数据库服务。

# pg安装目录
/usr/local/postgresql
/usr/local/postgresql/bin/postgres

# pg数据目录
/var/postgresql

# pg的备份目录
/home/postgres/data_dump

```



```bash
pg数据文件路径：/var/postgresql/data

# 创建迁移数据磁盘：home/400G磁盘
mkdir -p /mnt/postgresql/data

# 拷贝原始数据文件到迁移文件路径
/*
cp -rf /var/postgresql/data/* /mnt/postgresql/data    
*/

# 赋予postgres权限
sudo chown -R postgres:postgres postgresql

# 查看进程
ps -ef | grep postgres
```



````bash
# 用root用户，要cdpg程序下执行如下命令：
kill -9 pid
./pg_ctl -D /var/postgresql/data -l /var/postgresql/logfile stop
./pg_ctl -D /var/postgresql/data -l /var/postgresql/logfile start
./pg_ctl -D /var/postgresql/data -l /var/postgresql/logfile restart
./pg_ctl -D /home/postgresql/data -l /home/postgresql/logfile restart

# 切换postgres用户操作psql命令：
ps -ef|grep postgres   // 查看pg进程
kill -9 pid   //关掉postgres的历史启动进程
pg_ctl stop   //停止服务
pg_ctl -D /home/postgresql/data -l /home/postgresql/logfile start   // 使用新数据库路径启动服务
ps -ef|grep postgres   // 查看pg进程师傅启动成功
pg_ctl -D /home/postgresql/data -l /home/postgresql/logfile restart  // 重启服务
pg_ctl  restart    // 默认使用初始使用的数据路径

````



```bash
# 查找pgsql启动和关闭程序路径
find / -name pg_ctl
/usr/local/postgresql/bin/pg_ctl

#  切换postgres用户操作
[root@dongshuo postgresql]# su - postgres
# 停止postgresql服务
pg_ctl stop
/usr/local/postgresql/bin/pg_ctl stop

################################################
# 导入环境变量，配置数据库路径,修改数据库的配置路径
vi ~postgres/.bash_profile
添加：
/*
PATH=$PATH:$HOME/bin
export PATH
# pg数据库安装路径
export PGHOME=/usr/local/postgresql
# pg数据库数据存放路径
export PGDATA=/mnt/postgresql/data
export PATH=$PGHOME/bin:$PATH
export MANPATH=$PGHOME/share/man:$MANPATH
export LANG=en_US.utf8
export DATE=`date +"%Y-%m-%d %H:%M:%S"`
export LD_LIBRARY_PATH=$PGHOME/lib:$LD_LIBRARY_PATH
alias rm='rm  -i'
alias ll='ls -lh'
#alias pg_start='pg_ctl start -D $PGDATA'
#alias pg_stop='pg_ctl stop -D $PGDATA -m fast'
*/

# 修改postgres 目录环境变量文件，初始文件立即生效
source .bash_profile

################################################
#  开启postgresql服务
su - postgres
pg_ctl start 
pg_ctl start -D /mnt/postgresql/data -l /mnt/postgresql/logfile start
/usr/local/postgresql/bin/pg_ctl start -D /mnt/postgresql/data -l /mnt/postgresql/logfile start

#  开启postgresql服务
pg_ctl restart
/usr/local/postgresql/bin/pg_ctl restart

# 查看端口
netstat -apn|grep 8432

################################################
# 切换postgres用户操作psql命令：
# 查看pg进程
ps -ef|grep postgres
# 关掉postgres的历史启动进程
kill -9 pid
# 停止服务
pg_ctl stop
# 使用新数据库路径启动服务
pg_ctl -D /mnt/postgresql/data -l /mnt/postgresql/logfile start
#  查看pg进程师傅启动成功
ps -ef|grep postgres
# 重启服务
pg_ctl -D /mnt/postgresql/data -l /mnt/postgresql/logfile restart
#  默认使用初始使用的数据路径
pg_ctl  restart
```



```bash
# 实际操作步骤：
mkdir postgresql
cp -R /var/postgresql/* postgresql
chown -R postgres:postgres postgresql
su - postgres
pg_ctl stop
pg_ctl -D /mnt/postgresql/data -l /mnt/postgresql/logfile start

pg_basebackup -D /pgdata/data -Fp -Xs -v -P -h 192.168.99.70 -U repuser -p 8432
```





### 参考资料

```bash
# PostgreSQL修改数据库目录/数据库目录迁移
https://www.cnblogs.com/EasonJim/p/9052836.html
# postgresql 数据库路径迁移
https://www.cnblogs.com/littlewrong/p/9064185.html
```

