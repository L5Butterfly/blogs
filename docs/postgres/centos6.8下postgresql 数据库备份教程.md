---
title: "centos 6.8下postgresql 数据库备份教程"
date: 2019-06-05 09:25:00
author: empathy
img: /resources/images/2019060502.jpg
top: true
cover: true
coverImg: /resources/images/2019060502.jpg
password: 
toc: true
mathjax: false
layout: post
summary: "centos 6.8下postgresql 数据库备份教程"
categories: PostgreSQL
tags: [Centos,PostgreSQL]
keywords: Centos,PostgreSQL
comments: true
description: centos 6.8下postgresql 数据库备份教程
---



# centos 6.8下postgresql 数据库备份教程

`标签：postgresql备份`



## 1. postgresql备份

```bash
# 备份还原数据库
psql -h localhost -U postgres -p 8432 -d jieba_bak  -f /mnt/data_bak/data_backup/day_data/jieba.2018-07-17.dump

# 备份数据
pg_dump -h localhost -U postgres -p 8432 -d jieba_0417 > /mnt/data_bak/data_backup/day_data/jieba.2018-07-02.dump
# 压缩备份
tar zcvf "/mnt/data_bak/data_tarfile/jieba.2018-07-02.tar.gz" /mnt/data_bak/data_backup/day_data/jieba.2018-07-02.dump
# 删除备份文件
rm -rf /mnt/data_bak/data_backup/day_data/jieba.2018-07-01.dump
# 删除备份数据库
/usr/local/postgresql/bin/dropdb -U postgres -p 8432 jieba_bak
# 创建数据库
/usr/local/postgresql/bin/createdb -U postgres -p 8432 jieba_bak 
# 还原数据库
psql -h localhost -U postgres -p 8432 -d jieba_bak  -f /mnt/data_bak/data_backup/day_data/jieba.2018-07-02.dump
# 远程拷贝数据库
scp -P 1022 /mnt/data_bak/data_backup/day_data/jieba.2018-07-02.dump root@192.168.99.71:/mnt/data_backup/pg_data_backup
```



## 2. postgresql备份脚本

```bash
#!/bin/bash

log="/mnt/data_bak/log/bak.log"
starttime=`date "+%Y-%m-%d %H:%M:%S"`
cur_time=$(date '+%Y-%m-%d')
echo "data backup start at time: $starttime" > $log
sevendays_time=$(date -d -7days '+%Y-%m-%d')
yestodays_time=$(date -d -1days '+%Y-%m-%d')
echo "starting backup postgreSQL jieba database ......" >> $log
rm -rf /mnt/data_bak/data_tarfile/jieba.$sevendays_time.tar.gz
pg_dump -h localhost -U postgres -p 8432 -d jieba_0417 > /mnt/data_bak/data_backup/day_data/jieba.$cur_time.dump
tar zcvf "/mnt/data_bak/data_tarfile/jieba.$cur_time.tar.gz" /mnt/data_bak/data_backup/day_data/jieba.$cur_time.dump
echo "remove temp data bak file ......" >> $log
rm -rf /mnt/data_bak/data_backup/day_data/jieba.$yestodays_time.dump
echo "finish data backup ......" >> $log
echo "drop database jieba_bak start......" >> $log
/usr/local/postgresql/bin/dropdb -U postgres -p 8432 jieba_bak
echo "create database jieba_bak start......" >> $log
/usr/local/postgresql/bin/createdb -U postgres -p 8432 jieba_bak 
echo "create database jieba_bak end......" >> $log
echo "restore database start......" >> $log
psql -h localhost -U postgres -p 8432 -d jieba_bak  -f /mnt/data_bak/data_backup/day_data/jieba.$cur_time.dump
echo "restore database finished ......" >> $log
echo "success ok ......" >> $log
echo "transport data file 47.98.111.52 start ......" >> $log
scp -P 1022 /mnt/data_bak/data_backup/day_data/jieba.$cur_time.dump root@192.168.99.71:/mnt/data_backup/pg_data_backup
echo "transport data file 47.98.111.52 end ......" >> $log
endtime=`date "+%Y-%m-%d %H:%M:%S"`
echo "data backup finish time :$endtime......" >> $log

```





## 3.postgresql数据库连接数和状态查询

```bash
# 查看数据库的当前连接数和状态的几种方式：
# 只是能看出数据库服务是否正在运行和启动路径
pg_ctl status

# 统计当前postgresql相关进程数，在大体上可以估算数据库的连接数，非精准，但是目前最常用的
ps -ef |grep postgres |wc -l

# 包含本窗口的所有数据库连接数
SELECT count(*) FROM pg_stat_activity；

# 不包含本窗口的所有数据库连接数，其中pg_backend_pid()函数的意思是当前进程相关的后台进程ID
SELECT count(*) FROM pg_stat_activity WHERE NOT pid=pg_backend_pid();

# 数据库状态查询（类似于Oracle 的 select open_mode from v$database;）
select state from pg_stat_activity where datname = 'jieba_bak';

```





## 4. PostgreSQL 断开所有连接用户、删除数据库

```bash
# 关闭pgsql所有数据库连接数，进行数据库备份，删除操作
# 查看所有数据连接
select * from pg_stat_activity where datname = 'jieba_bak';

# 删除数据库连接
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE datname='jieba_bak' AND pid<>pg_backend_pid(); 

# 断开连接到这个数据库上的所有链接，再删除数据库,语句说明：
pg_terminate_backend：用来终止与数据库的连接的进程id的函数。
pg_stat_activity：是一个系统表，用于存储服务进程的属性和状态。
pg_backend_pid()：是一个系统函数，获取附加到当前会话的服务器进程的ID。

# PostgreSQL 断开所有连接用户、删除数据库、修改表名称
# 数据库版本：8.2
# 断开所有连接语句:
SELECT pg_terminate_backend(pg_stat_activity.procpid)  
FROM pg_stat_activity  
WHERE datname='jieba_bak' AND procpid<>pg_backend_pid();

# 数据库版本：9.2
# 断开所有连接语句：
SELECT pg_terminate_backend(pg_stat_activity.pid)  
FROM pg_stat_activity  
WHERE datname='jieba_bak' AND pid<>pg_backend_pid();

# 区别：
不同版本中表 pg_stat_activity 中的 pid 字段名称不一样
可以 select * from pg_stat_activity 查看不同版本的字段进行修改

# 删除数据库：
DROP DATABASE
# 修改数据库：
alter database "jieba_bak" rename to "jieba_test"
```



## 5. 参考博文

```bash
# PostgreSQL 断开所有连接用户
1.https://blog.csdn.net/wuuushao/article/details/53608180
# postgresql数据库连接数和状态查询
2.https://blog.csdn.net/pg_hgdb/article/details/79279535

```

