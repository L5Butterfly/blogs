---
title: "centos下ngrok内网穿透服务搭建"
date: 2019-03-09 09:25:00
author: empathy
img: /resources/images/20190315.jpg
top: true
cover: true
coverImg: /resources/images/20190315.jpg
password: 
toc: true
mathjax: false
summary: "centos下ngrok内网穿透服务搭建"
categories: Ngrok
tags: 
  - Ngrok
  - Centos
---



# ngrok内网穿透服务搭建



## 一、服务端配置

```shell
VPS：这里以阿里云ECS为例，操作系统为CentOS7（64位）。
域名：将一个域名或二级域名泛解析到VPS服务器上。
例如将*.tunnel.mydomain.com解析到VPS的IP。
要注意，此时还需要将tunnel.mydomain.com的A记录设置为VPS的IP。
```



### 1. 基础环境安装

```sh
# 1.下载ngrok源码包
# 创建名称为ngrok的目录
mkdir ngrok 
# 更新包管理器
yum update 
# 安装git
yum -y install git 
# 将ngrok源代码克隆回本地
git clone https://github.com/inconshreveable/ngrok.git ngrok2 

# 2.安装Go语言
yum -y install golang #安装go语言

# 3、设置环境变量
export GOPATH=/usr/local/ngrok/  #设置环境变量，Go语言的安装位置
export NGROK_DOMAIN="xxxx.cn"  #设置环境变量，xxx.cn为你的域名

# 注意：
NGROK_DOMAIN为外网访问的公网服务器，需要自行配置绑定公网Ip,云服务器需要开发相关的端口，否则不是后无法正常访问。

```



### 2. 生成证书

```sh
# 生成证书相关信息
cd ngrok
openssl genrsa -out rootCA.key 2048
openssl req -x509 -new -nodes -key rootCA.key -subj "/CN=$NGROK_DOMAIN" -days 5000 -out rootCA.pem
openssl genrsa -out server.key 2048
openssl req -new -key server.key -subj "/CN=$NGROK_DOMAIN" -out server.csr
openssl x509 -req -in server.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out server.crt -days 5000

```



### 3. 拷贝证书到指定位置

```sh
# 复制rootCA.pem到assets/client/tls/并更名为ngrokroot.crt
cp rootCA.pem assets/client/tls/ngrokroot.crt 
# 复制server.crt到assets/server/tls/并更名为snakeoil.crt
cp server.crt assets/server/tls/snakeoil.crt 
# 复制server.key到assets/server/tls/并更名为snakeoil.key
cp server.key assets/server/tls/snakeoil.key 
```



### 4. 生成客户端和服务端工具

 	由于go语言的特性，在编译时直接生成机器码，所以在运行过程中并不需要go的环境（非托管应用）。在ngrok目录下，运行一下命令分别生成对应的客户端与服务端。

​	生成完成后，在工作目录的bin文件夹下，产生对应的文件。以编译windows平台为例，会产生“ngrok.exe”与“ngrokd.exe”这两个文件，前者客户端，后者需要运行在公网服务器上。

​	ngrok 是客户端，ngrokd 是服务端。

```sh
#win服务端
GOOS=windows GOARCH=386 make release-server 
#win客户端
GOOS=windows GOARCH=386 make release-client
#linux服务端
GOOS=linux GOARCH=386 make release-server
#linux客户端
GOOS=linux GOARCH=386 make release-client

```



### 5. 启动ngrok服务端

```sh
# 启动命令
./ngrokd -domain=ds_ngrok.anfan.xyz -httpAddr=:8780 -httpsAddr=:8781 -tunnelAddr=:8744 -log ngrok.log

# 启动命令
./ngrokd -domain=ds_ngrok.anfan.xyz -httpAddr=:10080 -httpsAddr=:10081 -tunnelAddr=:10044 -log ngrok.log  > out.file 2>&1 &

# 后台挂起命令
nohup ./ngrokd -domain=ds_ngrok.anfan.xyz -httpAddr=:8780 -httpsAddr=:8781 -tunnelAddr=:8744 -log ngrok.log 2>&1 &

```





## 二、客户端配置

​	从服务器拷贝编译好的的客户端文件，服务器端为公网的服务器，通过ngrok源码可以编译不痛的客户端启动文件。新建一个ngrok配置文件, 配置相关的装发信息。

### 1. ngrok_cfg.yml配置

```sh
server_addr: "ds_ngrok.anfan.xyz:8744"  
trust_host_root_certs: false
log: /home/local/ngrokclient/ngrok.log
tunnels:
    postgres:
        remote_port: 8721
        proto:
            tcp: "127.0.0.1:5432"
    ssh:
        remote_port: 8722
        proto:
            tcp: "127.0.0.1:22"
    api:
        subdomain: "log"
        proto:
            http: 9092
```



### 2. 启动命令

​	使用screen后台挂起命令,首先安装screen工具，安装命令：`yum -y install screen`

```sh
# screen 后台运行的配置
screen -S ngrok
./ngrok -config=ngrok_cfg.yml start ssh postgres api
screen -ls 
screen -r ngrok

# 文件执行权限
chmod -R 777 ngrok
chmod -R 777 ngrok_cfg.yml

# 查看端口是否监听
netstat -apn| grep port
netstat -apn| grep 9092

# hexo启动后台运行
screen
hexo s
```



### 3. 单独配置

```sh
# 配置客户端,此处只是参数，可以忽略。
拷贝生成的ngrok.exe到本地计算机，并新建配置文件ngrok.cfg，内容为：

server_addr: "xxxx.cn:4443"
trust_host_root_certs: false
xxx.cn代表你的域名

再新建文件start.bat批处理文件，内容为：

ngrok.exe -subdomain hello -config=ngrok.cfg 8080
hello为子域名前缀，访问时格式为hello.xxx.cn
8080为本地tomcat服务器使用的端口号。
```



## 三、参考博文

````sh
# Ngrok配置和使用
1.https://www.jianshu.com/p/405fed75486c
# 内网穿透、反向代理（ngrok服务器搭建）
2.https://blog.csdn.net/xiahailong90/article/details/79107453
# 一分钟实现内网穿透（ngrok服务器搭建）
3. https://blog.csdn.net/zhangguo5/article/details/77848658
# 10分钟教你搭建自己的ngrok服务器
4. https://blog.csdn.net/yjc_1111/article/details/79353718
# 原 Ngrok搭建服务器
5.https://cloud.tencent.com/developer/article/1142087
````

