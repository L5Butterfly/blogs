---
title: "Java达西面试题"
date: 2019-04-16 09:25:00
author: empathy
img: /medias/images/2019041601.jpg
top: true
cover: true
coverImg: /medias/images/2019041601.jpg
password: 
toc: true
mathjax: true
summary: "Java达西面试题"
categories: Interview
tags:
  - Java
  - Interview
---



# 达西面试题



### 面试题一

**(1). 题目分析**

```bash
一奇怪的计数器（建议时间复杂度0 (logn),空间复杂度0 (1))
Bob有一个奇怪的计数器，在第一秒它显示的数字是3,之后每隔一秒计数器显示的数字都
会减1,直到1为止.
计数器的衰减是循环的，减到1之后的下一秒，计数器会重置成2 x前一个循环的初始值，然
后每隔一秒开始衰减，下图显示了前3个循环时每秒对应的计数器的值.
问题：计算在时间t(秒)时计数器显示的值.
函数原型：long strangeCounter(long t)
限制条件:l<=t<=1012
解释：t=4时是第二个周期的开始，它显示的是2x第一个周期开始值：2x3=6;
```



**(2).代码实现**

```java
/** java 代码实现 **/
package com.lemai.smspay.test;

public class DemoTest3 {

	public static void main(String[] args) {
		strangeCounter(21);
		System.out.println(strangeCounter(2147483647L));
		System.out.println(strangeCounter(4611686018427387904L));
	}
	

	/**
	 * 获取t秒对应的计数器的值
	 * @param time
	 * @return
	 */
	public static int strangeCounter(long t){
		
		//周期计数
	    int time=0;
	    //初始值，固定
	    int initTime=1;
	    int initValue=3;
	    //t秒对应的计数器值
	    int curValue =0;
	    
	    //int max=2147483647
	    //条件判断;0表示条件不符合返回的计数器值;int的范围本就小于10的10次方的，疑惑？
	    if(!(t>=1 && t<=Math.pow(10,10))){
	    	return 0;
	    }

	    //推理计算
	    while (true) {
			if(initTime==t){
				curValue=initValue;
				break;
			}
			initTime += 1;
			if (initValue == 1){
				time += 1;
				initValue = 3 * 2 * time;
			}else{
				initValue-=1;
			}		            
		}
		return curValue;
	}
}

```





### 面试题二

**(1). 题目分析**

```bash
二递归数字和（建议时间复杂度0 (n)，n为字符串长度，空间复杂度0 (1))
我们按照下述规则定义整数X的超级数字：
1. 如果X只有一位，超级数字就是X
2. 否则,x的超级数字等于x的所有数位的和的超级数字
比如9875的超级数字按照如下计算：
super_digit(9875) 9+8+7+5 = 29
super_digit(29) 2 + 9 = 11
super_digit(ll) 1 + 1 = 2
super_digit(2) =2
给定两个数字数字 p通过将n自连接k次得到.继续上面的例子n=9875,假设k=4,那
么初始时p= 9875 9875 9875 9875(加空格是为了看起来清晰点）
superDigit(p) = superDigit(9875987598759875)
5+7+8+9+5+7+8+9+5+7+8+9+5+7+8+9 = 116
superDigit(p) = superDigit(116)
1+1+6 = 8
superDigit(p) = superDigit(8)
问题：请实现函数 intsuperDigit(Stringn, intk)
n:字符串表示的整数
k:整数，将n自连接的次数
返回值：返回p的超级数字,p通过n自连接k次获得
限制条件：1 <=n < lO10000, l<=k<105
```



**(2).代码实现**

```java
/** java 代码实现 **/
package com.lemai.smspay.test;

public class DemoTest {

	public static void main(String[] args) {
		int p=superDigit("9875",4);
		System.out.println(p); 
	}
	
	
	/**
	 * 字符串数字求和
	 * @param n
	 * @return
	 */
	public static String superDigit(String Str){
		//2.合成字符串求和
		int x=0;
		char[] ar = Str.toCharArray();
		for(int i =0;i<ar.length;i++){
			x+=Integer.valueOf(String.valueOf(ar[i])); 
		}
		return String.valueOf(x);
	}
	
	/**
	 * 问题：实现函数
	 * @param n:字符串整数
	 * @param k：将n自连接次数
	 * @return 返回p的超级数字，p通过n自连接k次获得
	 * 限制条件：1<=n<=10的10000次方;1<=k<10的5次方
	 */
	public static int superDigit(String n,int k){
		
		//Infinity无穷大
		System.out.println(Math.pow(10,10000));
		//限制条件;对条件有疑惑???
		if(!(Integer.valueOf(n)>=1 && Integer.valueOf(n)<Math.pow(10,10000) && k>=1 && k<Math.pow(10,5))){
			return 0;
		}
		
		//1.数字自连接拼接
		String knStr="";
		for (int i = 0; i < k; i++) {
			knStr+=n;
		}
		String xNum=superDigit(knStr);
		//2.递归计算是否超级数
		while (xNum.length()!=1) {
			xNum=superDigit(xNum);
		}
		return Integer.valueOf(xNum);
	}
	
	
	/**
	 * Java中char与String的区别
	 * @return
	 */
	public String StringAndChar(){
		return "";
	}
}

```





### 面试题三

**(1). 题目分析**

```bash
三两个字符（建议时间复杂度0 (n3 )、空间复杂度0 (1),
欢迎时间复杂度为0 (n2 )和空间复杂度0 (n2 )的方案）：
在这个试题中，你会被给到一个字符串。你必须删除一些字符，直到字符串只由两个交替的
字符组成。当你选择删除一个字符，字符串中其它相同字符都必须删除。你的目标是建立一
个最长的字符串，这个字符串只由两个交替的字符组成。
举个例子，我们有个字符串"abaacdabd"。如果你删除了 a，那么剩下的字符串就是"bcdbd"。
然后再删除c，那么剩下的字符串就是我们想要的结果："bdbd"，它的长度是4。但如果删
除b或d，都无法得到我们想要的结果。
这个试题可以简单总结为：给定一个字符串s，把它转成一个由两个字符交替组成的最长字
符串t，返回t的长度。如果无法得到结果，则返回0.
实现以下方法：
入参：Strings
返回：int
int alternate(String s) {
解释
输入字符串s=beabeefeab,返回结果5.
字符串s中出现的字符是a、b、e和f。这就意味着t只能由其中两个字符组成，而必须删
除另外两个字符。组成字符串t的字符可能存在的情况：[a,b],[a,e], [a,f],[b, e], [b,f]和[e,
f]。
如果删除了 e和f，那么结果字符串就是"babab"。这个结果是正确的，因为它只包含两种
不同的字符（a和b)，而且在它们是交替出现的。
如果删除了 a和f，那么结果字符串就是"bebeeeb"。这个结果是不正确的，因为存在连续
的e字符。而如果再删除e呢，结果中就会存在连续的b，所以就无法产生正确的结果。
其它情况类似。
所以"babab"是我们可以得到的最长字符串。
```



**(2).代码实现**

```java
/** java 代码实现 **/
package com.lemai.smspay.test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DemoTest2 {

	/**
	 * 主入口
	 * @param args
	 */
	public static void main(String[] args) {
		//alterNate("beabeefeab");
		int lenth=alterNate("xyabyxabxyabyxabxyabyxabxyabyx,mn,mn,mn,mn,mn,mn,mn,mn,mn,mn,mn,mn,mn,mn");
		System.out.println(lenth);
	}
	

	/**
	 * 字符串数字求和
	 * @param n
	 * @return
	 */
	public static String superDigit(String str){
		
		//1. 先判断字符串由几个字母组成
		Set<String> strSet=new HashSet<String>();
		char[] strArray=str.toCharArray();
		for (char c : strArray) {
			strSet.add(String.valueOf(c));
		}
		
		//2. 删除一个字母,校验是否为AB交替字符串
		for (String c : strSet) {
			System.out.println(c);
		}
		//3. 循环删除字符串,符合的保存
		
		//4. 取最长的删除字母方案
		
		return "";
	}
	

	
	/**
	 * 删除后的字符串是否AB轮循
	 * 校验删除字母后的字符串是否符合
	 * @param oStr
	 * @return
	 */
	public static boolean checkValid(String oStr){
		boolean flag=true;
		char[] y=oStr.toCharArray();
		for (int i = 0; i < y.length-2; i++) {
			if(y[i]!=y[i+2]){
				flag=false;
				break;
			}
		}
		return flag;
	}
	
	
	/**
	 * 获取AB格式的字符串长度最大的字符串
	 * @param abList
	 * @return
	 */
	public static int getMaxLength(List<String> abList){
		int max=0;
		for (int i = 0; i < abList.size(); i++) {
			if(max<abList.get(i).length()){
				max=abList.get(i).length();
			}
		}
	    return max;
	}
    
	
	/**
	 * 返回删除XY字符串后的字符串;删除任意2个字符串实现
	 * @param xy (当前2个字母)
	 * @param xyList (所有2个字母组合的字符串)
	 * @param orignStr (原始字符串)
	 * @return
	 */
	public static String delOstrByXy(String xy,List<String> xyList,String orignStr){
		for (int i = 0; i < xyList.size(); i++) {
			//System.out.println("############################");
			//System.out.println(xy);
			//System.out.println(xyList.get(i));
			//System.out.println(xyList.get(i)!=xy);
			//System.out.println(!xyList.get(i).contains(xy.substring(0,1)));
			//System.out.println(!xyList.get(i).contains(xy.substring(1,2)));
			if(xyList.get(i)!=xy && !xyList.get(i).contains(xy.substring(0,1)) && !xyList.get(i).contains(xy.substring(1,2))){
				//System.out.println("##############33333##############");
				orignStr=orignStr.replace(xyList.get(i).substring(0,1),"").replace(xyList.get(i).substring(1,2),"");
				//System.out.println(orignStr);
			}
		}
		return orignStr;
	}
	
	
	/**
	 * 原始字符串
	 * @param oStr
	 * @return
	 */
	public static int alterNate(String oStr){
		//组合情况
		List<String> xyAllList=new ArrayList<String>();

		//所有符合的组合情况
		List<String> abList=new ArrayList<String>();
		
		// 字符数组
		char[] strAry = oStr.toCharArray();
		
		// 原始字符串由那些字母组成,字符不重复列表
		Set<String> strList=new HashSet<String>();
		for (Character string : strAry) {
			strList.add(String.valueOf(string));
		}
		System.out.println(strList);
		
		//Set转List
		List<String> strList3 = new ArrayList<>(strList);
		
		for (int i = 0; i < strList.size(); i++) {
			for (int j = i+1; j < strList3.size(); j++) {
				System.out.println(strList3.get(i));
				System.out.println(strList3.get(j));
				xyAllList.add(strList3.get(i)+strList3.get(j));
			}
		}
		
		System.out.println(xyAllList);
		
		// 筛选符合的字符
		for (String string : xyAllList) {
			System.out.println(string);
			String ostr=delOstrByXy(string,xyAllList,oStr);
			System.out.println(ostr);
			//判断删除后的字符串是否是AB轮循的字符串
		    if(checkValid(ostr)){
		    	abList.add(ostr);
		    }
		}
		
		System.out.println(abList);
		
		// 返回最长的轮循字符串
		return getMaxLength(abList);
		
		//jdk8特新型
		//Set<Character> xy=(Set<Character>) oStr.chars().mapToObj(c->(char)c).collect(Collectors.toList());
		//System.out.println(xy);
		//System.out.println(oStr.chars().mapToObj(c->(char)c).collect(Collectors.toList()));;
		
	}	
}

```



### 面试题扩展



```java
/** 面试题1的简单解答 **/
public Long strangeCounter1(long time) {
     String Long sum = 3;
     String Long cur = 3;
     while (sum < time) {
         cur *= 2;
         sum += cur;
     }
     return sum - time + 1;
}
```

