---
title: "Java web项目war部署Jetty容器"
date: 2019-06-02 09:21:00
author: empathy
img: /resources/images/20190602.jpg
top: true
cover: true
coverImg: /resources/images/20190602.jpg
password: 
toc: true
mathjax: false
summary: "Java web项目war部署Jetty容器"
categories: Java
tags: 
  - Jetty
  - Java
---



#  Java web项目war部署Jetty容器



### 前言

```bash
Jetty是当下非常流行的一款轻量级Java Web服务器和Servlet容器实现，它由Eclipse基金会托管，完全免费而且开放源代码，因此所有人均可以从其官网下载最新源代码进行研究。由于其轻量、灵活的特性，Jetty被广泛用于一系列知名产品，例如ActiveMQ、Maven、Spark、Google App Engine、Eclipse、Hadoop等等。下面就来研究如何安装和启动jetty。 
```



### 部署

**1. 下载jetty容器软件包**

```bash
# 官网下载jetty容器
https://www.eclipse.org/jetty/download.html

# linux下使用wget命令下载.tar.gz和.zip格式的源码
wget https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-distribution/9.4.18.v20190429/jetty-distribution-9.4.18.v20190429.tar.gz

wget https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-distribution/9.4.18.v20190429/jetty-distribution-9.4.18.v20190429.zip
```



**2. 配置jetty环境变量**

```bash
# 设置环境变量和启动项目
# 1.临时环境变量设置
set PROJECT_HOME=E:\jetty-9

# 2.永久环境变量设置
setx PROJECT_HOME E:\jetty-9

# 3.输出环境变量；查看环境变量是否正确；
echo %PROJECT_HOME%

# 4.配置文件路径；PROJECT_HOME/config
E:\jetty-9\config

# 4.修改配置文件;statr.ini
jetty.http.port=8081

# 5.jetty启动项目
# 指定端口启动项目
java -jar start.jar -Djetty.http.port=8088

# 设置是否主要进程
java -jar start.jar -Djetty.http.port=8088 -DMainProcess=true

# 指定主进程和进程名称和JVM最大内存
java -jar start.jar -Djetty.http.port=8088 -DprocessName=card -Xmx1024m -DMainProcess=true

# 6.访问项目
http://127.0.0.1:8081

```



**maven jetty指定端口启动 **

```bash
# eclipse里修改 Goals值
# 1.Tomcat
-Dmaven.tomcat.port=8080 tomcat:run
# 2.Jetty
-Djetty.port=8081 jetty:run
mvn -Djetty.port=8081 jetty:run
mvn  jetty:run
# 3.eclipse中jetty指定端口启动
jetty:run -Djetty.port=8099
```



**3. maven工具打包项目成war包进行部署**

```bash
# mvn打包应用
# 1. 打包父工程，切换到工程目录，执行打包命令
# 将父工程打包安装到本地仓库环境下
mvn install 

# 在项目target下生成jar包
mvn clean package

# 2.打包web工程，切换到工程目录，执行打包命令
# 将web工程打包安装到本地仓库环境下
mvn install 

# 在项目target下生成war包
mvn clean package
```



**4. 拷贝war到webapps目录下**

```bash
# web应用部署
1. 将打包好的应用app.war命名为ROOT.war

2.启动jetty容器
java -jar start.jar

3. 访问地址即可。
http://127.0.0.1:8080
```





### 容器对比

```bash
# Jetty和tomcat的比较
1. 相同点：
Tomcat和Jetty都是一种Servlet引擎，他们都支持标准的servlet规范和JavaEE的规范。

2. 不同点：
#架构比较 
Jetty的架构比Tomcat的更为简单 
Jetty的架构是基于Handler来实现的，主要的扩展功能都可以用Handler来实现，扩展简单。 
Tomcat的架构是基于容器设计的，进行扩展是需要了解Tomcat的整体设计结构，不易扩展。

#性能比较 
Jetty和Tomcat性能方面差异不大 
Jetty可以同时处理大量连接而且可以长时间保持连接，适合于web聊天应用等等。 
Jetty的架构简单，因此作为服务器，Jetty可以按需加载组件，减少不需要的组件，减少了服务器内存开销，从而提高服务器性能。 
Jetty默认采用NIO结束在处理I/O请求上更占优势，在处理静态资源时，性能较高
少数非常繁忙;Tomcat适合处理少数非常繁忙的链接，也就是说链接生命周期短的话，Tomcat的总体性能更高。 
Tomcat默认采用BIO处理I/O请求，在处理静态资源时，性能较差。
其它比较 
Jetty的应用更加快速，修改简单，对新的Servlet规范的支持较好。 
Tomcat目前应用比较广泛，对JavaEE和Servlet的支持更加全面，很多特性会直接集成进来。

# 部署区别
Jetty部署项目非常简单，之间将项目复制到webapps目录中等待Jetty编译即可。
jetty部署项目是不需要关掉服务，直接部署就可以。如果是war包jetty也是直接编译，和tomcat不同，jetty不会将war包解压出来。
```



**jetty9配置contextPath**

```bash
# jetty9配置contextPath 说明：

目录结构：
webapps
---demo.war
---demo.xml

# demo.xml文件内容

     <?xml version="1.0"  encoding="ISO-8859-1"?>
     <!DOCTYPE Configure PUBLIC "-//Jetty//Configure//EN" "http://www.eclipse.org/jetty/configure.dtd">
     <Configure class="org.eclipse.jetty.webapp.WebAppContext">
         <Set name="contextPath">/</Set>
         <Set name="war"><SystemProperty name="jetty.home" default="."/>/webapps/demo.war</Set>
     </Configure>

```



### 参考资料

```bash
# Linux 下Jetty多端口启动
1. https://blog.csdn.net/lansedeyouling/article/details/50678868

# Windows命令行设置永久环境变量
2. https://blog.csdn.net/su1322339466/article/details/52983052
3. http://blog.jues.org.cn/post/windows-ming-ling-xing-cmd-she-zhi-huan-jing-bian-liang-yong-jiu-sheng-xiao.html

# Maven命令行使用：mvn clean package 打包
4.https://www.cnblogs.com/frankyou/p/6062179.html

# jetty的安装和启动
5. https://blog.csdn.net/acm_lkl/article/details/78681659

# tomcat与jetty的区别
6.https://www.cnblogs.com/fengli9998/p/7247559.html
7.https://www.cnblogs.com/fengli9998/p/7247559.html

# Jetty服务怎么配置，如何发布项目
https://www.cnblogs.com/wdh1995/p/7338551.html
```

