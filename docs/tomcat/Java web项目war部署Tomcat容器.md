---
title: "Java web项目war部署Tomcat容器"
date: 2019-06-01 09:21:00
author: empathy
img: /resources/images/20190601.jpg
top: true
cover: true
coverImg: /resources/images/20190601.jpg
password: 
toc: true
mathjax: false
summary: "Java web项目war部署Tomcat容器"
categories: Java
tags: 
  - Tomcat
  - Java
---



#  Java web项目war部署Tomcat容器



### 前言

```bash
Tomcat容器属于web容器的一种，web容器还包括weblogic容器、JBoss容器等；而Tcomcat、webLogic等包含servlet容器。

这三者容器的概念：
web容器：可以部署多个WEB应用程序的环境。
Tomcat容器：是Java Servlet，JavaServer Pages，Java Expression Language和Java WebSocket（Java EE）技术的开源实现。
Servlet容器：全称server applet，意为服务程序。主要作用是给上级容器(Tomcat)提供doGet()和doPost()等方法。其生命周期实例化、初始化、调用、销毁受控于Tomcat容器。
```



### 部署

**1. 下载jetty容器软件包**

```bash
# 官网下载tomcat容器
https://tomcat.apache.org/download-80.cgi

# linux下使用wget命令下载.tar.gz和.zip格式的源码
wget http://mirrors.tuna.tsinghua.edu.cn/apache/tomcat/tomcat-8/v8.5.41/bin/apache-tomcat-8.5.41.zip

wget http://mirrors.tuna.tsinghua.edu.cn/apache/tomcat/tomcat-8/v8.5.41/bin/apache-tomcat-8.5.41.tar.gz
```



**2. 配置tomcat环境变量**

```bash
# 设置环境变量和启动项目
# 1.临时环境变量设置
set PROJECT_HOME=E:\tomcat-9

# 2.永久环境变量设置
setx PROJECT_HOME E:\tomcat-9

# 3.输出环境变量；查看环境变量是否正确；
echo %PROJECT_HOME%

# 4.配置文件路径；PROJECT_HOME/config
E:\tomcat-9\config

# 4.修改conf目录的server.xml文件;默认端口8080
<Connector port="8080" protocol="HTTP/1.1" connectionTimeout="20000" redirectPort="8443"/>

# 5.项目根路径配置；card.war放在webapps目录即可。包名为ROOT.war可以不用配置。
<Host name="localhost"  appBase="webapps" unpackWARs="true" autoDeploy="true">
	<Context path="" docBase="card" debug="0"></Context>
</Host>

# 6.tomcat启动项目
startup.bat

# 7.停止应用
shutdown.bat

# 8.访问项目
http://127.0.0.1:8080

```



**3. maven工具打包项目成war包进行部署**

```bash
# mvn打包应用
# 1. 打包父工程，切换到工程目录，执行打包命令
# 将父工程打包安装到本地仓库环境下
mvn install 

# 在项目target下生成jar包
mvn clean package

# 2.打包web工程，切换到工程目录，执行打包命令
# 将web工程打包安装到本地仓库环境下
mvn install 

# 在项目target下生成war包
mvn clean package
```



**4. 拷贝war到webapps目录下**

```bash
# web应用部署
1. 将打包好的应用app.war命名为ROOT.war

2.启动tomcat容器
start.bat

3.停止应用
shutdown.bat

3. 访问地址即可。
http://127.0.0.1:8080
```



### 容器对比

```bash
# Jetty和tomcat的比较
1. 相同点：
Tomcat和Jetty都是一种Servlet引擎，他们都支持标准的servlet规范和JavaEE的规范。

2. 不同点：
#架构比较 
Jetty的架构比Tomcat的更为简单 
Jetty的架构是基于Handler来实现的，主要的扩展功能都可以用Handler来实现，扩展简单。 
Tomcat的架构是基于容器设计的，进行扩展是需要了解Tomcat的整体设计结构，不易扩展。

#性能比较 
Jetty和Tomcat性能方面差异不大 
Jetty可以同时处理大量连接而且可以长时间保持连接，适合于web聊天应用等等。 
Jetty的架构简单，因此作为服务器，Jetty可以按需加载组件，减少不需要的组件，减少了服务器内存开销，从而提高服务器性能。 
Jetty默认采用NIO结束在处理I/O请求上更占优势，在处理静态资源时，性能较高
少数非常繁忙;Tomcat适合处理少数非常繁忙的链接，也就是说链接生命周期短的话，Tomcat的总体性能更高。 
Tomcat默认采用BIO处理I/O请求，在处理静态资源时，性能较差。
其它比较 
Jetty的应用更加快速，修改简单，对新的Servlet规范的支持较好。 
Tomcat目前应用比较广泛，对JavaEE和Servlet的支持更加全面，很多特性会直接集成进来。

# 部署区别
Jetty部署项目非常简单，之间将项目复制到webapps目录中等待Jetty编译即可。
jetty部署项目是不需要关掉服务，直接部署就可以。如果是war包jetty也是直接编译，和tomcat不同，jetty不会将war包解压出来。
```



**Tomcat9配置访问路径**

```bash
# tomcat项目去掉context前缀路径
现在一般都是一个tomcat只用于部署一个项目，所以没有必要再带上context作为前缀了。

# 方法一：修改server.xml，添加Context
 <Host name="localhost" appBase="webapps" unpackWARs="true" autoDeploy="true">
    <Context path="" docBase="card" debug="0">
    </Context>
 </Host>
重要的是docBase，填上webapps目录下的要部署的项目文件夹名 ； path设置为空
该方法缺点是，会部署2次。分别是ROOT目录一次，card 目录一次。在tomcat启动时，会首先在CATALINA_HOME/webapps/路径下创建名称为ROOT的目录，然后将文件card.war中的内容拷贝到ROOT目录下，开始部署ROOT应用程序。接下来才会解压card.war，再部署card应用。

# 方法二：修改server.xml，添加Context
经验证，在server.xml，添加Context也是可以，不过docBase要指向webapps目录外的war包（配置绝对路径）

 <Host name="localhost" appBase="webapps" unpackWARs="true" autoDeploy="true">
    <Context path="" docBase="E:/tomcat-8/app/war/card.war" debug="0">
    </Context>
 </Host>
```



### 参考资料

```bash
# Windows命令行设置永久环境变量
1. https://blog.csdn.net/su1322339466/article/details/52983052
# Maven命令行使用：mvn clean package 打包
2.https://www.cnblogs.com/frankyou/p/6062179.html
# jetty的安装和启动
3. https://blog.csdn.net/acm_lkl/article/details/78681659
# tomcat项目去掉context前缀路径
4.https://blog.csdn.net/dujianxiong/article/details/82350094
# Java Web 项目发布到Tomcat中三种部署方法
5.https://blog.csdn.net/qq_35893120/article/details/79587454
```

