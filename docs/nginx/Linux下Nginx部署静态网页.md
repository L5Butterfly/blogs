---
title: "Linux下Nginx部署静态网页"
date: 2019-03-03 09:25:00
author: empathy
img: /resources/images/20190303.jpg
top: true
cover: true
coverImg: /resources/images/20190303.jpg
password: 
toc: true
mathjax: false
summary: "Linux下Nginx部署静态网页，Nginx配置文件的配置"
categories: Nginx
tags: 
  - Nginx
  - Linux
---



# Linux下Nginx部署静态网页



## 1.安装nginx环境

```bash
# 安装环境
yum install gcc
yum install pcre-devel
yum install zlib zlib-devel
yum install openssl openssl-devel

# 在usr/local目录下新建nginx文件夹
cd /usr/local
mkdir nginx

# 进入nginx文件夹
cd nginx

# 下载nginx的tar包
wget http://nginx.org/download/nginx-1.9.7.tar.gz

# 解压tar
tar -zxvf nginx-1.9.7.tar.gz

# 安装nginx
./configure

# 执行make
make

# 执行make install
make install

# 进入nginx的sbin目录
cd sbin

# 启动nginx
sudo ./nginx

# 查询nginx.conf是否正确
/usr/local/nginx/sbin/nginx -t

# 重启nginx
/usr/local/nginx/sbin/nginx -s reload
```



## 2.部署静态网站配置

```bash
# 进入nginx目录
cd nginx

# 编辑配置文件
vim conf/nginx.conf

 server {
        listen       80;
        server_name  _;
        # 静态页面根目录
        #root /usr/local/nginx/nginx-1.9.7/web/;     
        #index index.html;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            root   /usr/local/nginx/nginx-1.9.7/web/;
            index  index.html;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
```



## 3. Nginx配置

```bash
 # 一般nginx.conf配置不要随意修改，导致配置文件混乱。
 
 # 优化配置的方案
 在conf目录下新建一个vhost文件，里面存放各个网站的和项目的配置文件
 
 # 在nginx.conf 追加配置，包含vhost下的所有配置文件
 include vhost/*.conf;
```



## 4.单独配置应用配置文件

**在vhost下修改app.conf配置文件**

```bash
server {
        listen       80;
        server_name xxx.xxx.com;
		
		# xiaorui
		root /home/app/xiaorui;
	    index index.html;
        
		# download
		location /download {
			alias /home/app/dongman;
			index index.html;
		}
		# h5
        location /h5 {
            alias /home/app/h5demo;
            index index.html;
        }
		# xiaorui
		location /rui {
            alias /home/app/xiaorui;
            index index.html;
        }
}
```



## 5.其他配置参考

```bash
server {
	# 监听80端口
	listen  80;
	# 域名地址
	server_name  www.aabbccdd.com;

	location / {
		# 项目根路径
		root /home/app/tomcat/webapps/product/views;
		# 默认请求转到root路径下的index.html页面
		index index.html;
	}
}



server {
  # 你的域名或者 ip
  server_name static.naice.me; 
  # 你的克隆到的项目路径
  root /www/static-web/static-web; 
  # 显示首页
  index index.html;
  # 静态文件访问
  location ~* ^.+\.(jpg|jpeg|gif|png|ico|css|js|pdf|txt){
    root /www/static-web/static-web;
  } 
}
```



## 6.重启Nginx服务

```bash
# 检验配置是否有效
nginx -t
# 重启nginx服务
nginx -s reload
```





## 7.root目录与alias目录的区别

```bash
# root目录与alias目录的区别
Nginx路径location配置中，使用root目录与alias目录的区别
1）alias指定的目录是准确的，即location匹配访问的path目录下的文件直接是在alias目录下查找的；
2）root指定的目录是location匹配访问的path目录的上一级目录,这个path目录一定要是真实存在root指定目录下的；

# 举例说明:
比如静态资源文件在服务器/var/www/static/目录下
1）配置alias目录
    location /static/ {
        alias  /var/www/static/;
    }

# 注意：alias指定的目录后面必须要加上"/"，即/var/www/static/不能改成/var/www/static
# 访问http://IP:PORT/static/index.html时，实际访问的是/var/www/static/index.html
    
2）也可改成配置root目录
    location /static/ {
        root  /var/www/;
    }

# 注意：location中指定的/static/必须是在root指定的/var/www/目录中真实存在的。
# 两者配置后的访问效果是一样的。 

# 配置习惯: 
一般情况下，在nginx配置中的良好习惯是：
1）在location / 中配置root目录
2）在location /somepath/ 中配置alias虚拟目录
    
# 配置默认主页:
比如访问 http://IP:PORT/，默认访问服务器/var/www/static/目录下的index.html
1）配置alias目录方式
    location / {
        alias  /var/www/static/;
        index  index.html index.htm;
    }
   
2）配置root目录方式
    location / {
        root  /var/www/static/;
        index  index.html index.htm;
    }
```



## 8. 参考资料

```bash
# 1.拿Nginx 部署你的静态网页
https://segmentfault.com/a/1190000010487262
# 2.linux nginx 部署静态网页
https://www.colabug.com/5332878.html
# 3.基于nginx的静态网页部署
https://blog.csdn.net/ljp1919/article/details/72833982
# 4.Nginx之upstream的四种配置方式
https://blog.csdn.net/qq_32625839/article/details/82184739
# 5.Nginx配置详解
https://www.cnblogs.com/knowledgesea/p/5175711.html
# 6.Nginx静态文件路径配置
https://blog.csdn.net/spark_csdn/article/details/80836326
```

