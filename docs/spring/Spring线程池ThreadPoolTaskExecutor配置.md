---
title: "Spring线程池ThreadPoolTaskExecutor配置"
date: 2019-06-03 09:25:00
author: empathy
img: /resources/images/20190603.jpg
top: true
cover: true
coverImg: /resources/images/20190603.jpg
password: 
toc: true
mathjax: false
layout: post
summary: "Spring线程池ThreadPoolTaskExecutor配置"
categories: 线程池
tags: [Spring,线程池]
keywords: Spring,线程池,ThreadPool
comments: true
description: Spring线程池ThreadPoolTaskExecutor配置
---





# Spring线程池ThreadPoolTaskExecutor配置



##  1. ThreadPoolTaskExecutor配置

```java
<!-- spring thread pool executor -->           
    <bean id="taskExecutor" class="org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor">
        <!-- 线程池维护线程的最少数量 -->
        <property name="corePoolSize" value="5" />
        <!-- 允许的空闲时间 -->
        <property name="keepAliveSeconds" value="200" />
        <!-- 线程池维护线程的最大数量 -->
        <property name="maxPoolSize" value="10" />
        <!-- 缓存队列 -->
        <property name="queueCapacity" value="20" />
        <!-- 对拒绝task的处理策略 -->
        <property name="rejectedExecutionHandler">
            <bean class="java.util.concurrent.ThreadPoolExecutor$CallerRunsPolicy" />
        </property>
    </bean>
```



### 1.2 属性字段说明

```bash
# ThreadPoolTaskExecutor配置字段说明：
1. corePoolSize：线程池维护线程的最少数量
2. keepAliveSeconds：允许的空闲时间
3. maxPoolSize：线程池维护线程的最大数量
4. queueCapacity：缓存队列
5. rejectedExecutionHandler：对拒绝task的处理策略

```





### 1.3  Execute(Runable)方法执行过程

```bash
# Execute(Runable)方法执行过程
1. 如果此时线程池中的数量小于corePoolSize，即使线程池中的线程都处于空闲状态，也要创建新的线程来处理被添加的任务。

2. 如果此时线程池中的数量等于 corePoolSize，但是缓冲队列 workQueue未满，那么任务被放入缓冲队列。

3. 如果此时线程池中的数量大于corePoolSize，缓冲队列workQueue满，并且线程池中的数量小于maxPoolSize，建新的线程来处理被添加的任务。

4. 如果此时线程池中的数量大于corePoolSize，缓冲队列workQueue满，并且线程池中的数量等于maxPoolSize，那么通过handler所指定的策略来处理此任务。也就是：处理任务的优先级为：核心线程corePoolSize、任务队列workQueue、最大线程 maximumPoolSize，如果三者都满了，使用handler处理被拒绝的任务。

4. 当线程池中的线程数量大于corePoolSize时，如果某线程空闲时间超过keepAliveTime，线程将被终止。这样，线程池可以动态的调整池中的线程数。
```



## 2. spring-task.xml配置

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context"
    xmlns:mvc="http://www.springframework.org/schema/mvc"
    xmlns:aop="http://www.springframework.org/schema/aop"
    xmlns:tx="http://www.springframework.org/schema/tx"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.3.xsd
     	http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-4.3.xsd 
        http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.3.xsd
        http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-4.3.xsd
        http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.3.xsd ">                    
 
 	<!-- 初始化属性文件,通过ApplicationContextAware加载Spring上下文环境-->
 	<!-- Spring源码中ApplicationContextAwareProcessor.postProcessBeforeInitialization()，对继承自ApplicationContextAware的bean进行处理，
 	调用其setApplicationContext。而ApplicationContextAwareProcessor是在spring容器start的时候生成的。 -->
    <bean id="appContextUtil" class="com.dongshuo.cn.syncjob.utils.AppContextUtil"></bean>
    
     <!-- scan the package and the sub package -->
 	<context:component-scan base-package="com.dongshuo.cn.syncjob" />
 	
 	<!-- 配置说明：https://www.cnblogs.com/redcool/p/6426173.html -->
 	<!-- spring线程池ThreadPoolTaskExecutor与阻塞队列BlockingQueue,ThreadPoolTaskExecutor是一个spring的线程池技术-->
 	 <!-- 异步线程池 -->
    <bean id="threadPool"
        class="org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor">
        <!-- 核心线程数 -->
        <property name="corePoolSize" value="3" />
        <!-- 最大线程数 -->
        <property name="maxPoolSize" value="10" />
        <!-- 队列最大长度 >=mainExecutor.maxSize -->
        <property name="queueCapacity" value="25" />
        <!-- 线程池维护线程所允许的空闲时间 -->
        <property name="keepAliveSeconds" value="300" />
        <!-- 线程池对拒绝任务(无线程可用)的处理策略 ThreadPoolExecutor.CallerRunsPolicy策略 ,调用者的线程会执行该任务,如果执行器已关闭,则丢弃.  -->
        <property name="rejectedExecutionHandler">
            <bean class="java.util.concurrent.ThreadPoolExecutor$CallerRunsPolicy" />
        </property>
    </bean>
    
    <!-- AbortPolicy:直接抛出java.util.concurrent.RejectedExecutionException异常 -->
    <!-- CallerRunsPolicy:主线程直接执行该任务，执行完之后尝试添加下一个任务到线程池中，-->
    <!-- DiscardOldestPolicy:抛弃旧的任务、暂不支持；会导致被丢弃的任务无法再次被执行-->
    <!-- DiscardPolicy:抛弃当前任务、暂不支持；会导致被丢弃的任务无法再次被执行 -->
    
     <bean id="jobUtil" class="com.dongshuo.cn.syncjob.utils.JobUtil" >
         <property name="systemPool" ref="threadPool"></property>
          <property name="busiPool" ref="threadPool"></property>
     </bean>
</beans>
```



### 2.1 JobUtil类的加载

```java
package com.dongshuo.cn.syncjob.utils;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


/**
 * @author: empathy
 * @date: 2018年10月25日 下午11:30:36
 * @description: Spring的线程池ThreadPoolTaskExecutor使用案例
 * @version: v1.0
 */
public class JobUtil {
	
	/**
	 * 系统内部处理任务时使用此线程
	 */
	private static ThreadPoolTaskExecutor systemPool;
	
	/**
	 * 业务处理任务时使用此线程
	 */
	private static ThreadPoolTaskExecutor busiPool;
	
	private JobUtil(){}
	
	/**
	 * 添加到任务队列:系统内部任务
	 * 
	 * @param job
	 */
	public static void addSystemWorkJob(Runnable job) {
		systemPool.execute(job);
	}

	/**
	 * 添加到任务队列:业务类任务
	 * 
	 * @param job
	 */
	public static void addBusiWorkJob(Runnable job) {
		busiPool.execute(job);
	}
	
	public void setSystemPool(ThreadPoolTaskExecutor systemPool) {
		JobUtil.systemPool = systemPool;
	}
	public void setBusiPool(ThreadPoolTaskExecutor busiPool) {
		JobUtil.busiPool = busiPool;
	}

}

```



### 2.2 自定义一个任务实现Runnable 接口

```java
package com.dongshuo.cn.syncjob.jobs;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.dongshuo.cn.syncjob.utils.EmailUtil;

/**
 * 
 * @author: empathy
 * @date: 2018年10月27日 上午11:20:28
 * @description: 异步邮件发送任务-多线程并发处理
 * @version: v1.0
 */
public class SendEmailJob implements Runnable {
	
	private static Logger logger = Logger.getLogger(SendEmailJob.class);
	
	private String toPeople;
	
	private String ccPeople;
	
	private String subject;
	
	private String content;
	
	private ArrayList<String> fileList;
	
	/**
	 * 构造方法1
	 */
	public SendEmailJob(String toPeople, String ccPeople, String subject,String content, ArrayList<String> fileList) {
		this.toPeople = toPeople;
		this.ccPeople = ccPeople;
		this.subject = subject;
		this.content = content;
		this.fileList = fileList;
	}

	/**
	 * 构造方法2
	 */
	public SendEmailJob(String emailTos, String emailCcs, String subject,String content) {
		this.toPeople = emailTos;
		this.ccPeople = emailCcs;
		this.subject = subject;
		this.content = content;
		this.fileList=null;
	}

	/**
	 * 需要注意的是，执行线程的时候可以使用start()方法或者run()方法，虽然使用run会达到同样的效果，
	 * 但是run是在主线程中使用的，也就是使用你当前的方法内线程，而不是另起一个线程，这样就达不到异步的效果，所以务必使用start()
	 */
	@Override
	public void run() {
		try {
			EmailUtil.sendHtmlMail(toPeople,ccPeople, subject, content, fileList);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			e.printStackTrace();
		}
	}
}

```





###  2.3 执行异步任务

```java
package com.dongshuo.cn.syncjob.main;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dongshuo.cn.syncjob.jobs.SendEmailJob;
import com.dongshuo.cn.syncjob.utils.JobUtil;
import com.dongshuo.cn.syncjob.utils.Util;

/**
 * @author: empathy
 * @date: 2018年10月26日 上午12:18:02
 * @description: spring 异步任务实现
 * @version: v1.0
 */
public class SyncJobTest {
	
	private static final Logger logger = Logger.getLogger(SyncJobTest.class);

	/**
	 * @param args
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		//加载spring-mvc配置文件,异步任务配置文件(配置地址：https://www.cnblogs.com/lic309/p/4186880.html)
		new ClassPathXmlApplicationContext( "file:"+"D:\\workspace\\JIEBA\\syncjob\\config\\"+"spring-mvc.xml");
		
		String  emailTos="xxx@qq.com";
		
		String  emailCcs="xxx@qq.com";
		
		String  subject="异步任务实现";
		
		String  content="定时发送异步邮件任务";
		
		try {
			System.out.println("==============strat===============");
			if(Util.notEmpty(emailTos) || Util.notEmpty(emailCcs)){
				// TODO: 邮件异步通知类
				JobUtil.addBusiWorkJob(new SendEmailJob(emailTos,emailCcs, subject, content));
				logger.info("执行成功");
				// 暂停5s
				Thread.sleep(2000);
				System.out.println("==============end===============");
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

}

```





## 3. spring 通过ApplicationContextAware加载上下文环境

```JAVA
package com.dongshuo.cn.syncjob.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 * @author: empathy
 * @date: 2018年10月26日 下午11:09:29
 * @description: 通过ApplicationContextAware加载Spring上下文环境
 * @version: v1.0
 */
public class AppContextUtil implements ApplicationContextAware {

	private static ApplicationContext appCtx; 
	
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		appCtx = appContext;
	}

	/**
	 * 获取ApplicationContext
	 * @return
	 */
    public static ApplicationContext getApplicationContext(){  
        return appCtx;
    }  
      
    /**  
     * 这是一个便利的方法，帮助我们快速得到一个BEAN  
     * @param beanName bean的名字  
     * @return 返回一个bean对象  
     */    
    public static Object getBean( String beanName ) {    
        return appCtx.getBean( beanName );    
    }    
    
    /**
     * 通过类型获得BEAN
     * @param cls
     * @return
     */
    public static <T> T getBean(Class<T> cls){
        return appCtx.getBean(cls); 
    }
}

```





## 4. 参考信息

```bash
# ThreadPoolTaskExecutor配置问题
1. https://blog.csdn.net/u013887008/article/details/80980052
# Spring教程____Spring线程池_ThreadPoolTaskExecutor的配置和使用
2. https://blog.csdn.net/qq827245563/article/details/78194438
# Spring线程池ThreadPoolTaskExecutor配置及详情
3. https://www.cnblogs.com/redcool/p/6426173.html

```

