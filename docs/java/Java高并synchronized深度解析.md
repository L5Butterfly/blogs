---
title: "Java高并synchronized深度解析"
date: 2019-04-19 09:25:00
author: empathy
img: /resources/images/20190419.jpg
top: true
cover: true
coverImg: /resources/images/20190419.jpg
password: 
toc: true
mathjax: false
summary: "Java高并synchronized深度解析"
categories: Java
tags: 
  - Java
  - 多线程
---





# Java高并synchronized深度解析



## 简介

​	高并发问题向来是Java程序员进阶的重点，也是面试的难点。想要打通高并发的奇经八脉，synchronized是你不得不趟过的坑，本课程从synchronized，从使用方法到底层原理源码，娓娓道来。还对常见面试题和更深层扩展方面的思考，由浅入深做出了讲解。

**Synchronized的作用**：能够保证在**同一时刻**最多只有**一个**线程执行该段代码，以保证并发安全的效果。

防治线程干扰和内存一致性错误，同一时刻只有一个线程执行该代码。



```bash
# Synchronized的两个用法:
    1、对象锁：
        a、方法锁（默认锁对象为this当前实例对象）
        b、同步代码块锁（自己指定锁对象）
    2、类锁：
        a、使用关键字synchronized修饰静态的方法
        
# 并行执行和串行执行效果演示代码
# synchronized代码块的作用：
synchronized(this){
     #需要保护的代码块
}   
```





## 对象锁

### 代码块形式

```java

package demo;

/**
 * 对象锁使用;对象锁;同步锁
 * 对象锁实例：代码块形式
 */
public class SynchronizedObjectCodeBlock implements Runnable {

    //本类的实例
    static SynchronizedObjectCodeBlock instance=new SynchronizedObjectCodeBlock();

    @Override
    public void run() {

		//1.使用synchronized关键字保护的代码块，保证代码并行执行，有先后顺序的执行代码
       synchronized (this){
            System.out.println("我是对象锁的代码块形式，我叫"+Thread.currentThread().getName());
            //休眠3秒钟，执行效果更佳明显
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"执行完毕。");
        }

        //2.无synchronized关键字修改的代码块，多线程代码是串行执行代码的，基本上是同时执行代码
        System.out.println("我是对象锁的代码块形式，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }


    public static void main(String[] args) {
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);
        /*
        try {
            th1.start();
            th2.start();
            th1.join();
            th2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }
}    
```





**执行效果对比：**

```bash
# 使用synchronized (this){}修改的效果
我是对象锁的代码块形式，我叫Thread-1
Thread-1执行完毕。
我是对象锁的代码块形式，我叫Thread-0
Thread-0执行完毕。
finished

# 无使用synchronized (this){}修改的效果
我是对象锁的代码块形式，我叫Thread-0
我是对象锁的代码块形式，我叫Thread-1
Thread-0执行完毕。
Thread-1执行完毕。
finished
```



### 对象锁区别

`lock`锁和`this`锁，不同的代码块使用两把不同的锁，代码块是并行执行的，同时执行的。使用同一把锁，代码是等待按照顺序执行的。

```java
package demo;

/**
 * 对象锁使用;对象锁;同步锁
 * 对象锁实例：代码块形式
 */
public class SynchronizedObjectCodeBlock2 implements Runnable {

    //本类的实例
    static SynchronizedObjectCodeBlock2 instance=new SynchronizedObjectCodeBlock2();

    //自定义锁对象,代替this对象;创建2把不同的锁;
    Object lock1=new Object();
    Object lock2=new Object();

    @Override
    public void run() {

       synchronized (lock1){
            //保护的代码块，保证代码并行执行，有先后顺序的执行代码
            System.out.println("我是对象锁lock1，我叫"+Thread.currentThread().getName());
            //休眠3秒钟，执行效果更佳明显
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"lock1执行完毕。");
        }


        synchronized (lock2){
            //保护的代码块，保证代码并行执行，有先后顺序的执行代码
            System.out.println("我是对象锁lock2，我叫"+Thread.currentThread().getName());
            //休眠3秒钟，执行效果更佳明显
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"lock2执行完毕。");
        }
        
        /*
        synchronized (lock1){
            //保护的代码块，保证代码并行执行，有先后顺序的执行代码
            System.out.println("我是对象锁lock2，我叫"+Thread.currentThread().getName());
            //休眠3秒钟，执行效果更佳明显
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"lock2执行完毕。");
        }
        */
    }


    public static void main(String[] args) {

        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);

        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }
}

```



### 方法锁形式

**对象锁分类**

**代码块形式：**手动指定锁对象,可以自己指定lock对象。

**方法锁形式：**synchronized修饰普通方法，锁对象默认为this。

```java
package demo;

/**
 * 对象锁使用;对象锁;同步锁
 * 对象锁实例：方法锁形式
 */
public class SynchronizedObjectMethod implements Runnable {

    //本类的实例
    static SynchronizedObjectMethod instance=new SynchronizedObjectMethod();

    @Override
    public void run() {
        method();
    }


    //synchronized修饰普通方法，默认是对象锁为this;方法上加上同步修饰符synchronized
    public synchronized void method(){
        //保护的代码块，保证代码并行执行，有先后顺序的执行代码
        System.out.println("我是对象锁的代码块形式，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }


    public static void main(String[] args) {

        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);

        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }
}
```





## 类锁

​    	**概念：**Java类可能有很多个对象，但只有一个Class对象，锁类时Class对象的锁（锁类只能在同一时刻被一个对象拥有）

​	**本质：**所谓类锁，只不过是Class对象的锁而已。

​        **形式：**

​            1、synchronize关键字加在static方法上。

​            2、synchronized（*.class）代码块。

​	**用法和效果：**类锁只有在同一时刻被一个对象拥有。



### static形式

```java

/**代码演练**/
package demo;


/**
 * 类锁的第一种形式：static形式
 * 静态形式是体现在方法上的
 */
public class SynchronizedClassStatic implements Runnable {

    //本类的实例
    static SynchronizedClassStatic instance1=new SynchronizedClassStatic();
    static SynchronizedClassStatic instance2=new SynchronizedClassStatic();


    @Override
    public void run() {
        method();
    }


    //加static和不加static的执行效果是不一样。
    public static synchronized void method(){

        //保护的代码块，保证代码并行执行，有先后顺序的执行代码
        System.out.println("我是类锁的第一种形式，加static修饰，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");

    }


    //加static和不加static的执行效果是不一样。
    public synchronized void method2(){

        //保护的代码块，保证代码并行执行，有先后顺序的执行代码
        System.out.println("我是类锁的第一种形式,不加static修饰，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");

    }


    public static void main(String[] args) {

        //实例2个线程对象
        Thread th1=new Thread(instance1);
        Thread th2=new Thread(instance2);

        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }

}

```



**类锁用法总结**

```bash
# 用法总结
1. synchronized关键字加在static方法上。
2. synchronized修饰的方法如果加上static关键字，类锁只能被一个对象访问。
3. synchronized修饰的方法如果加上不static关键字，类锁可以被不同的对象访问，代码是并行执行的，不是按照顺序执行的。
```







### *class形式

```java
package demo;


/**
 * 类锁的第二种形式：*class形式
 * 静态形式是体现在方法上的
 */
public class SynchronizedClassClass  implements Runnable{

    //本类的实例
    static SynchronizedClassClass instance1=new SynchronizedClassClass();
    static SynchronizedClassClass instance2=new SynchronizedClassClass();

    @Override
    public void run() {

        /*//普通的写法，使用this;举反例
        synchronized(this){
            //synchronized类锁的代码块，保证代码并行执行，有先后顺序的执行代码
            System.out.println("我是类锁的第二种形式,synchronized（*.class），我叫"+Thread.currentThread().getName());
            //休眠3秒钟，执行效果更佳明显
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"执行完毕。");

        }*/

        try {
            method(); //代码并行执行
            method2(); //代码等待按照顺序执行
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    public void method() throws InterruptedException{
        //普通的写法，使用this;举反例
        synchronized(this){
            //synchronized类锁的代码块，保证代码并行执行，有先后顺序的执行代码
            System.out.println("我是类锁的第二种形式,synchronized（*.class），我叫"+Thread.currentThread().getName());
            //休眠3秒钟，执行效果更佳明显
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"执行完毕。");

        }
    }



    public void method2() throws InterruptedException{
        //正常的写法，用*.class
        synchronized(SynchronizedClassClass.class){
            //synchronized类锁的代码块，保证代码并行执行，有先后顺序的执行代码
            System.out.println("我是类锁的第二种形式,synchronized（*.class），我叫"+Thread.currentThread().getName());
            //休眠3秒钟，执行效果更佳明显
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"执行完毕。");

        }
    }


    public static void main(String[] args) {

        //实例2个线程对象
        Thread th1=new Thread(instance1);
        Thread th2=new Thread(instance2);

        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }
}

```



***class形式使用总结**

```bash
# *class形式使用总结
1. synchronized(this){};修饰,代码会并行执行，多线程下不能保证数据的一致性。
2. synchronized(*.class){};修饰，代码会串行执行，按照顺序执行，多线程只能有一个对象获取到锁，其他锁只能等待锁释放才能获取。

```







## 实战应用

**消失的请求数解决方案**

​	使用多线程执行一个方法，方法的逻辑执行累加操作，最后程序执行完毕，得到的结果不预期数小，这个是为什么呢，接下来看下面的代码解释，你就一目了然了。



```bash
# 不是有并发手段会有什么后果？如何解决？
# 问题逻辑：
两个线程同时执行a++操作，最后结果会比预计的少。

# 原因分析：
count++,他看上去只是一个操作，其实他包含了3个动作。
1. 读取count
2. 将count加1
3. 将count值写入到内存中
```





**原始的代码**

```java
/** 存在问题的原始代码**/
package demo;


/**
 * 两个线程同时执行a++操作，最后结果会比预计的少。
 */
public class DisappearRequest implements  Runnable{

    static DisappearRequest instance=new DisappearRequest();

    static int i=0;

    public static void main(String[] args) throws InterruptedException {

        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);
        System.out.println("执行......");
        th1.start();
        th2.start();
        th1.join();
        th2.join();
        System.out.println("执行完毕，执行结果：");
        System.out.println(i);

    }

    @Override
    public void run() {

        for (int j=0;j<100000;j++){
            i++;
        }

    }
}

```





**优化的代码**

```java
/** 使用锁优化后的代码**/
/** 1.对象锁方式优化**/
/**########################################################**/
package demo;

/**
 * 两个线程同时执行a++操作，最后结果会比预计的少。
 */
public class DisappearRequest implements  Runnable{

    static DisappearRequest instance=new DisappearRequest();
    static int i=0;
    
    public static void main(String[] args) throws InterruptedException {
        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);
        System.out.println("执行......");
        th1.start();
        th2.start();
        th1.join();
        th2.join();
        System.out.println("执行完毕，执行结果：");
        System.out.println(i);
    }

    @Override
    public void run() {
        synchronized (this){
            for (int j=0;j<100000;j++){
                i++;
            }
        }
    }
}


/** 2.类锁方式优化**/
/**########################################################**/
package demo;

/**
 * 两个线程同时执行a++操作，最后结果会比预计的少。
 */
public class DisappearRequest implements  Runnable{

    static DisappearRequest instance=new DisappearRequest();
    static int i=0;
    
    public static void main(String[] args) throws InterruptedException {
        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);
        System.out.println("执行......");
        th1.start();
        th2.start();
        th1.join();
        th2.join();
        System.out.println("执行完毕，执行结果：");
        System.out.println(i);
    }

    @Override
    public void run() {
        synchronized (DisappearRequest.class){
            for (int j=0;j<100000;j++){
                i++;
            }
        }
    }
}


/** 3.方法锁方式优化**/
//static不能加在run方法上，所以一般都另外写一个方法来调用，达到类锁的作用
/**########################################################**/
package demo;

/**
 * 两个线程同时执行a++操作，最后结果会比预计的少。
 */
public class DisappearRequest implements  Runnable{
    static DisappearRequest instance=new DisappearRequest();
    static int i=0;

    public static void main(String[] args) throws InterruptedException {

        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);
        System.out.println("执行......");
        th1.start();
        th2.start();
        th1.join();
        th2.join();
        System.out.println("执行完毕，执行结果：");
        System.out.println(i);

    }

    @Override
    public void run() {
        method();
    }

    public static synchronized void method(){
        for (int j=0;j<100000;j++){
            i++;
        }
    }
}


/** 4. synchronized修饰优化**/
/**########################################################**/
package demo;
/**
 * 两个线程同时执行a++操作，最后结果会比预计的少。
 */
public class DisappearRequest implements  Runnable{

    static DisappearRequest instance=new DisappearRequest();
    static int i=0;
    public static void main(String[] args) throws InterruptedException {

        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);
        System.out.println("执行......");
        th1.start();
        th2.start();
        th1.join();
        th2.join();
        System.out.println("执行完毕，执行结果：");
        System.out.println(i);

    }
    
    @Override
    public synchronized void run() {
        for (int j=0;j<100000;j++){
            i++;
        }
    }
}

```





## 面试分析

**多线程访问同步方法的7种情况（面试常考）**



```bash
# 多线程访问同步方法的7种情况
1. 两个线程同时访问一个对象的同步方法。
2. 两个线程访问的是两个对象的同步方法。
3. 两个线程访问的是synchronized的静态方法。
4. 同时访问同步方法和非同步方法。
5. 访问同一个对象的不同的普通同步方法。
6. 同时访问静态synchronized的和非静态synchronized的方法。
7. synchronized修饰方法抛异常后，会自动释放锁。
```



**两个线程同时访问一个对象的同步方法**

```java
package demo;


/**
 * 对象锁使用
 * 对象锁实例：方法锁形式
 */
public class SynchronizedObjectMethod implements Runnable {

    //本类的实例
    static SynchronizedObjectMethod instance=new SynchronizedObjectMethod();

    @Override
    public void run() {
        method();
    }

    //synchronized修饰普通方法，默认是对象锁为this;方法上加上同步修饰符synchronized
    public synchronized void method(){
        //保护的代码块，保证代码并行执行，有先后顺序的执行代码
        System.out.println("我是对象锁的代码块形式，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }


    public static void main(String[] args) {

        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);

        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }
}
```



**两个线程访问的是两个对象的同步方法**

```java
package demo;


/**
 * 对象锁使用
 * 对象锁实例：代码块形式
 */
public class SynchronizedObjectCodeBlock1 implements Runnable {

    //本类的实例
    static SynchronizedObjectCodeBlock1 instance=new SynchronizedObjectCodeBlock1();
    static SynchronizedObjectCodeBlock1 instance2=new SynchronizedObjectCodeBlock1();

    @Override
    public void run() {
		
       //虽然这这里使用了this关键字，但是这个this不是同一个锁对象
       synchronized (this){
            //保护的代码块，保证代码并行执行，有先后顺序的执行代码
            System.out.println("我是对象锁的代码块形式，我叫"+Thread.currentThread().getName());
            //休眠3秒钟，执行效果更佳明显
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"执行完毕。");
        }
    }

    public static void main(String[] args) {
        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance2);

        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }
}

```



**两个线程访问的是synchronized的静态方法**

```java
/**只要是静态的，不同实例使用的锁就是同一把对象锁 **/
package demo;

/**
 * 类锁的第一种形式：static形式
 * 静态形式是体现在方法上的
 */
public class SynchronizedClassStatic implements Runnable {
    //本类的实例
    static SynchronizedClassStatic instance1=new SynchronizedClassStatic();
    static SynchronizedClassStatic instance2=new SynchronizedClassStatic();

    @Override
    public void run() {
        method();
    }

    //加static和不加static的执行效果是不一样。
    public static synchronized void method(){
        //保护的代码块，保证代码并行执行，有先后顺序的执行代码
        System.out.println("我是类锁的第一种形式，加static修饰，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }

    //加static和不加static的执行效果是不一样。
    public synchronized void method2(){
        //保护的代码块，保证代码并行执行，有先后顺序的执行代码
        System.out.println("我是类锁的第一种形式,不加static修饰，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }

    public static void main(String[] args) {
        //实例2个线程对象
        Thread th1=new Thread(instance1);
        Thread th2=new Thread(instance2);
        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }
}
```



**同时访问同步方法和非同步方法**

```java
package demo;


/**
 * 同时访问同步方法和非同步方法执行结果分析
 * 2个线程分别执行不同的方法代码是并行执行的，对非同步方法，锁是没有作用的
 * 总结：synchronized只对修饰的方法起作用，其他方法不受影响。
 */
public class SynchronizedYesOrNo implements Runnable{

    static SynchronizedYesOrNo instance=new SynchronizedYesOrNo();

    @Override
    public void run() {
        if(Thread.currentThread().getName().equals("Thread-0")){
            method1();
        }else{
            method2();
             //method3();
        }
    }

    public synchronized void method1(){
        //保护的代码块，保证代码并行执行，有先后顺序的执行代码
        System.out.println("我是加锁的形式，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }

    public void method2(){
        //保护的代码块，保证代码并行执行，有先后顺序的执行代码
        System.out.println("我是没有加锁的形式，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }
    
    
    public synchronized void method3(){
        //保护的代码块，保证代码并行执行，有先后顺序的执行代码
        System.out.println("我是加锁的形式，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }

    public static void main(String[] args) {
        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);
        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }

}
```



**访问同一个对象的不同的普通同步方法**

```java
package demo;

/**
 * 访问同一个对象的不同的普通同步方法。
 * synchronized的底层原来默认使用this锁，多线程使用同一个实例就是同步执行。
 */
public class SynchronizedDifferentMethod  implements Runnable{

    static SynchronizedDifferentMethod instance=new SynchronizedDifferentMethod();
    static SynchronizedDifferentMethod instance2=new SynchronizedDifferentMethod();

    @Override
    public void run() {
        if(Thread.currentThread().getName().equals("Thread-0")){
            method1();
        }else{
            method3();
        }
    }

    //synchronized的底层原来默认使用this锁，这个this是同一个实例对象，可以保证串行执行，如果是不同的实例就会并行执行了。
    public synchronized void method1(){
        //保护的代码块，保证代码串行执行，有先后顺序的执行代码
        System.out.println("我是加锁的形式，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }

    public synchronized void method3(){
        //保护的代码块，保证代码串行执行，有先后顺序的执行代码
        System.out.println("我是加锁的形式，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }

    public static void main(String[] args) {
        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);
        //Thread th2=new Thread(instance2);

        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }
}

```



**同时访问静态synchronized的和非静态synchronized的方法**

```java
package demo;


/**
 * 同时访问静态synchronized的和非静态synchronized的方法
 */
public class SynchronizedStaticAndNormal implements Runnable {

    static SynchronizedStaticAndNormal instance=new SynchronizedStaticAndNormal();

    @Override
    public void run() {

        if(Thread.currentThread().getName().equals("Thread-0")){
            method1();
        }else{
            method3();
        }
    }

    //synchronized+static=类锁；使用*.class锁，和this是同一个锁对象，不能保证串行执行。
    public synchronized static void method1(){
        //保护的代码块，保证代码串行执行，有先后顺序的执行代码
        System.out.println("我是静态加锁的方法，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }

    //synchronized的底层原来默认使用this锁，这个this是同一个实例对象，可以保证串行执行。
    public synchronized void method3(){
        //保护的代码块，保证代码串行执行，有先后顺序的执行代码
        System.out.println("我是非静态加锁的方法，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }


    public static void main(String[] args) {
        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);

        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }
}

```



**synchronized修饰方法抛异常后，会释放锁**

`synchronized`修饰方法抛异常后，会自动释放锁，而自定义的`lock`对象锁在方法抛异常后，不会自动释放锁，需要手动捕捉异常，在`finally`里面处理异常，手动释放`lock`锁对象。



```java
package demo;


/**
 * synchronized修饰的同步方法抛异常后，会释放锁。
 * 展示不抛异常前和抛出异常后的对比，一旦抛出异常，第二个线程立刻进入同步方法，说明第一个线程的锁已经释放。
 */
public class SynchronizedException implements Runnable {

    static SynchronizedException instance=new SynchronizedException();

    @Override
    public void run() {

        if(Thread.currentThread().getName().equals("Thread-0")){
            method1();
        }else{
            method3();
        }
    }

    public synchronized void method1(){
        //保护的代码块，保证代码串行执行，有先后顺序的执行代码
        System.out.println("我是加锁的方法，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
            //手动抛出一个异常
            throw new Exception();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            //捕捉异常
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }

    public synchronized void method3(){
        //保护的代码块，保证代码串行执行，有先后顺序的执行代码
        System.out.println("我是加锁的方法，我叫"+Thread.currentThread().getName());
        //休眠3秒钟，执行效果更佳明显
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"执行完毕。");
    }

    public static void main(String[] args) {
        //实例2个线程对象
        Thread th1=new Thread(instance);
        Thread th2=new Thread(instance);

        th1.start();
        th2.start();
        while (th1.isAlive() || th2.isAlive()){
        }
        System.out.println("finished");
    }
}

```



## 总结

```bash
# 关于7种synchronized相关面试问题学习总结；
# 3点核心思想和对应的实例：
1. 一把锁只能同时被一个线程获取，没有拿到锁的线程必须等待，（对应第1、5种情况）
2. 每个实例都对应自己的一把锁，不同实例之间互相不影响；例外：锁对象是*。class以及synchronized修饰的是static方法的时候，所有的对象都共用一把锁，（对应第2、3、4、6种情况）
3. 无论方法正常执行完毕或者方法执行抛出异常，都会释放锁，（对应第7种情况）

# 其他慕课学友总结：
1. 一把锁只能同时被一个线程获取，没有拿到锁的线程必须等待；每个实例都对应有自己的一把锁，不同实例之间互不影响。
2. 在一个被 synchronized 修饰的方法中去调用另外一个没有被 synchronized 修饰的方法，这不是线程安全的，另外一个方法可能被另外的线程访问。
3. 在被synchronized修饰的方法中调用普通方法，普通方法是可以被多个线程同时调用的，是线程不安全的。
4. 对象锁：每个对象都有一把锁；类锁：该类的所有对象公用一把锁；synchronized关键字是否正常关闭都会释放锁；synchronized只能保证被该关键字修饰的方法。
5. 一把锁只能同时被一个线程获取，没有拿到锁的线程必须等待；每个实例都对应有自己的一把锁，不同实例之间互不影响。
```







## synchronized性质

### 可重入性质

1、同一个方法是可以重复调用的，递归证实了，可重入。

2、锁方法1调用锁方法2，证明不是同一个方法也可重入。

3、不在同一个类中，也可能可重入，子类调用父类。



```java
package synchronize;

/*
* 使用递归方式调用：
* 可重入粒度测试：递归调用本方法
* 证明：被synchronized关键字修饰的同一个方法是可以被重入的
* */
public class SynchronizedRecursion {

    int a=0;

    public static void main(String[] args) {
        SynchronizedRecursion synchronizedRecursion=new SynchronizedRecursion();
        synchronizedRecursion.method();
    }
    
    private synchronized void method() {
        System.out.println("这个是method方法"+a);
        if(a==0){
            a++;
            method();
        }
    }
}

/**#############################################################**/
package synchronize;

/*
 * 可重入粒度测试：调用类的另外一个方法
 * 证明：可重入不要求是被synchronized关键字修饰的同一个方法
 * */
public class SynchronizedOtherMethod {

    public synchronized void method1(){
        System.out.println("我是方法1");
        method2();
    }

    public synchronized void method2(){
        System.out.println("我是方法2");

    }

    public static void main(String[] args) {
        SynchronizedOtherMethod s=new SynchronizedOtherMethod();
        s.method1();
    }
}

/**#############################################################**/
package synchronize;

/*
 * 可重入粒度测试：调用父类的方法
 * 证明：情况3：证明可重入不要求是同一个类中的
 * 总结：同步锁调用粒度关系范围：在同一个线程中可以被重入。
 * */
public class SynchronizedSupperClass {

    //被synchronized关键字修饰的方法
    public synchronized  void doSomething(){
        System.out.println("我是父类的方法");
    }
}

class TestClass extends  SynchronizedSupperClass{

    //重写父类的方法
    public synchronized void doSomething(){
        System.out.println("我是父类的方法");
    }
    public static void main(String[] args) {
        TestClass t=new TestClass();
        t.doSomething();
    }
}
/**#############################################################**/
/** 总结：**/
1. 情况1：证明同一个方法是可重入的。
2. 情况2：证明可重入不要求是同一个方。
3. 情况3：证明可重入不要求是同一个类中的。
4. 在子类中调用父类被子类重写的方法。
5. 可重入锁，粒度是线程范围的，只要在一个线程中，如果需要的是同一把锁，那么就不需要重新释放锁，再获取锁。
```





### 不可中断性质

```bash
# synchronized的性质：不可中断性质
1、线程A拿到锁，不释放的话，别人永远拿不到锁，永远等待；
2、Lock锁会有一些比较灵活的功能，按时间等

# 性质：不可中断
一旦这个锁已经被別人获得了，如果我还想获得，我只能选择等待或者阻塞，直到别的线程释放这个锁。如果别人永远不释放锁,那么我只能永远地等下去。相比之下，未来会介绍的Lock类，可以拥有中断的能力，第一点，如果我觉得我等的时间太长了，有权中断现在已经获取到锁的线程的执行；第二点，如果我觉得我等待的时间太长了不想再等了，也可以退出。

# java内存模型(JMM)
http://www.cnblogs.com/dolphin0520/p/3920373.html 
例如:i=i+1;单线程情况:执行这个语句的时候,先从主内存中读取i的值,复制一份到高速缓存当中。
```





## 原理分析

**加锁和释放锁的原理：**现象、时机、深入JVM看字节码。

**可重入原理：**加锁次数计数器。

**可见性原理： ** 内存模型。



```java
/**ReentrantLocak特性（对比synchronized）---ReentrantLock(重入锁)**/
效果和synchronized一样，都可以同步执行，lock方法获得锁，unlock方法释放锁

package synchronize;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 加锁和释放锁原理: 等价代码的分析
 * ReentrantLock： 内置锁
 */
public class SynchronizedToLock {

        Lock lock=new ReentrantLock();

        public synchronized void method1(){
            System.out.println("我是synchronized形式的锁");
        }

        public void method2(){
            lock.lock();
            try {
                System.out.println("我是lock形式的锁");
            }finally {
                //不管是否抛出异常，都会执行此代码
                lock.unlock();
            }
        }

    public static void main(String[] args) {
        SynchronizedToLock instance=new SynchronizedToLock();
        instance.method1();
        instance.method2();
        //方法1和方法2是等价的写法，相当于方法2是synchronized的内部实现原理
    }
}
```







**原理总结：**

```bash
# 可重入原理
1、JVM负责跟踪对象被加锁的次数；
2、有个monitor计数器，如果当前线程再次获取该锁，计数递增
3、任务结束离开，则会执行monitorexit，计数递减，直到完全释放


# JAVA内存模型 JMM
1、synchronized保证可见性，当一个线程获取到对象锁，会把需要的变量从主内存读取到 当前线程内存即本地内存中来，保证了数据就是最新的；
2、当synchronized结束后，会把修改后的变量立即刷新写到主内存中；
3、但是不能防止  某个变量，被别的不加锁的方法修改，有个关键字，volitile
```





## synchronized缺陷

```bash
# synchronized缺陷
1、效率低，锁的释放情况少，一种是正常执行任务完释放，一种是异常JVM释放，
不能设置超时时间；
2、不够灵活，读的话可能不需要加锁，例如读写锁就比较灵活；
3、无法判断状态，是否获取到锁
```





## 常见面试问题

```bash
# 常见synchronized相关的面试问题
1. 使用注意点：锁对象不能为空、作用域不易过大、避免死锁。
（多线程编程主要是提高效率-并行执行代码，作用域过大，导致串行执行，降低代码执行效率）
2. 如何选择Lock和synchronized关键字？
建议：有现成的工具类最好使用现成的工具类。
3. 多线程访问同步方法的各种情况。（参考上面的总结）
```



## 参考资料

```bash
# Java高并发之魂：synchronized深度解析
https://www.imooc.com/learn/1086
# 学习源码地址
https://gitee.com/empathyess/JavaDemo.git
```

