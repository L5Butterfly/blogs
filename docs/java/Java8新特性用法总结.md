---
title: "Java8新特性用法总结"
date: 2019-04-20 09:25:00
author: empathy
img: /resources/images/20190420.jpg
top: true
cover: true
coverImg: /resources/images/20190420.jpg
password: 
toc: true
mathjax: false
summary: "Java8新特性用法总结"
categories: Java
tags: Java
---



# Java8新特性用法总结



## 前言

```bash
# Stream API
Stream 是 Java8 中处理集合的关键抽象概念，它可以指定你希望对集合进行的操作，可以执行非常复杂的查找、过滤和映射数据等操作。使用Stream API 对集合数据进行操作，就类似于使用 SQL 执行的数据库查询。也可以使用 Stream API 来并行执行操作。简而言之，Stream API 提供了一种高效且易于使用的处理数据的方式

# Stream
是数据渠道，用于操作数据源（集合、数组等）所生成的元素序列。 
“集合讲的是数据，流讲的是计算！” 
1. Stream 自己不会存储元素。 
2. Stream 不会改变源对象。相反，他们会返回一个持有结果的新Stream。 
3. Stream 操作是延迟执行的。这意味着他们会等到需要结果的时候才执行。

# Stream 的操作三个步骤
1.创建 Stream :一个数据源（如：集合、数组），获取一个流
2.中间操作:一个中间操作链，对数据源的数据进行处理
3.终止操作(终端操作) :一个终止操作，执行中间操作链，并产生结果

# Lambda 表达式
Lambda 是一个匿名函数，我们可以把 Lambda 表达式理解为是一段可以传递的代码（将代码像数据一样进行传递）。可以写出更简洁、更灵活的代码。作为一种更紧凑的代码风格，使Java的语言表达能力得到了提升。 
Lambda 表达式在Java 语言中引入了一个新的语法元素和操作符。这个操作符为 “->” ， 该操作符被称为 Lambda 操作符或箭头操作符。它将 Lambda 分为两个部分： 
- 左侧：指定了 Lambda 表达式需要的所有参数 
- 右侧：指定了 Lambda 体，即 Lambda 表达式要执行的功能。
```





## Java8新特征性总结

### filter用法

```java
/**/
package java8com;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Java8Demo {
	
	/**
	 * jdk8,java8的新特性总结;stream;removeIf
	 */
	public static void main(String args[]) {
		
		//原始列表数据
	    List<String> lines = Arrays.asList("spring", "node", "python","java");
	    
	    //过滤数据
	    List<String> result0 = getFilterOutput(lines, "java");
	    System.out.println(result0);
	    // 循环输出
	    for (String temp : result0) {
	      System.out.println(temp);
	    }
	 
	    //使用lambda表达式，使用lambda来进一步精简代码
	    List<String> result1 = lines.stream().filter(line -> !"java".equals(line)).collect(Collectors.toList());
	    System.out.println(result1);
	    // 循环输出
	    result1.forEach(System.out::println); 
	    
	    
	    //获取返回的列表大小
	    int result2 = lines.stream().filter(line -> !"java".equals(line)).collect(Collectors.toList()).size();
	    System.out.println(result2);
	  }
	 
	
	  /**
	   * 过滤列表数据
	   * @param lines
	   * @param filter
	   * @return
	   */
	  private static List<String> getFilterOutput(List<String> lines, String filter) {
	    List<String> result = new ArrayList<>();
	    for (String line : lines) {
	      if (!filter.equals(line)) {
	        result.add(line);
	      }
	    }
	    return result;
	  }
}

```



### removeIf用法

```java
/**/
package java8com;

import java.util.Iterator;
import java.util.List;

import com.google.common.collect.Lists;

public class Java8RemoveIfDemo {
	
	//作用：删除集合中符合条件的成员，empty集合也可以，但是null就抛异常了。
	public static void main(String args[]) { 
		//removeIfTest();
		beforeRemove();
	}
	
	
    /**
     * jdk8用法：删除集合中符合条件的成员，empty集合也可以，但是null就炸啦。
     */
    @SuppressWarnings("unused")
	private static void removeIfTest() {
        List<String> list = Lists.newArrayList("1","12","13","14","15","0");
        System.out.println("初始时："+ list.toString());
        list.removeIf(s -> s.contains("1"));
        System.out.println("过滤完：" + list.toString());
    }
    
    
    
    /**
     * jdk8之前版本用法：咱之前是怎么从集合中过滤掉不要的元素的
     */
    private static void beforeRemove() {
        List<String> list = Lists.newArrayList("1", "12", "13", "14", "15", "0");
        System.out.println("初始时：" + list.toString());
 
        //1.使用for循环删除元素
        System.out.println("for i 循环删除元素");
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).contains("1")) {
                list.remove(i);
                //一般之前这么操作集合的时候，估计都是会忘记 i-- 的
                i--;
            }
        }
        System.out.println("过滤完：" + list.toString());
 
        
        //2.Iterator 迭代器 循环删除元素
        System.out.println("Iterator 迭代器 循环删除元素");
        list = Lists.newArrayList("1", "12", "13", "14", "15", "0");
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().contains("1")) {
                iterator.remove();
            }
        }
        System.out.println("过滤完：" + list.toString());
        
        
        //3.加强 for 循环  循环删除元素，直接异常。
        System.out.println("加强 for 循环  循环删除元素，直接异常。");
        list = Lists.newArrayList("1", "12", "13", "14", "15", "0");
        //list.remove(0);  
        for (String s : list) {
            if (s.contains("1")) {
                //list.remove(0);  
                System.out.println(s);
            }
        }
        System.out.println("过滤完：" + list.toString());
    }
}
```





### 实例应用

```java
/*融合计费产品实现轮循计费*/
@Override
	public void getProductByChargeCode(FirstReqVo reqVo) throws BaseAppException {
		//1、手机号码校验
		checkMobile(reqVo.getMobile());
		
		//2、计费点校验
		DisProductPriceVo disProductPriceVo = disPriceDao.selectInfoByCode(reqVo.getChargeCode());
		ExceptionHandler.publishMsg(Util.isEmpty(disProductPriceVo), "渠道计费点不存在或者已经下线，请联系平台商务人员！");
		ExceptionHandler.publishMsg(Util.isEmpty(disProductPriceVo.getProductPriceCode()), "产品计费点不存在或者已经下线，请联系平台商务人员！");
		
		//3、获取请求用户的省份信息
		RegionCacheVo regionInfo =getRegionId(reqVo.getMobile(), reqVo.getImsi()) ;
		Long regionId = Util.isEmpty(regionInfo) ? null : regionInfo.getRegionId();
		reqVo.setRegionId(regionId);
		reqVo.setProductId(disProductPriceVo.getProductId());
		reqVo.setProductPriceVo(disProductPriceVo);
		
		ProductCacheVo product = ProductCache.INSTANCE.getItem(disProductPriceVo.getProductId());
		
		//若果是融合产品，先去选择使用的子产品
		if(BusiConstants.PRODUCT_TYPE_FUSE.equals(product.getProductType())){
			reqVo.setFuseProductId(disProductPriceVo.getProductId());
			//根据融合产品ID，资费，省份，查询可以使用的子产品，按优先级从高到低排序
			disProductPriceVo.setFuseProductId(disProductPriceVo.getProductId());
			disProductPriceVo.setRegionId(regionId);
			
			ArrayList<DisProductPriceVo> fusePriceList = disPriceDao.selectFuseChildProduct(disProductPriceVo);
			//筛选符合要求的子产品，暂时只有超量判断
			doFilterFuseProduct(fusePriceList,regionInfo);
			ExceptionHandler.publishMsg(Util.isEmpty(fusePriceList), "无可用通道" );
			//融合产品优化
			DisProductPriceVo fusePriceVo =null;
			//其他正常走优先级搞的产品
			fusePriceVo = fusePriceList.get(0);
			//筛选出2个以上的信息;判断优先级是否一样
			if(fusePriceList.size() > 1 && fusePriceList.get(0).getPriority()==fusePriceList.get(1).getPriority()){
				//当优先级和省份和资费一样时，随机跑各个子产品
				Long priLevel = fusePriceVo.getPriority();
				int i = fusePriceList.stream().filter(price -> price.getPriority() == priLevel).collect(Collectors.toList()).size();
				//随机跑一个子产品,轮循规则还没有想到
				Random random = new Random();
				int priIndex=random.nextInt(i);
				fusePriceVo = fusePriceList.get(priIndex);
			}
			disProductPriceVo.setProductId(fusePriceVo.getProductId());
			disProductPriceVo.setProductPriceCode(fusePriceVo.getProductPriceCode());
			disProductPriceVo.setProductPriceName(fusePriceVo.getProductPriceName());
			
			product = ProductCache.INSTANCE.getItem(fusePriceVo.getProductId());
			
			reqVo.setProductId(disProductPriceVo.getProductId());
			
			reqVo.setProductPriceVo(disProductPriceVo);
		}
		
		//4、产品省份是否配置了限额，暂时只针对非组合计费点
		ProductProvinceInfoVo productProvinceVo = provinceDao.selectProductProvince(disProductPriceVo.getProductId(), regionId);
		ExceptionHandler.publishMsg(Util.notEmpty(regionId) && Util.isEmpty(productProvinceVo.getRegionId()), "省份已关停" );
		
		//5.校验产品的开通时间
		checkTimeLimit(product,productProvinceVo);
		
		checkPhoneLimit(product, productProvinceVo, reqVo.getMobile());
		
		//6、点播和包月校验
		String chargeType = product.getChargeType();
		if(BusiConstants.CHARGE_TYPE_MONTH.equals(chargeType)){
			doMonPay(product, reqVo, productProvinceVo, regionInfo, disProductPriceVo);
		}else{
			doRdoPay(product, reqVo, productProvinceVo, regionInfo, disProductPriceVo);
		}
	}
```





```java
/*融合产品集合中筛选出符合计费产品;或者移除不符合计费的产品*/
/**
	 * 产品省份到量，移除道量的产品，然后进行轮循计费
	 * @param fusePriceList
	 * @param regionInfo
	 * @return
	 * @throws BaseAppException
	 */
	private void doFilterFuseProduct(ArrayList<DisProductPriceVo> fusePriceList, RegionCacheVo regionInfo)throws BaseAppException {
		Date now = DateUtil.getNowDate();
		for (int i = 0;i<fusePriceList.size();i++) {
			DisProductPriceVo fusePrice = fusePriceList.get(i);
			//1、产品（省份）超量检测
			boolean useProductPlan = Util.isEmpty(fusePrice.getLimitAmount()) || fusePrice.getLimitAmount() <=0l;
			Long planAmount = useProductPlan ? fusePrice.getProductDayLimitAmount() : fusePrice.getLimitAmount();
			Long dayUsedAmount = BusiConstants.CHARGE_TYPE_MONTH.equals(fusePrice.getChargeType())?
					(monthLogDao.selectUsedAmount(DateUtil.getDayStartTime(now), DateUtil.getDayEndTime(now), fusePrice.getProductId(), fusePrice.getDistributorId(), useProductPlan ? null : regionInfo.getRegionId())):
					(orderLogDao.selectUsedAmount(DateUtil.getDayStartTime(now), DateUtil.getDayEndTime(now), fusePrice.getProductId(), fusePrice.getDistributorId(), useProductPlan ? null : regionInfo.getRegionId()));
			planAmount = (null == planAmount  || planAmount<=0) ? Long.MAX_VALUE : planAmount;
			//2、如果超量则移除
			if(dayUsedAmount > planAmount) {
				fusePriceList.remove(fusePrice);
				i--;
			}
		}
	}
```





```java
/*使用removeIf()方法实现，removeIf(new Predicate<T>)方法*/
/**
	 * 产品省份到量，移除道量的产品，然后进行轮循计费
	 * @param fusePriceList
	 * @param regionInfo
	 * @return
	 * @throws BaseAppException
	 */
	private void doFilterFuseProduct(ArrayList<DisProductPriceVo> fusePriceList, RegionCacheVo regionInfo)throws BaseAppException {
        //1、产品（省份）超量检测
		Date now = DateUtil.getNowDate();
		Predicate<DisProductPriceVo> fusePriceList3 = (fusePrice3) -> {
			boolean useProductPlan3 = Util.isEmpty(fusePrice3.getLimitAmount()) || fusePrice3.getLimitAmount() <=0l;
			Long planAmount3 = useProductPlan3 ? fusePrice3.getProductDayLimitAmount() : fusePrice3.getLimitAmount();	
			Long dayUsedAmount3=0L;
			try {
				dayUsedAmount3 = BusiConstants.CHARGE_TYPE_MONTH.equals(fusePrice3.getChargeType())?
						(monthLogDao.selectUsedAmount(DateUtil.getDayStartTime(now), DateUtil.getDayEndTime(now), fusePrice3.getProductId(), fusePrice3.getDistributorId(), useProductPlan3 ? null : regionInfo.getRegionId())):
						(orderLogDao.selectUsedAmount(DateUtil.getDayStartTime(now), DateUtil.getDayEndTime(now), fusePrice3.getProductId(), fusePrice3.getDistributorId(), useProductPlan3 ? null : regionInfo.getRegionId()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			planAmount3 = (null == planAmount3  || planAmount3<=0) ? Long.MAX_VALUE : planAmount3;
			return dayUsedAmount3 > planAmount3;
        };
		fusePriceList.removeIf(fusePriceList3);
	}
```





## 参考资料

```bash
# Java8流（Stream）操作实例-筛选、映射、查找匹配
https://blog.csdn.net/zyhlwzy/article/details/78625136
# Java集合Stream类filter的使用
https://blog.csdn.net/qq_33829547/article/details/80279488
# java8集合方法 removeIf()的示例
https://blog.csdn.net/qq_27093465/article/details/79154566
# Java8中使用filter()过滤列表，使用collect将stream转化为list
https://blog.csdn.net/huludan/article/details/54316387
# java8 stream ,filter 等功能代替for循环
https://www.cnblogs.com/yimiyan/p/5992440.html
# JAVA8 Stream接口，map操作，filter操作，flatMap操作
https://blog.csdn.net/qq_28410283/article/details/80642786
```

