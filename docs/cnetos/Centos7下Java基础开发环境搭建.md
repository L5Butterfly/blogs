---
title: "centos7下Java基础开发环境搭建"
date: 2019-03-01 09:25:00
author: empathy
img: /resources/images/20190301.jpg
top: true
cover: true
coverImg: /resources/images/20190301.jpg
password: 
toc: true
mathjax: false
summary: "centos7下Java基础开发环境搭建"
categories: Linux
tags: 
  - Linux
  - Centos
---



# Centos7下Java基础开发环境搭建



## 前言

需要安装基础环境信息：

1. `jdk`
2. `meavn`
3. `nginx`
4. `svn`
5. `postgresql`
6. `mysql`



## JDK8安装

```sh
# 参考资料：
# Centos7安装JDK8以及环境配置
https://blog.csdn.net/pang_ping/article/details/80570011

# 官网下载地址
https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

# wget下载,官网不支持wget下载
wget https://download.oracle.com/otn-pub/java/jdk/jdk-8u191-linux-x64.tar.gz

# 配置步骤
1、下载jdk
2、解压：tar -zxvf jdk-8u191-linux-x64.tar.gz
3、移动到自动地址：
mkdir /usr/local/jdk  
mv jdk1.8.0_191 /usr/local/jdk/
4、编辑配置文件：
vi /etc/profile
结尾添加：
#JDK环境配置
JAVA_HOME=/usr/local/jdk/jdk1.8.0_191
JRE_HOME=/usr/local/jdk/jdk1.8.0_191/jre
CLASS_PATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib
PATH=$PATH:$JAVA_HOME/bin:$JRE_HOME/bin
export JAVA_HOME JRE_HOME CLASS_PATH PATH

5、重新加载配置文件：source /etc/profile
6、检查是否安装成功：java -version

# 安装成功显示
[root@iZ2ze5yghwjbz157011nhzZ software]# java -version
java version "1.8.0_191"
Java(TM) SE Runtime Environment (build 1.8.0_191-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.191-b12, mixed mode)
```





##  Maven安装

```sh
# 配置参考地址：
# Linux 环境下安装Maven
https://www.cnblogs.com/jimmy-muyuan/p/7895933.html
# CentOS Maven配置
https://blog.csdn.net/lj402159806/article/details/78595570

# maven官网地址
http://maven.apache.org/download.cgi

# 下载3.5.2的maven版本
wget https://archive.apache.org/dist/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.tar.gz

# wget下载重命名
wget -c https://archive.apache.org/dist/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.tar.gz -O maven-3.5.2.tar.gz

# 1、创建安装路径
mkdir -p /usr/local/maven

# 2、移动maven到安装目录
mv apache-maven-3.5.2 /usr/local/maven/

# 4、配置maven环境变量
vi /etc/profile
#添加环境变量
export MAVEN_HOME=/usr/local/maven/apache-maven-3.5.2
export MAVEN_HOME
export PATH=$PATH:$MAVEN_HOME/bin

# 编辑之后记得使用source /etc/profile命令是改动生效。
source /etc/profile

# 5、验证结果
在任意路径下执行mvn -version验证命令是否有效。
mvn -v 或者 mvn -version

# 正常结果如下，能够看到当前maven及jdk版本。
Apache Maven 3.5.2 (138edd61fd100ec658bfa2d307c43b76940a5d7d; 2017-10-18T15:58:13+08:00)
Maven home: /usr/local/maven/apache-maven-3.5.2
Java version: 1.8.0_191, vendor: Oracle Corporation
Java home: /usr/local/jdk/jdk1.8.0_191/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "3.10.0-693.2.2.el7.x86_64", arch: "amd64", family: "unix"
```





## SVN安装

```sh
# 配置参考资料：
# CentOS SVN服务器安装配置小记
https://www.cnblogs.com/wangyan-tutu/p/3358297.html
# Centos7下SVN服务器如何搭建
https://www.cnblogs.com/fuyuanming/p/6123395.html
# CentOS下搭建SVN服务器
https://www.cnblogs.com/weifeng1463/p/7593729.html

# svn安装
yum install subversion

# 创建svn版本库目录
mkdir -p /usr/local/svn/svnrepos

# 创建版本库;初始化版本库，生成配置文件信息
svnadmin create /usr/local/svn/svnrepos

# 进入conf目录（该svn版本库配置文件）
authz：是权限控制文件
passwd：帐号密码文件
svnserve.conf：SVN服务配置文件

# 设置帐号密码
vi passwd
# 在[users]块中添加用户和密码，格式：帐号=密码
test=test
chao=chao

# 设置权限
vi authz
# 在末尾添加如下代码：配置限制版本库的根目录dan对其有读写权限，w只有读权限
[/]
chao=rw
test=r

# 修改svnserve.conf文件
###打开下面的几个注释：功能说明###
anon-access = read #匿名用户可读
auth-access = write #授权用户可写
password-db = passwd #使用哪个文件作为账号文件
authz-db = authz #使用哪个文件作为权限文件
realm = /usr/local/svn/svnrepos # 认证空间名，版本库所在目录

# 启动svn版本库　
svnserve -d -r /usr/local/svn/svnrepos

# 修改svn默认启动端口;--listen-port 8690 指定端口启动
svnserve -d -r /usr/local/svn/svnrepos --listen-port 8690

# SVN默认的打开端口是3690；最好修改svn端口。
netstat -antp | grep svn

# centos7 打开防火墙端口；firewall配置开放
$ sudo firewall-cmd --permanent --add-port=8690/tcp
$ sudo firewall-cmd --reload

# 防火墙配置；SVN的默认端口是3609，要在防火墙上开放这个端口才行；iptables配置开放
$ iptables -A INPUT -i eth0 -p tcp --dport 8690 -j ACCEPT
$ service iptables save

# 在windows上测试
在elipse中导入SVN，输入SVN://ip  

# svn访问地址
svn://39.96.68.224:8690(test:test)

# windows平台安装svn客户端即可。
```





## Nginx安装

```sh
# 参考配置方案：
# Centos下 Nginx安装与配置
# https://www.cnblogs.com/zhanghaoyong/p/7737536.html
# CentOS7.0安装Nginx 1.7.4
# https://www.cnblogs.com/jerrypro/p/7062101.html

# 1、下载
$ wget http://nginx.org/download/nginx-1.10.2.tar.gz

# 2、解压
$ tar -zxvf nginx-1.10.2.tar.gz
$ cd nginx-1.10.2 

# 3、指定下载路径
$ ./configure --prefix=/usr/local/nginx

# 4、编译安装：
make && make install

# 5、启动
$ /usr/local/nginx/sbin/nginx

# 检查是否启动成功：
$ ps -ef | grep nginx

打开浏览器访问此机器的 IP，如果浏览器出现 Welcome to nginx! 则表示 Nginx 已经安装并运行成功。

# 6、重启：
$ /usr/local/nginx/sbin/nginx –s reload

# 7、停止：
$ /usr/local/nginx/sbin/nginx –s stop

# 8、测试配置文件是否正常
$ /usr/local/nginx/sbin/nginx –t

# 9、强制关闭：
$ pkill nginx

# 10、创建配置文件存放路径
$ mkdir -p /usr/local/nginx/conf/vhost

# 11、备份初始配置文件
$ cp nginx.conf nginx_bak.conf

# 12、在nginx.conf配置末尾追加内容，使其vhost下的配置文件生效。
include vhost/*.conf;
```





## MySQL安装

```sh
# MySQL安装参考信息
https://blog.csdn.net/qq_41809943/article/details/82177550
这里选择安装MySQL5.7，也可以到官网下载最新或者选择合适的版本。

# 1.下载MySQL源rpm安装包
wget http://dev.mysql.com/get/mysql57-community-release-el7-8.noarch.rpm

# 2.安装MySQL源
rpm -ivh mysql57-community-release-el7-8.noarch.rpm
#或者执行
yum localinstall mysql57-community-release-el7-8.noarch.rpm

# 3. 检查MySQL源是否安装成功
yum repolist enabled |grep mysql
# 显示结果
mysql-connectors-community/x86_64 MySQL Connectors Community                  74
mysql-tools-community/x86_64      MySQL Tools Community                       74
mysql57-community/x86_64          MySQL 5.7 Community Server                 307

# 4.安装mysql-community-server
yum install mysql-community-server

# 5. 启动mysqld服务
systemctl start mysqld

# 6.查看服务器是否运行
ps -ef | grep mysqld

# 7.查看服务监听端口
netstat -antp | grep mysqld

##################################################
# mysql安装完成的初始密码：
##################################################
grep 'temporary password' /var/log/mysqld.log

# mysql初始密码
uY!sWT!s!1os

# mysql修改密码失败，提示密码不符合规范
ALTER USER root@localhost IDENTIFIED BY 'test654321';
set password for 'root'@'localhost'=password('test654321'); 

# 修好密码失败解决方案：
set global validate_password_policy=0;
set global validate_password_length=10;

# 注意: mysql特性。
mysql5.7默认安装了密码安全检查插件（validate_password），默认密码检查策略要求密码必须包含：
大小写字母、数字和特殊符号，并且长度不能少于8位。否则会提示ERROR 1819 (HY000): 
Your password does not satisfy the current policy requirements错误。

# 为typecho创建一个数据库，这里数据库名为typecho。
create database typecho;
grant all privileges on typecho.* to typecho@localhost identified by 'test654321';
flush privileges;

# mysql允许远程连接配置
# 支持root用户允许远程连接mysql数据库
grant all privileges on *.* to 'root'@'%' identified by 'test654321' with grant option;

# 刷新权限 
flush privileges;

################################
### 参考配置方案
################################
# 配置mysql允许远程链接
https://blog.csdn.net/z858466/article/details/79946141

#配置mysql允许远程连接的方法
https://www.cnblogs.com/linjiqin/p/5270938.html

```



## MySQL备份

```sh
# 数据库备份;输入密码
/usr/bin/mysqldump -u root -p typecho > /home/typecho.sql

# 数据库还原;输入密码
mysql -u root -p typecho < /home/typecho.sql
```



## PostgreSQL安装

```sh
# PostgreSQL安装
# Linux下 PostgreSQL 编译安装
# 参考地址：https://blog.csdn.net/jerry_sc/article/details/76408116
# 可能需要显安装依赖包（yum -y install gcc gcc-c++ readline-devel zlib-devel）

# 使用wget工具拉取软件源码包
wget http://ftp.postgresql.org/pub/source/v9.6.6/postgresql-9.6.6.tar.gz

# tar zxf 包名称 进行解压
tar zxf postgresql-9.6.6.tar.gz

# 创建用户postgres、设置密码 postgres；根据提示输入用户密码
# useradd -m -d /var/local/postgres postgres
adduser postgres
passwd postgres

# cd 到解压的安装包目录进行编译安装包
cd postgresql-9.6.6

# 指定包的安装路径
./configure --prefix=/var/local/postgres

# 编译安装包即可
gmake && gmake install
make && make install

# 设置系统环境变量
vi /etc/profile

# 添加一行软件的安装路径信息，信息如下
PATH=$PATH:$HOME/bin:/var/local/postgres/bin
export PATH

# 保存退出，使用source命令是文件立即生效
source /etc/profile

# 切换到postgre用户，初始化数据库
su - postgres

# 指定数据库数据文件目录进行初始化设置
#创建数据库存放目录
mkdir -p /var/local/postgres/data

# 赋予postgres用户文件权限
chown postgres:postgres /var/local/postgres -R

# 初始化数据库，指定数据库目录
/var/local/postgres/bin/initdb -D /var/local/postgres/data
initdb -D /var/local/postgres/data

# 退出postgres用户，切换到root用户
exit

# 编译启动命令
# 从postgres解压后的文件夹里拷贝linux到/etc/init.d/下
# 注意：/etc/init.d/目录是相关服务的启动目录存放文件信息，启动配置文件
# 拷贝linux到/etc/init.d/目录下并重命名postgresql
cp /root/software/postgresql-9.6.6/contrib/start-scripts/linux /etc/init.d/postgresql

# 编辑脚本postgresql信息
vi /etc/init.d/postgresql

# 编辑内容主要是修改下面两行即可。
# pg的安装目录
prefix=/var/local/postgres

# pg的数据存放目录
PGDATA="/var/local/postgres/data"

# 保存退出,为postgresql添加可执行权限
chmod +x /etc/init.d/postgresql

# 启动数据库
# 使用postgresql脚本呢启动postgres数据库
/etc/init.d/postgresql start
# 重启服务
/etc/init.d/postgresql restart

# 查看服务是否启动成功
ps -ef | grep postgres

# 查看端口是否监听状态
netstat -antp | grep 5432

# 把postgresql加入自启动列表
cd /etc/init.d
chkconfig --add postgresql

# 查看一下自启动列表：
chkconfig --list

# PostgreSQL 允许远程访问设置方法；配置如下2点即可。
1. pg_hba.conf：配置对数据库的访问权限，
2. postgresql.conf：配置PostgreSQL数据库服务器的相应的参数。

# 配置的步骤：
# 1.找到配置文件所在路径
find / -name  pg_hba.conf
# 2.编辑pg_hba.conf文件
vi /var/local/postgres/data/pg_hba.conf
# 3.修改pg_hba.conf文件，配置用户的访问权限，在末尾追加如下内容即可，然后保存退出。
host    all             all             0.0.0.0/0               md5 

# 配置PostgreSQL数据库服务器的相应的参数
# 修改postgresql.conf文件，将数据库服务器的监听模式修改为监听所有主机发出的连接请求。
# 操作步骤：
# 1.找到配置文件所在路径
find / -name  postgresql.conf
# 2.编辑postgresql.conf文件
vi /var/local/postgres/data/postgresql.conf
# 3. 修改内容如下：
将listen_addresses前的"#"去掉，并将listen_addresses ='localhost'
改成listen_addresses = '*'。
将listen_port前的"#"去掉,可以修改数据库的端口。

# 查看post相关进程信息
ps -ef| grep postgres

# 重启服务以使设置生效
/etc/init.d/postgresql restart

# 修改数据库登录密码，
su - postgres
psql -p 8432
ALTER USER postgres WITH PASSWORD 'postgres';


########### 扩展知识###########
# 注意：postgresql启动后就可以利用servicepostgresql start、restart或者stop来控制它了。
# 初始化数据库：
/etc/init.d/postgresql initdb
# 启动数据库：
/etc/init.d/postgresql start
# 停止数据库：
/etc/init.d/postgresql stop

# 参考配置：http://39.96.68.224:81/index.php/archives/19/
```







