---
title: "git日常开发使用命令"
date: 2019-06-16 09:25:00
author: empathy
img: /resources/images/20190616.jpg
top: true
cover: true
coverImg: /resources/images/20190616.jpg
password: 
toc: true
mathjax: true
summary: "git日常开发使用命令"
categories: 版本控制
tags:
  - Git
---



# git日常开发使用命令



### 项目初始化

```bash
1. 项目初始化
$ git init

2. 远程仓库克隆项目到本地
$ git clone git@address

3. 查看和修改远端地址
# git remote -v 命令来查看当前仓库的远端地址，从下方看到是 https格式的
$ git remote -v

# git remote add 远端仓库地址别名 <git@url> 来添加一个新的仓库地址，下方添加的是一个ssh的仓库链接。
$ git remote add
$ git remote add origin_alias https://gitee.com/empathyess/tester.git

4.远端仓库地址删除和重命名
# 查看目前现有的远端仓库
$ git remote -v

# 删除别名为origin_alias的远端地址，删除后，再次查看时，origin的仓库地址就没有了。
$ git remote rm origin_alias

# 将origin_ssh重命名为origin即可
$ git remote rename origin_ssh origin

# 查看push到远端的那个分支上
$ git remote show origin
```



### 项目版本控制

```bash
# 日常代码修改提交使用命令
1. 查看项目文件状态
$ git status 

2. 提交修改文件到暂存区（3种命令）
# 提交所有变化
$ git add <文件名>
$ git add -A (git add --all的缩写)

# 提交被修改(modified)和被删除(deleted)文件，不包括新文件(new)
$ git add -u  (git add --update的缩写)

# 提交新文件(new)和被修改(modified)文件，不包括被删除(deleted)文件
$ git add .  (提交非删除文件)

3. 将提交到暂存区的文件恢复到之前暂存的状态
$ git reset HEAD <文件名>

4. 对本地未提交暂存区的文件进行回滚到修改前的状态，此操作不可逆（文件未提交暂存区），操作慎用。
$ git checkout -- <文件名>

5. 根据上面git status命令的提示内容，3种命令的使用：
# 暂存文件的命令：
$ git add <文件名>
# 放弃未暂存文件的修改命令：
$ git checkout -- <文件名>
# 将被修改的文件（暂存并提交）的命令：
$ git commit -a 
# 只对状态为M的文件有用，而对新增而为添加的问题是不起作用的，新添加的文件仍然处于Untracked 状态中。
$ git commit -a -m "提交说明" 

# 使用 git add 命令对新增文件进行暂存，使用 git commit 命令对暂存的内容进行提交。
$ git add <文件名>
$ git commit -m "提交说明"

6. 将本地提交的代码推送到远程分支地址
# 配置了github的ssh-key，第1次push需要输入用户名和密码,后续就不需要了。
git push

```



### 分支管理

```bash
# git 分支切换和管理
1. 查看项目当前分支
# 查看本地仓库分支信息
$ git branch

# 查看远程仓库分支信息
$ git branch -r

# 初始化默认创建
$ git init
##############################
2. 切换分支
# 查看当前项目有哪些分支，项目处于哪个分支下
$ git branch

# 切换到开发分支
$ git checkout develop


# 查看分支的提交历史记录日志
$ git log
$ git log --pretty=oneline

# 只显示提交版本+提交的注释
$ git log --oneline   


# 提交代码到新的分支
$ git push
$ git push --set-upstream origin develop

#############################
3. 日常开发分支的创建
# git branch新建分支
$ git branch newBranch
$ git branch test
$ git branch develop

# 强制新建分支
$ git branch -f newBranch


# 新建分支的同时切换到新的分支
$ git checkout -b newBr

# 强制切换分支并覆盖已有分支并切换
$ git checkout -B newBr

# 分支重新命名
$ git branch -m newBr newBr3

# 删除本地其他无效的分支(-D:强制删除)
$ git branch -d test
$ git branch -d oneBr twoBr
$ git branch -D newBr

# 恢复删除的分支
$ git branch newBr a12b74f
```





### 日志记录

```bash
# 查看git提交的日志记录
$ git reflog
$ git log
$ git log -r
$ git log --help

```





### 版本的回滚

```bash
# git恢复之前版本的两种方法reset、revert
1. git reset命令
# 查看本地历史提交版本
$ git log
# 使用“git reset --hard 目标版本号”命令将版本回退：
$ git reset --hard 39d3ce44d30f2986fcd7329258bdf25314fb3637
# 使用“git push -f”提交更改，此时如果用“git push”会报错，因为我们本地库HEAD指向的版本比远程库的要旧
$ git push -f
# 所以我们要用“git push -f”强制推上去，就可以了

2. git revert命令
# 查看线上日志和版本信息
$ git log
$ git log -r

# 使用（git revert -n 版本号）做反做，并使用“git commit -m 版本名”提交：
$ git revert -n 39d3ce44d30f2986fcd7329258bdf25314fb3637

# git commit -m 备注提交内容
# 注意： 这里可能会出现冲突，那么需要手动修改冲突的文件。而且要git add 文件名。
$ git commit -m 回滚线上某个版本

# git revert所有版本内容都有记录，回滚会生成新的版本信息和目标版本一致。
# 此时可以用“git log”查看本地的版本信息，可见多生成了一个新的版本：
$ git log

# 使用“git push”推上远程库：
$ git push

# 查看github和gitee上显示的远程库版本信息
```





### 从远程仓克隆到本地新分支

```bash
# 本地仓库和远程仓库的使用场景：
# 1. 克隆远程仓代码到本地
$ git clone  http://xxx.git

# 2.当前分支是master 下，创建test分支
$ git checkout –b test

# 3.当前分支为test,把当前test分支代码还原到 指定节点
$ git reset --hard e6058a0d07fb5a5b8b71230315120460b111311e

# git log 查看提交的版本,可以回滚之前的版本
$ git log

# 提交代码到本地仓
$ git commit -m  备注内容

# 推送本地到远程test是本地分支名
$ git push origin test

# 所以我们要用“git push -f”强制推上去，就可以了
$ git push # 推送被拒绝了，可以采用强制推送方式到分支
$ git push -f

# 合并本地仓库test分支到主要开发分支master分支;合并前要切换到master分支
$ git merge test

```



### 忽略提交配置

```bash
# 配置文件和开发软件的配置无关信息不需要提交
git忽略的原理:
新建一个.gitignore文件里面加入;.idea/workspace.xml 即可，但是如果你在这样做之前已经将此文件提交到了git远程仓库，那就需要执行以下命令了清除远程文件，提交后即可:

# 添加内容
.idea/*
.mavn*

# 删除远程分支的忽略文件 
git  rm -r --cached .idea
git  rm -r --cached <filename>

# 推送配置修改
git push
git push --set-upstream origin test
```





### 分支删除

```bash
# 删除远程分支
git push origin --delete

# 删除本地分支
git branch -d develop

# 同时删除2个本地分支
git branch -d demo fix

# 查看本地分支
git branch

# 查看远程分支
git branch -r
```





### 总结

```bash
# 根据git status命令的相关提示，引出了其他相关命,下方是对其相关命令的总结：
$ git status : 查看当前仓库中文件的状态。
$ git status -s : 文件状态的简写（M - 修改， A - 添加， D - 删除， R - 重命名，?? - 未追踪）。
$ git add <文件名> ：将后方紧跟的文件进行暂存，以便commit使用。
$ git reset HEAD <文件名> : 将已经暂存的文件进行撤销，回到未暂存的状态。
$ git checkout -- <文件名> ：撤销对尚未暂存文件的修改，该操作不可逆，慎用。
$ git commit -a : 对那些被修改的文件单尚未暂存和提交的文件进行暂存和提交。注意：对未暂存的新增文件无效。
$ git commit : 对暂存区的文件进行提交到本地仓库。
$ git push : 将本地仓库已经提交的内容发布到远端。

```





### 扩展

```bash
# 删除远程分支文件
$ git rm -r --cached .idea         # 删除.idea文件夹
$ git commit -m '删除.idea'        # 提交,添加操作说明
$ git pull
# * [new branch]      feature/tester -> origin/feature/tester
# 创建本地分支和远程分支关联起来
$ git checkout -b feature/tester origin/feature/tester

# 删除远程分支文件
$ git rm -r --cached .settings
$ git rm -r --cached .classpath
$ git rm -r --cached .project
```





### 参考资料

```bash
# 蚂蚁部落
http://www.softwhy.com/article-8569-1.html
# git操作笔记（git常用命令行工具）
https://blog.csdn.net/changjiufu/article/details/82315963
# 前端日常常用git命令
https://www.cnblogs.com/web1/p/6972567.html
# git reset命令的使用
https://www.jianshu.com/p/cbd5cd504f14
# git恢复之前版本的两种方法reset、revert（图文详解）
https://blog.csdn.net/yxlshk/article/details/79944535
```

