---
title: "SpringBoot框架下整合Redis应用"
date: 2019-06-04 09:25:00
author: empathy
img: /resources/images/20190604.jpg
top: true
cover: true
coverImg: /resources/images/20190604.jpg
password: 
toc: true
mathjax: false
layout: post
summary: "SpringBoot框架下整合Redis应用"
categories: Redis
tags: [SpringBoot,Redis]
keywords: SpringBoot,Redis
comments: true
description: SpringBoot框架下整合Redis应用
---



# SpringBoot框架下整合Redis应用



## 前言

```bash
# springboot整合redis——redisTemplate的使用
# 　StringRedisTemplate作为RedisTemplate的子类，只支持KV为String的操作
StringRedisTemplate与RedisTemplate
两者的关系是StringRedisTemplate继承RedisTemplate。
两者的数据是不共通的；也就是说StringRedisTemplate只能管理StringRedisTemplate里面的数据，
RedisTemplate只能管理RedisTemplate中的数据。
SDR默认采用的序列化策略有两种，一种是String的序列化策略，一种是JDK的序列化策略。
StringRedisTemplate默认采用的是String的序列化策略，保存的key和value都是采用此策略序列化保存的。
RedisTemplate默认采用的是JDK的序列化策略，保存的key和value都是采用此策略序列化保存的。
```






## 环境说明
1. `eclipse开发功能`
2. `meavn项目`
3. `spring boot整合`



## 项目初始化
### 项目结构

```
# 查看项目目录结构：tree -L 7
.
├── docs
│   ├── application.properties
│   ├── application.yml
│   └── redis.md
├── pom.xml
├── src
│   ├── main
│   │   ├── java
│   │   │   └── com
│   │   │       └── empathy
│   │   │           └── redisdemo
│   │   │               ├── config
│   │   │               ├── controller
│   │   │               ├── dao
│   │   │               ├── mapper
│   │   │               ├── model
│   │   │               ├── redis
│   │   │               ├── RedisDemoApplication.java
│   │   │               └── service
│   │   └── resources
│   │       ├── application.properties
│   │       ├── config
│   │       │   └── random.properties
│   │       ├── log4j.properties
│   │       ├── logback.xml
│   │       └── mybatis
│   │           ├── mapper
│   │           │   └── UserMapper.xml
│   │           └── mybatis-config.xml
│   └── test
│       └── java
│           └── com
│               └── empathy
│                   └── redisdemo
│                       ├── dao
│                       └── redis
└── target
    ├── classes
    │   ├── application.properties
    │   ├── com
    │   │   └── empathy
    │   │       └── redisdemo
    │   │           ├── config
    │   │           │   ├── RandomConfig.class
    │   │           │   └── RedisConfig.class
    │   │           ├── controller
    │   │           │   ├── ControllerDemo.class
    │   │           │   └── RedisController.class
    │   │           ├── dao
    │   │           │   ├── impl
    │   │           │   └── UserDao.class
    │   │           ├── mapper
    │   │           │   └── UserMapper.class
    │   │           ├── model
    │   │           │   ├── Demo.class
    │   │           │   └── User.class
    │   │           ├── redis
    │   │           │   ├── JsonUtil.class
    │   │           │   ├── ObjectRedisSerializer.class
    │   │           │   ├── RedisCacheConfig$1.class
    │   │           │   ├── RedisCacheConfig.class
    │   │           │   ├── RedisService.class
    │   │           │   ├── RedisUtil.class
    │   │           │   └── RedisUtils.class
    │   │           ├── RedisDemoApplication.class
    │   │           └── service
    │   │               ├── DemoService.class
    │   │               └── impl
    │   ├── config
    │   │   └── random.properties
    │   ├── log4j.properties
    │   ├── logback.xml
    │   └── mybatis
    │       ├── mapper
    │       │   └── UserMapper.xml
    │       └── mybatis-config.xml
    └── test-classes
        └── com
            └── empathy
                └── redisdemo
                    ├── dao
                    │   └── UserDaoJdbcTemplateImplTest.class
                    └── redis
                        └── RedisTest.class
```



### POM文件配置

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.empathy</groupId>
	<artifactId>springboot-redis-demo</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>springboot-redis-demo</name>
	<description>Demo project for Spring Boot</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.0.0.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<spring-cloud.version>Finchley.M9</spring-cloud.version>
	</properties>

	<dependencies>
		 <!-- spring-web -->
		<dependency>
	      <groupId>org.springframework.boot</groupId>
	      <artifactId>spring-boot-starter-web</artifactId>
	      <exclusions>
	        	<exclusion> 
		            <groupId>org.springframework.boot</groupId>
		            <artifactId>spring-boot-starter-logging</artifactId>
	        	</exclusion>
	       </exclusions>
	    </dependency>
	    <!-- Log4j -->
	    <dependency>
		    <groupId>org.springframework.boot</groupId>
		    <artifactId>spring-boot-starter-log4j</artifactId>
		    <version>1.3.8.RELEASE</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>
		
		<!-- JdbcTemplate依赖包 -->
		<dependency>
      	 	<groupId>org.springframework.boot</groupId>
       		<artifactId>spring-boot-starter-jdbc</artifactId>
		</dependency>
		<!-- MyBatis依赖包 -->
		<dependency>
	    	<groupId>org.mybatis.spring.boot</groupId>
	    	<artifactId>mybatis-spring-boot-starter</artifactId>
	    	<version>1.1.1</version>
		</dependency>
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<scope>runtime</scope>
		</dependency>
		
		<!-- redis依赖包 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-redis</artifactId>
		</dependency>
		
		<dependency>
       		<groupId>org.springframework.boot</groupId>
      		<artifactId>spring-boot-starter-test</artifactId>
       		<scope>test</scope>
		</dependency>
	</dependencies>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

	<repositories>
		<repository>
			<id>spring-milestones</id>
			<name>Spring Milestones</name>
			<url>https://repo.spring.io/milestone</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>
</project>

```



### 配置文件

**`1.application.properties`**

```properties
#当前服务端口
server.port=8888

# log4配置
#logging.config=src/main/resources/log4j.properties


###################################
#数据库链接信息
###################################
spring.datasource.url = jdbc:mysql://127.0.0.1/test?useUnicode=true&characterEncoding=utf-8
spring.datasource.driver-class-name = com.mysql.jdbc.Driver
spring.datasource.username = root
spring.datasource.password = root123456
#spring.datasource.type=com.alibaba.druid.pool.DruidDataSource


###################################
#mybatis
###################################
#mybatis自身的一些配置，例如基本类型的别名
#mybatis.config-location=classpath:mybatis/mybatis-config.xml
#指定mapper文件夹的位置
#mybatis.mapper-locations=classpath:mybatis/mapper/*.xml


###################################
#redis配置
###################################
#Redis服务器地址
spring.redis.host=127.0.0.1
#Redis服务器连接端口
spring.redis.port=6379
#Redis数据库索引（默认为0）
spring.redis.database=0  
#连接池最大连接数（使用负值表示没有限制）
spring.redis.jedis.pool.max-active=50
#连接池最大阻塞等待时间（使用负值表示没有限制）
spring.redis.jedis.pool.max-wait=3000
#连接池中的最大空闲连接
spring.redis.jedis.pool.max-idle=20
#连接池中的最小空闲连接
spring.redis.jedis.pool.min-idle=2
#连接超时时间（毫秒）
spring.redis.timeout=5000

#缓存
#spring.cache.cache-names=book1,book2
spring.cache.type=REDIS


###################################
# redis config
###################################
## REDIS (RedisProperties)
## Redis数据库索引（默认为0）
#spring.redis.database=0  
## Redis服务器地址
#spring.redis.host=192.168.0.58
## Redis服务器连接端口
#spring.redis.port=6379  
## Redis服务器连接密码（默认为空）
#spring.redis.password=  
## 连接池最大连接数（使用负值表示没有限制）
#spring.redis.pool.max-active=8  
## 连接池最大阻塞等待时间（使用负值表示没有限制）
#spring.redis.pool.max-wait=-1  
## 连接池中的最大空闲连接
#spring.redis.pool.max-idle=8  
## 连接池中的最小空闲连接
#spring.redis.pool.min-idle=0
## 连接超时时间（毫秒）
#spring.redis.timeout=0

```



**`2.log4j.properties`**

```properties
#####log4j配置####
log4j.rootLogger=INFO, CONSOLE, ROLLING_FILE, DAILY_ROLLING_FILE
 
log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender
log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout
log4j.appender.CONSOLE.layout.ConversionPattern=<%d>[%5p] %m - %c%n
 
# org.apache.log4j.RollingFileAppender
log4j.appender.ROLLING_FILE=org.apache.log4j.RollingFileAppender
log4j.appender.ROLLING_FILE.File=./logs/client.log
log4j.appender.ROLLING_FILE.Append=true
log4j.appender.ROLLING_FILE.MaxFileSize=20000KB
log4j.appender.ROLLING_FILE.MaxBackupIndex=100
log4j.appender.ROLLING_FILE.layout=org.apache.log4j.PatternLayout
 
#log4j.appender.ROLLING_FILE.layout.ConversionPattern=<%d>[%5p] %c - %m%n
log4j.appender.ROLLING_FILE.layout.ConversionPattern=%d %c [%t] (%F:%L) %-5p --> %m%n

# org.apache.log4j.DailyRollingFileAppender
log4j.appender.DAILY_ROLLING_FILE=org.apache.log4j.DailyRollingFileAppender
log4j.appender.DAILY_ROLLING_FILE.File=./logs/client3.log
log4j.appender.DAILY_ROLLING_FILE.DatePattern='.'yyyy-MM-dd'.log'
log4j.appender.DAILY_ROLLING_FILE.Append=true
log4j.appender.DAILY_ROLLING_FILE.layout=org.apache.log4j.PatternLayout
log4j.appender.DAILY_ROLLING_FILE.layout.ConversionPattern=%d %c [%t] (%F:%L) %-5p --> %m%n
 
```



## 项目源代码

### RedisConfig配置

```java
package com.empathy.redisdemo.config;

import java.io.Serializable;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.empathy.redisdemo.redis.ObjectRedisSerializer;


/**
 * @author: empathy
 * @description: Rdeis配置类
 * @date: 2019年2月11日 上午10:38:41 
 * @ver: 1.0
 *
 */
@Configuration
public class RedisConfig {

    /**
     * 连接 redis 需要 RedisConnection 和 RedisConnectionFactory，
     * RedisConnection 是通过 RedisConnectionFactory 进行创建
     * RedisConnection 提供较低级的数据操作 （byte arrays）
     */
	/* 
	@Bean
    RedisConnectionFactory initJedisConnectionFactory(){
        //在这里设置redis连接对象配置
        return new JedisConnectionFactory();
    }
    */
    
    /**
     * 配置RedisTemplate实例
     * 设置数据存入 redis 的序列化方式
     * @param factory
     * @return
     */
    @Bean
    public RedisTemplate<Serializable, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<Serializable, Object> template = new RedisTemplate<Serializable, Object>();
        template.setConnectionFactory(connectionFactory);
        template.afterPropertiesSet();
        template.setKeySerializer(new StringRedisSerializer());
        //template.setValueSerializer(new StringRedisSerializer());
        //save object
        template.setValueSerializer(new ObjectRedisSerializer());
        return template;
    }
}
```







###  Redis操作工具类

```java
/**
Redis操作工具类;基于RedisTemplate
1. redis缓存的增删改查功能
2. 可以根据RedisTemplate接口扩展更多功能
**/


package com.empathy.redisdemo.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;


/**
 * 
 * @author: empathy
 * @description: redis操作工具类(基于RedisTemplate)
 * @date: 2019年2月11日 上午10:46:39 
 * @ver: 1.0
 *
 */
@Component
public class RedisUtils {

	@Autowired
	private RedisTemplate<String, String> redisTemplate;
	
	
	//@Resource 
	//private RedisTemplate<Serializable, Object> redisTemplate3;

	/**
	 * 读取缓存
	 * 
	 * @param key
	 * @return
	 */
	public String get(final String key) {
		
		return redisTemplate.opsForValue().get(key);
	}
	
	
	/**
	 * 读取缓存3
	 * 
	 * @param key
	 * @return
	 */
	public Object get3(final String key) {
		//return redisTemplate3.opsForValue().get(key);
		return null;
	}

	/**
	 * 写入缓存
	 */
	public boolean set(final String key, String value) {
		boolean result = false;
		try {
			redisTemplate.opsForValue().set(key, value);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	/**
	 * 写入缓存3
	 */
	public boolean set3(final String key, Object value) {
		boolean result = false;
		try {
			//redisTemplate3.opsForValue().set(key, value);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 更新缓存
	 */
	public boolean getAndSet(final String key, String value) {
		boolean result = false;
		try {
			redisTemplate.opsForValue().getAndSet(key, value);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 删除缓存
	 */
	public boolean delete(final String key) {
		boolean result = false;
		try {
			redisTemplate.delete(key);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}

```





### 控制器操作

```java
/** redis 控制器的基本操作 
1. 新增操作接口
2. 查询操作接口
3. 更新操作接口
4. 删除操作接口
**/

package com.empathy.redisdemo.controller;

import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empathy.redisdemo.model.Demo;
import com.empathy.redisdemo.model.User;
import com.empathy.redisdemo.redis.RedisService;
import com.empathy.redisdemo.redis.RedisUtils;

/**
 * 
 * @author: empathy
 * @description: Redis控制器
 * @date: 2019年2月11日 上午10:42:46 
 * @ver: 1.0
 *
 */
@RestController
@RequestMapping("/redis")
public class RedisController {
	
	@Autowired
	private RedisTemplate<String, String> redisTemplate;
	
	
	@Resource
	private RedisUtils redisUtils;
	
	@Resource
	private RedisService  redisService;
	
	
	@RequestMapping("/set")
	public String set() {
		redisUtils.set("redis_key3", "redis_vale1");
		redisUtils.set("redis:demo", "redis_vale1");
		return "hello redis";
	}
	
	
	@RequestMapping("/set3")
	public String set3() {
		//redisService.set("redis_key7", "redis_vale7");
		/*
		User user=new User();
		user.setAge(18);
		user.setUserName("wangchao");
		user.setSex("1");
		user.setUserId("123456");
		*/
		Demo demo=new Demo();
		demo.setAge(18);
		demo.setUserName("wangchao3");
		demo.setSex("1");
		demo.setUserId("654321");
		redisService.set("redis:user:"+demo.getUserId(),demo);
		return "user";
	}
	
	
	@RequestMapping("/set2")
	public String set2() {
		// save a object
		User user=new User();
		user.setAge(18);
		user.setUserName("wangchao");
		user.setSex("1");
		user.setUserId("123123");
		redisUtils.set3("redis:user:"+user.getUserId(),user);
		return "user";
	}
	
	@RequestMapping("/get")
	public String get() {
		//String value = redisUtils.get("redis_key7");
		String value = redisService.get("redis_key7");
		//String value3 = redisUtils.get("redis:demo");
		return value;
	}
	
	
	@RequestMapping("/get3")
	public String get3() {
		//User user = redisService.get("redis:user:654321",User.class);
		Demo demo = redisService.get("redis:user:654321",Demo.class);
		return demo.getUserName();
	}
	
	
	@RequestMapping("/get2")
	public String get2() {
		User user = (User)redisUtils.get3("redis:user:123123");
		return user.getUserName();
	}
}

```





### 工具类

```java
/** JSON对象序列化和反序列化工具
1. 主要用于：redis缓存字符串对象存取的转化工具
2. 取redis字符串对象，转化为实体类对象，便于业务操作
**/
package com.empathy.redisdemo.utils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * 
 * @author: empathy
 * @description: JSON对象序列化和反序列化工具
 * @date: 2019年2月11日 上午10:44:34 
 * @ver: 1.0
 *
 */
public class JsonUtil {
    
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String convertObj2String(Object object) {
        String s = null;
        try {
            s = objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static <T> T convertString2Obj(String s, Class<T> clazz) {
        T t = null;
        try {
            t = objectMapper.readValue(s, clazz);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return t;
    }
}
```









### redis操作类汇总

```java
/** reids 操作类汇总
1. 基于spring-reids-data的接口扩展实现 
2. 主要实现redis缓存的增删改查操作实现
**/

package com.empathy.redisdemo.utils;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;


/**
 * 
 * @author: empathy
 * @description: Redis服务类
 * @date: 2019年2月11日 上午10:46:03 
 * @ver: 1.0
 *
 */
@Service
public class RedisService {

       @Autowired
        private StringRedisTemplate redisTemplate;

        /**
         * 一周有多少秒
         */
        private static final long WEEK_SECONDS = 7 * 24 * 60 * 60;


        /**
         * 将 key，value 存放到redis数据库中，默认设置过期时间为一周
         *
         * @param key
         * @param value
         */
        public void set(String key, Object value) {
            redisTemplate.opsForValue().set(key, JsonUtil.convertObj2String(value), WEEK_SECONDS, TimeUnit.SECONDS);
        }

        /**
         * 将 key，value 存放到redis数据库中，设置过期时间单位是秒
         *
         * @param key
         * @param value
         * @param expireTime
         */
        public void set(String key, Object value, long expireTime) {
            redisTemplate.opsForValue().set(key, JsonUtil.convertObj2String(value), expireTime, TimeUnit.SECONDS);
        }

        /**
         * 判断 key 是否在 redis 数据库中
         *
         * @param key
         * @return
         */
        public boolean exists(final String key) {
            return redisTemplate.hasKey(key);
        }

        /**
         * 获取与 key 对应的对象
         * @param key
         * @param clazz 目标对象类型
         * @param <T>
         * @return
         */
        public <T> T get(String key, Class<T> clazz) {
            String s = get(key);
            if (s == null) {
                return null;
            }
            return JsonUtil.convertString2Obj(s, clazz);
        }

        /**
         * 获取 key 对应的字符串
         * @param key
         * @return
         */
        public String get(String key) {
            return redisTemplate.opsForValue().get(key);
        }

        /**
         * 删除 key 对应的 value
         * @param key
         */
        public void delete(String key) {
            redisTemplate.delete(key);
        }
}

```





### 项目启动

```java
/** spring boot启动类 
**/
package com.empathy.redisdemo;

import org.apache.log4j.Logger;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;


/**
 * SpringBoot启动类
 * @author empathy
 * 2018年7月2日下午5:41:45
 */
@SpringBootApplication
//指定要扫描的Mapper类的包的路径
@MapperScan("com.empathy.redisdemo.mapper")
//开启缓存功能
@EnableCaching
public class RedisDemoApplication {
	
	private static Logger logger = Logger.getLogger(RedisDemoApplication.class);
	//private static Logger logger = LoggerFactory.getLogger(SpringbootApplication.class);
	
	public static void main(String[] args) {
		logger.info("############################## start #################################");
		if(logger.isDebugEnabled()) {
			logger.debug("SpringBoot starting...");
		}
		logger.info("############################## end #################################");
		SpringApplication.run(RedisDemoApplication.class, args);
	}
}


```





## 参考资料

```
# springboot配置redis
1.https://www.cnblogs.com/xiaoping1993/p/7761123.html
# springboot整合redis——redisTemplate的使用（扩展redis的默认配置）
2.https://www.cnblogs.com/jiangbei/p/8601107.html
# Springboot+Redis 配置和使用（直接使用redis的默认配置）
3.https://blog.csdn.net/ai88030669/article/details/78686403
# SpringBoot(六)：SpringBoot整合Redis
4.https://blog.csdn.net/plei_yue/article/details/79362372
# spring boot redis配置
5. https://blog.csdn.net/qq_37334435/article/details/79758184
```

